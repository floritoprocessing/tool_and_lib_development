//testVec
import java.util.Vector;

Particle3D p=new Particle3D();
float ms=2.0;

float border=200;

void setup() {
  size(800,600,P3D);
  background(255,255,255);
  p.setBorder(border,border,border);
  p.setMov(random(-ms,ms),random(-ms,ms),random(-ms,ms));
  p.setPos(random(-border,border),random(-border,border),random(-border,border));
  p.setMinimumGravDistance(3);
  p.setDrag(0.995);
}

void draw() {
  background(255,255,255);
  translate(width/2.0,height/2.0,-border);
  
  // MOUSE ROTATE:
  rotateY(PI/2.0*(width/2.0-mouseX)/(float)width);
  rotateX(PI/2.0*(mouseY-height/2.0)/(float)height);
  
  
  
  
  p.movUpdateGravitateTo(0,0,0,5);
  //p.movUpdateDrag();
  p.posUpdate();
  if (frameCount%10==0) { p.tracePosition(); }
  
  drawTrace(p.trace);
  drawBallOnPos(p.getPosX(),p.getPosY(),p.getPosZ(),8);
  drawBallShadowOnCube(p.getPosX(),p.getPosY(),p.getPosZ(),8,border);
  drawCube(2*border);
}








/****************************
 very boring drawing routines
 ****************************/

void drawTrace(Vector _vc) {
  stroke(255,0,0,64);
  beginShape(LINE_STRIP);
  for (int i=0;i<_vc.size();i++) {
      Vec v=new Vec((Vec)_vc.elementAt(i));
      vertex((float)v.x,(float)v.y,(float)v.z);
  }
  endShape();
}

void drawCube(float side) {
  noFill(); stroke(0,0,0,32);
  box(side);
}

void drawBallOnPos(double _x, double _y, double _z, float _side) {
  pushMatrix();
  translate((float)_x,(float)_y,(float)_z);
  fill(128,0,0); noStroke();
  sphere(_side);
  popMatrix();
}

void drawBallShadowOnCube(double _x, double _y, double _z, float _side, float _border) {
  noStroke(); fill(128,128,128,12);
  ellipseMode(RADIUS);
  
  float x=(float)_x, y=(float)_y, z=(float)_z;
  
  pushMatrix();
    translate(x,y,-_border);
    ellipse(0,0,_side,_side);
    translate(0,0,2*_border);
    ellipse(0,0,_side,_side);
  popMatrix();
  
  pushMatrix();
    translate(-_border,y,z);
    pushMatrix(); 
      rotateY(PI/2.0);
      ellipse(0,0,_side,_side); 
    popMatrix();
    translate(_border*2,0,0);
    pushMatrix(); 
      rotateY(PI/2.0);
      ellipse(0,0,_side,_side);
    popMatrix();
  popMatrix();
  
  pushMatrix();
    translate(x,-_border,z);
    pushMatrix(); 
      rotateX(PI/2.0);
      ellipse(0,0,_side,_side); 
    popMatrix();
    translate(0,_border*2,0);
    pushMatrix(); 
      rotateX(PI/2.0);
      ellipse(0,0,_side,_side);
    popMatrix();
  popMatrix();
}
