/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double x=0,y=0,z=0;
  
  Vec() {
  }

  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }

  void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }
  
  void add(double _x, double _y, double _z) {
    x+=_x;
    y+=_y;
    z+=_z;
  }

  void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }

  void negX() {
    x=-x;
  }

  void negY() {
    y=-y;
  }

  void negZ() {
    z=-z;
  }

  void neg() {
    negX();
    negY();
    negZ();
  }

  void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
}
