class Particle3D {
  Vec pos=new Vec();
  Vec mov=new Vec();
  Vector trace=new Vector();
  
  double minGravDist2=0.0001;
  double drag=1.0;
  Vec border=new Vec(1,1,1);
  
  Particle3D() {
  }
  
  void setPos(double _x, double _y, double _z) {
    pos=new Vec(_x,_y,_z);
  }
  
  void setMov(double _x, double _y, double _z) {
    mov=new Vec(_x,_y,_z);
  }
  
  void setDrag(double _drag) {
    drag=_drag;
  }
  
  void setBorder(double _borderX, double _borderY, double _borderZ) {
    border=new Vec(_borderX,_borderY,_borderZ);
  }
  
  void setMinimumGravDistance(double _minGravDist) {
    minGravDist2=_minGravDist*_minGravDist;
  }
  
  double getPosX() { return pos.x; }
  double getPosY() { return pos.y; }
  double getPosZ() { return pos.z; }
  
  void movUpdateGravitateTo(double _x, double _y, double _z, double _fFac) {
    double dx=_x-pos.x;
    double dy=_y-pos.y;
    double dz=_z-pos.z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {d2=minGravDist2;}
    //double d=Math.sqrt(d2);
    double F=_fFac/d2;
    mov.add(new Vec(dx*F,dy*F,dz*F));
  }
  
  void movUpdateDrag() {
    mov.mul(drag);
  }
  
  void posUpdate() {
    pos.add(mov);
    if (pos.x>border.x)  {mov.negX(); pos.x-=2*(pos.x-border.x); }
    if (pos.x<-border.x) {mov.negX(); pos.x-=2*(pos.x+border.x); }
    if (pos.y>border.y)  {mov.negY(); pos.y-=2*(pos.y-border.y); }
    if (pos.y<-border.y) {mov.negY(); pos.y-=2*(pos.y+border.y); }
    if (pos.z>border.z)  {mov.negZ(); pos.z-=2*(pos.z-border.z); }
    if (pos.z<-border.z) {mov.negZ(); pos.z-=2*(pos.z+border.z); }
  }
  
  void tracePosition() {
    trace.addElement(new Vec(pos));
  }
  
}
