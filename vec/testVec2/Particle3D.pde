class Particle3D {
  Vec pos=new Vec();
  Vec mov=new Vec();
  Vec acc=new Vec();
  Vector trace=new Vector();
  
  //double minGravDist2=0.0001;
  double drag=1.0;
  Vec border=new Vec(1,1,1);
  
  Particle3D() {
  }
  
  void setPos(double _x, double _y, double _z) {
    pos=new Vec(_x,_y,_z);
  }
  
  void setMov(double _x, double _y, double _z) {
    mov=new Vec(_x,_y,_z);
  }
  
  void setAcc(double _x, double _y, double _z) {
    acc=new Vec(_x,_y,_z);
  }
  
  void setDrag(double _drag) {
    drag=_drag;
  }
  
  void setBorder(double _borderX, double _borderY, double _borderZ) {
    border=new Vec(_borderX,_borderY,_borderZ);
  }
  
//  void setMinimumGravDistance(double _minGravDist) {
//    minGravDist2=_minGravDist*_minGravDist;
//  }
  
  double getPosX() { return pos.x; }
  double getPosY() { return pos.y; }
  double getPosZ() { return pos.z; }
  Vec getPos() { return pos; }
  
  void accUpdateGravitateTo(Vec _v, double _fFac, double _mgd) {
    double minGravDistSQ=_mgd*_mgd;
//    double dx=_v.x-pos.x;
//    double dy=_v.y-pos.y;
//    double dz=_v.z-pos.z;
    
    Vec d=new Vec(_v);
    d.sub(pos);
    double d2=d.lenSQ();
//    double d2=dx*dx+dy*dy+dz*dz;
    
    if (d2<minGravDistSQ) {d2=minGravDistSQ;}
    double F=_fFac/d2;
    acc=new Vec(d); acc.mul(F);
  }
  
  void movUpdateAcc() {
    mov.add(acc);
  }
  
  void movUpdateDrag() {
    mov.mul(drag);
  }
  
  void posUpdate(boolean borderCheck) {
    pos.add(mov);
    if (borderCheck) {
      if (pos.x>border.x)  {mov.negX(); pos.x-=2*(pos.x-border.x); }
      if (pos.x<-border.x) {mov.negX(); pos.x-=2*(pos.x+border.x); }
      if (pos.y>border.y)  {mov.negY(); pos.y-=2*(pos.y-border.y); }
      if (pos.y<-border.y) {mov.negY(); pos.y-=2*(pos.y+border.y); }
      if (pos.z>border.z)  {mov.negZ(); pos.z-=2*(pos.z-border.z); }
      if (pos.z<-border.z) {mov.negZ(); pos.z-=2*(pos.z+border.z); }
    }
  }
  
  void tracePosition() {
    trace.addElement(new Vec(pos));
  }
  
}
