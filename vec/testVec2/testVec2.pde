//testVec
import java.util.Vector;

Particle3D[] p=new Particle3D[3];
float ms=0.2;
double GRAVITY_STRENGTH=5.0;
double MIN_GRAV_DISTANCE=8.0;
double DRAG=0.995;

float BORDER=200;

void setup() {
  size(800,800,P3D);
  background(255,255,255);
  
  for (int i=0;i<p.length;i++) {
    p[i]=new Particle3D();
    p[i].setBorder(BORDER,BORDER,BORDER);
    p[i].setMov(random(-ms,ms),random(-ms,ms),random(-ms,ms));
    p[i].setPos(random(-BORDER,BORDER),random(-BORDER,BORDER),random(-BORDER,BORDER));
    p[i].setDrag(DRAG);
  }
}

void draw() {
  background(255,255,255);
  translate(width/2.0,height/2.0,-BORDER);
  
  // MOUSE ROTATE:
  rotateY(PI/2.0*(width/2.0-mouseX)/(float)width);
  rotateX(PI/2.0*(mouseY-height/2.0)/(float)height);
  
  // NEW MOV VECTOR FOR ALL 3D PARTICLES ACCORDING TO GRAVITY TO EACH OTHER
  for (int i=0;i<p.length;i++) {
    p[i].setAcc(0,0,0);
    for (int j=0;j<p.length;j++) {
      if (i!=j) {
        p[i].accUpdateGravitateTo(p[j].getPos(),GRAVITY_STRENGTH,MIN_GRAV_DISTANCE);
      }
    }
    p[i].movUpdateAcc();
    //p[i].movUpdateDrag();
  }
  
  // UPDATE ALL POSITIONS
  for (int i=0;i<p.length;i++) {
    p[i].posUpdate(false);
    if (frameCount%10==0) { p[i].tracePosition(); }
  }
    
  // DRAW ALL
  for (int i=0;i<p.length;i++) {
    drawTrace(p[i].trace);
    drawBallOnPos(p[i].getPosX(),p[i].getPosY(),p[i].getPosZ(),8);
    drawBallShadowOnCube(p[i].getPosX(),p[i].getPosY(),p[i].getPosZ(),8,BORDER);
  }
  drawCube(2*BORDER);
}








/****************************
 very boring drawing routines
 ****************************/

void drawTrace(Vector _vc) {
  stroke(255,0,0,64);
  beginShape(LINE_STRIP);
  for (int i=0;i<_vc.size();i++) {
      Vec v=new Vec((Vec)_vc.elementAt(i));
      vertex((float)v.x,(float)v.y,(float)v.z);
  }
  endShape();
}

void drawCube(float side) {
  noFill(); stroke(0,0,0,32);
  box(side);
}

void drawBallOnPos(double _x, double _y, double _z, float _side) {
  pushMatrix();
  translate((float)_x,(float)_y,(float)_z);
  fill(128,0,0); noStroke();
  sphere(_side);
  popMatrix();
}

void drawBallShadowOnCube(double _x, double _y, double _z, float _side, float _border) {
  noStroke(); fill(128,128,128,12);
  ellipseMode(RADIUS);
  
  float x=(float)_x, y=(float)_y, z=(float)_z;
  boolean xWithin=(x>-BORDER&&x<BORDER);
  boolean yWithin=(y>-BORDER&&y<BORDER);
  boolean zWithin=(z>-BORDER&&z<BORDER);
  
  if (xWithin&&yWithin) {
    pushMatrix();
    translate(x,y,-_border);
    ellipse(0,0,_side,_side);
    translate(0,0,2*_border);
    ellipse(0,0,_side,_side);
    popMatrix();
  }
  
  if (yWithin&&zWithin) {
    pushMatrix();
    translate(-_border,y,z);
    pushMatrix(); 
      rotateY(PI/2.0);
      ellipse(0,0,_side,_side); 
    popMatrix();
    translate(_border*2,0,0);
    pushMatrix(); 
      rotateY(PI/2.0);
      ellipse(0,0,_side,_side);
    popMatrix();
    popMatrix();
  }
  
  if (xWithin&&zWithin) {
    pushMatrix();
    translate(x,-_border,z);
    pushMatrix(); 
      rotateX(PI/2.0);
      ellipse(0,0,_side,_side); 
    popMatrix();
    translate(0,_border*2,0);
    pushMatrix(); 
      rotateX(PI/2.0);
      ellipse(0,0,_side,_side);
    popMatrix();
    popMatrix();
  }
}
