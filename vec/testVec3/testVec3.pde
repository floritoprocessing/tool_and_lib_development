//testVec
import java.util.Vector;

float ms=0.02;
double GRAVITY_STRENGTH=0.01;
double MIN_GRAV_DISTANCE=4.0;
double DRAG=0.9999;

boolean BOUNCE_ON_BORDERS=false;
boolean DRAW_TRACE=false;
boolean DRAW_BALL_SHADOW=false;

float BALL_SIZE=2;

float BORDER=200;

int RENDERS_PER_FRAME=100;

Particle3D[] p=new Particle3D[20];

void setup() {
  size(800,800,P3D); //P3D, JAVA2D, OPENGL
  background(255,255,255);
  
  for (int i=0;i<p.length;i++) {
    p[i]=new Particle3D();
    p[i].setBorder(BORDER,BORDER,BORDER);
    p[i].setMov(random(-ms,ms),random(-ms,ms),random(-ms,ms));
    p[i].setPos(random(-BORDER,BORDER),random(-BORDER,BORDER),random(-BORDER,BORDER));
    p[i].pos.div(4.0);
    p[i].setDrag(DRAG);
  }
}

void draw() {


  for (int rpf=0;rpf<RENDERS_PER_FRAME;rpf++) {
  
    // NEW MOV VECTOR FOR ALL 3D PARTICLES ACCORDING TO GRAVITY TO EACH OTHER
    for (int i=0;i<p.length;i++) {
      p[i].setAcc(0,0,0);
      for (int j=0;j<p.length;j++) {
        if (i!=j) {
          p[i].accUpdateGravitateTo(p[j].getPos(),GRAVITY_STRENGTH,MIN_GRAV_DISTANCE);
        }
      }
      p[i].movUpdateAcc();
      //p[i].movUpdateDrag();
    }
  
  
    // UPDATE ALL POSITIONS
    for (int i=0;i<p.length;i++) {
      p[i].posUpdate(BOUNCE_ON_BORDERS);
      if (DRAW_TRACE) { if (frameCount%10==0) { p[i].tracePosition(); } }
    }
    
  }
  
  
  // MOVE ALL POSITIONS TO AN AVERAGE CENTER
  Vec center=new Vec();
  for (int i=0;i<p.length;i++) { center.add(p[i].getPos()); }
  center.div((float)p.length);
  for (int i=0;i<p.length;i++) { p[i].pos.sub(center); }
  
  
  // DRAW EVERYTHING
  
  background(255,255,255);
  translate(width/2.0,height/2.0,-BORDER);
  
  // MOUSE ROTATE:
  rotateY(PI/2.0*(width/2.0-mouseX)/(float)width);
  rotateX(PI/2.0*(mouseY-height/2.0)/(float)height);

  for (int i=0;i<p.length;i++) {
    drawBallOnPos(p[i].getPosX(),p[i].getPosY(),p[i].getPosZ(),BALL_SIZE);
    if (DRAW_TRACE) { drawTrace(p[i].trace); }
    if (DRAW_BALL_SHADOW) { drawBallShadowOnCube(p[i].getPos(),BALL_SIZE,BORDER); }
  }
  drawCube(2*BORDER);
  
  
  
}








/****************************
 very boring drawing routines
 ****************************/

void drawTrace(Vector _vc) {
  stroke(255,0,0,64);
  beginShape(LINE_STRIP);
  for (int i=0;i<_vc.size();i++) {
      Vec v=new Vec((Vec)_vc.elementAt(i));
      vertex((float)v.x,(float)v.y,(float)v.z);
  }
  endShape();
}

void drawCube(float side) {
  noFill(); stroke(0,0,0,32);
  box(side);
}

void drawBallOnPos(double _x, double _y, double _z, float _side) {
  pushMatrix();
  translate((float)_x,(float)_y,(float)_z);
  fill(128,0,0); noStroke();
  sphere(_side);
  popMatrix();
}

void drawBallShadowOnCube(Vec _v, float _side, float _border) {
  noStroke(); fill(128,128,128,12);
  ellipseMode(RADIUS);
  
  float x=(float)_v.x, y=(float)_v.y, z=(float)_v.z;
  boolean xWithin=(x>-BORDER&&x<BORDER);
  boolean yWithin=(y>-BORDER&&y<BORDER);
  boolean zWithin=(z>-BORDER&&z<BORDER);
  
  if (xWithin&&yWithin) {
    pushMatrix();
    translate(x,y,-_border);
    ellipse(0,0,_side,_side);
    translate(0,0,2*_border);
    ellipse(0,0,_side,_side);
    popMatrix();
  }
  
  if (yWithin&&zWithin) {
    pushMatrix();
    translate(-_border,y,z);
    pushMatrix(); 
      rotateY(PI/2.0);
      ellipse(0,0,_side,_side); 
    popMatrix();
    translate(_border*2,0,0);
    pushMatrix(); 
      rotateY(PI/2.0);
      ellipse(0,0,_side,_side);
    popMatrix();
    popMatrix();
  }
  
  if (xWithin&&zWithin) {
    pushMatrix();
    translate(x,-_border,z);
    pushMatrix(); 
      rotateX(PI/2.0);
      ellipse(0,0,_side,_side); 
    popMatrix();
    translate(0,_border*2,0);
    pushMatrix(); 
      rotateX(PI/2.0);
      ellipse(0,0,_side,_side);
    popMatrix();
    popMatrix();
  }
}
