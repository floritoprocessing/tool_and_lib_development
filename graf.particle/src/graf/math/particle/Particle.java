package graf.math.particle;

import graf.math.vector.Vec;

public class Particle implements Cloneable {
	
	private Vec pos = new Vec();
	private Vec mov = new Vec();
	private double mass=1;
	
	public Particle() {
	}

	/**
	 * @param pos
	 */
	public Particle(Vec pos) {
		this.pos = pos;
	}

	/**
	 * @param pos
	 * @param mass
	 */
	public Particle(Vec pos, double mass) {
		this.pos = pos;
		this.mass = mass;
	}

	/**
	 * @param pos
	 * @param mov
	 */
	public Particle(Vec pos, Vec mov) {
		this.pos = pos;
		this.mov = mov;
	}
	
	/**
	 * @param pos
	 * @param mov
	 * @param mass
	 */
	public Particle(Vec pos, Vec mov, double mass) {
		this.pos = pos;
		this.mov = mov;
		this.mass = mass;
	}

	/**
	 * @return the mass
	 */
	public double getMass() {
		return mass;
	}
	

	/**
	 * @return the motion vector mov
	 */
	public Vec getMov() {
		return mov;
	}

	/**
	 * @return the position vector pos
	 */
	public Vec getPos() {
		return pos;
	}

	/**
	 * Move the Particle by it's internal motion vector mov
	 */
	/*public void move() {
		pos.add(mov);
	}*/

	/**
	 * Move the Particle by it's internal motion vector mov multiplied by fac
	 * @param fac
	 *//*
	public void move(double fac) {
		pos.add(Vec.mul(mov,fac));
	}*/

	/**
	 * @param mass the mass to set
	 */
	public void setMass(double mass) {
		this.mass = mass;
	}
	
	/**
	 * Sets the motion vector mov
	 * @param mov the mov to set
	 */
	public void setMov(Vec mov) {
		this.mov = mov;
	}
	
	/**
	 * Sets the position vector pos
	 * @param pos the pos to set
	 */
	public void setPos(Vec pos) {
		this.pos = pos;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(mass);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		result = PRIME * result + ((mov == null) ? 0 : mov.hashCode());
		result = PRIME * result + ((pos == null) ? 0 : pos.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Particle other = (Particle) obj;
		if (Double.doubleToLongBits(mass) != Double.doubleToLongBits(other.mass))
			return false;
		if (mov == null) {
			if (other.mov != null)
				return false;
		} else if (!mov.equals(other.mov))
			return false;
		if (pos == null) {
			if (other.pos != null)
				return false;
		} else if (!pos.equals(other.pos))
			return false;
		return true;
	}
	
	public Object clone() {
		return new Particle(new Vec(pos),new Vec(mov),mass);
	}

	public void integrate(double time, Vec acceleration) {
		if (acceleration==null) acceleration = new Vec();
		double ti2 = time*time;
		double nx = pos.getX() + mov.getX()*time + 0.5*acceleration.getX()*ti2;
		double ny = pos.getY() + mov.getY()*time + 0.5*acceleration.getY()*ti2;
		double nz = pos.getZ() + mov.getZ()*time + 0.5*acceleration.getZ()*ti2;
		Vec newPos = new Vec(nx,ny,nz);
		if (!acceleration.isNullVector()) {
			mov = Vec.add(mov, Vec.mul(acceleration, time));
		}
		
		
		pos.set(newPos);
	}

}
