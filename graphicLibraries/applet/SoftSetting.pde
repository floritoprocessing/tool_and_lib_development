void blendCircle(float x, float y, float r, int c, float perc, int blendMode) {
  float circum=2*PI*r;
  if (circum==0) {
    return;
  } else {
    for (float rdperc=0;rdperc<1.0;rdperc+=1.0/circum) {
      float rd=2*PI*rdperc;
      float xx=x+r*cos(rd);
      float yy=y+r*sin(rd);
      blendSet(xx,yy,c,perc,blendMode);
    }
  }
}

void blendLine(float x1, float y1, float x2, float y2, int c, float perc, int blendMode) {
  float len=sqrt(sq(x2-x1)+sq(y2-y1));
  if (len==0) {
    return;
  } else {
    float dx=x2-x1;
    float dy=y2-y1;
    for (float lenperc=0;lenperc<1.0;lenperc+=1.0/len) {
      blendSet(x1+lenperc*dx,y1+lenperc*dy,c,perc,blendMode);
    }
  }
}

void blendSet(float x, float y, int c, float perc, int blendMode) {
  int x0=floor(x);
  int x1=x0+1;
  int y0=floor(y);
  int y1=y0+1;
  float px0=x1-x;
  float px1=x-x0;
  float py0=y1-y;
  float py1=y-y0;
  float press00=px0*py0;
  float press10=px1*py0;
  float press01=px0*py1;
  float press11=px1*py1;
  int c00=colorMix(c,press00,perc,get(x0,y0),blendMode);
  int c10=colorMix(c,press10,perc,get(x1,y0),blendMode);
  int c01=colorMix(c,press01,perc,get(x0,y1),blendMode);
  int c11=colorMix(c,press11,perc,get(x1,y1),blendMode);
  set(x0,y0,c00);
  set(x1,y0,c10);
  set(x0,y1,c01);
  set(x1,y1,c11);
}

class BlendMode {
  public static final int BLENDMODE_ADD=0;
  public static final int BLENDMODE_MULTIPLY=1;
  //public static final int BLENDMODE_ADD_TRANS=1;
  BlendMode() {}
}

int colorMix(int c1, float p1, float perc, int c2, int blendMode) {
  int c=0;
  // draw color: (is 1);
  int r1=c1>>16&0xFF;
  int g1=c1>>8&0xFF;
  int b1=c1&0xFF;
  // background color (is 2);
  int r2=c2>>16&0xFF;
  int g2=c2>>8&0xFF;
  int b2=c2&0xFF;
  // blend:
  int rr=0, gg=0, bb=0;
  switch (blendMode) {
    case BlendMode.BLENDMODE_ADD:
      rr=r2+r1;
      rr=rr<255?rr:255;
      gg=g2+g1;
      gg=gg<255?gg:255;
      bb=b2+b1;
      bb=bb<255?bb:255;
    break;
    case BlendMode.BLENDMODE_MULTIPLY:
      rr=(r2*r1)>>8;
      gg=(g2*g1)>>8;
      bb=(b2*b1)>>8;
    break;
    default: break;
  }
  // transparency;
  // change end result by mixing it linear with background color:
  float drawPerc=p1*perc;
  float bgPerc=1.0-drawPerc;
  rr=int(rr*drawPerc+r2*bgPerc);
  gg=int(gg*drawPerc+g2*bgPerc);
  bb=int(bb*drawPerc+b2*bgPerc);
  c=rr<<16|gg<<8|bb;
  
  return c;
}
