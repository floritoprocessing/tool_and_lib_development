package graf.math.superlong;

import java.util.Vector;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class SuperLong {

	private int maxId=0;
	private final Vector<Digit> digits = new Vector<Digit>();
	
	public SuperLong() {
		addNewDigit();
	}
	
	public SuperLong(int decimalValue) {
		super();
		addDecimalValue(decimalValue);
	}
	
	public SuperLong(String decimalValue) {
		super();
		addDecimalValue(decimalValue);
	}

	public Digit addNewDigit() {
		Digit d = new Digit(0,this,maxId++);
		digits.add(d);
		return d;
	}
	
	public Digit getNextDigit(Digit d) {
		if (d.getId()<maxId-1) {
			return digits.elementAt(d.getId()+1);
		} else {
			return addNewDigit();
		}
	}
	
	public void addOne() {
		digits.elementAt(0).increase();
	}
	
	public int amountOfDigits() {
		return maxId;
	}
	
	public void addDecimalValue(String value) {
		int numberOfDigits = value.length();
		for (int d=0;d<numberOfDigits;d++) {
			int increaseVal = Integer.parseInt(value.substring(numberOfDigits-d-1,numberOfDigits-d));
			for (int i=0;i<increaseVal;i++) {
				if (d<maxId) {
					digits.elementAt(d).increase();
				} else {
					addNewDigit().increase();
				}
			}
		}
	}

	public void addDecimalValue(int value) {
		int numberOfDigits = 1 + (int)Math.log10(value);
		for (int d=0;d<numberOfDigits;d++) {
			int e0 = (int)Math.pow(10, d);
			int e1 = (int)Math.pow(10, d+1);
			
			int increaseVal = value%e1;
			value -= increaseVal;
			increaseVal /= e0;
			
			for (int i=0;i<increaseVal;i++) {
				if (d<maxId) {
					digits.elementAt(d).increase();
				} else {
					addNewDigit().increase();
				}
			}

		}
	}

	public String getDecimalAsString() {
		String out = "";
		for (int i=0;i<maxId;i++) {
			out = digits.elementAt(i).value() + out;
		}
		return out;
	}
	
	public SuperLong duplicate() {
		addDecimalValue(getDecimalAsString());
		return this;
	}
	
	public static final SuperLong getMathPow2(int exponent) {
		if (exponent<0) throw new NumberFormatException("Keep exponent >= 0");
		SuperLong out = new SuperLong(1);
		for (int i=0;i<exponent;i++) {
			out.addDecimalValue(out.getDecimalAsString());
			//out.duplicate();
		}
		return out;
	}
	
	public static final SuperLong getMathPow10(int exponent) {
		if (exponent<0) throw new NumberFormatException("Keep exponent >= 0");
		SuperLong out = new SuperLong();
		for (int i=0;i<exponent;i++)
			out.addNewDigit();
		out.digits.elementAt(out.maxId-1).increase();
		return out;
	}
	
	public String toString() {
		return getDecimalAsString();
	}
	
}
