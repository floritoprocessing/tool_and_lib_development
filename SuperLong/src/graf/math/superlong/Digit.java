package graf.math.superlong;

public class Digit {

	private int id=0;
	private int val;
	private SuperLong superLong;
	
	public Digit(int val, SuperLong superLong, int id) {
		if (val<0||val>9) throw new RuntimeException("Illegal value, stay within 0..9");
		this.val = val;
		this.superLong = superLong;
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void increase() {
		val++;
		if (val==10) {
			val=0;
			superLong.getNextDigit(this).increase();
		}
	}

	public int value() {
		return val;
	}
}
