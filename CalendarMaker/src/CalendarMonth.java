import java.util.Calendar;
import java.util.Vector;


public class CalendarMonth {
	
	Vector<CalendarDayObject> days = new Vector<>();
	private static int dayInMilliseconds = 1000*60*60*24;
	
	public CalendarMonth(int year, int humanMonth) {
		CalendarDayObject calendarDay = new CalendarDayObject(year, humanMonth, 0);
		
		while (!calendarDay.isMonday()) {
			calendarDay.c.setTimeInMillis(calendarDay.c.getTimeInMillis()-dayInMilliseconds);
		}
		
		CalendarDayObject firstMondayOnView = calendarDay;
		long time = firstMondayOnView.c.getTimeInMillis();
		
		CalendarDayObject today = null;
		boolean startOfMonth = false;
		boolean endOfMonth = false;
		do {
			for (int i=0;i<7;i++) {
				today = new CalendarDayObject(year, humanMonth, 0);
				today.c.setTimeInMillis(time);
				days.add(today);
				
				time += dayInMilliseconds;
				
				if (today.c.get(Calendar.DATE)==1) startOfMonth=true;
				else if (startOfMonth && !endOfMonth) {
					Calendar tomorrow  = Calendar.getInstance();
					tomorrow.setTimeInMillis(time);
					if (today.c.get(Calendar.DATE)==1 || tomorrow.get(Calendar.DATE)==1) endOfMonth=true;
				}
				//System.out.println(today.c.get(Calendar.DATE)+" "+startOfMonth+" ");
			}
		} while (!endOfMonth);
	}
	
	

}
