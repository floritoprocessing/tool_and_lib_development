import java.util.Calendar;

import processing.core.PApplet;


public class CalendarMaker extends PApplet {

	private static final long serialVersionUID = -5164502802895242910L;

	public void setup() {
		size(1200,840);
	}
	
	public void draw() {
		
	}

	public static void main(String[] args) {
		CalendarMonth month = new CalendarMonth(2019, 2);
		int rows = (int)Math.ceil(month.days.size()/7.0);
		int index=0;
		for (int y=0;y<rows;y++) {			
			for (int x=0;x<7;x++) {
				int date = month.days.get(index).c.get(Calendar.DATE);
				int rnd = (int)(5*Math.random());
				for (int e=0;e<rnd;e++) {
					month.days.get(index).attachedObjects.add("randomWord");
				}
				System.out.print(date+"\t");
				index++;
			}
			System.out.println();
		}
	}
}
