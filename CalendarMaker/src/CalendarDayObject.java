import java.util.Calendar;
import java.util.Vector;


public class CalendarDayObject {

	public Calendar c;
	public Vector<String> attachedObjects = new Vector<String>();
	
	public CalendarDayObject(int year, int humanMonth, int day) {
		c = Calendar.getInstance();
		c.set(year, humanMonth-1, day, 12, 0);
		//System.out.println(c);
	}
	
	boolean isMonday() {
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
	}
	
	public void addObject(String object) {
		attachedObjects.add(object);
	}
}
