package graf.math.vector;

public class Vec implements Cloneable {

	public static Vec add(Vec v1, Vec v2) {
		return new Vec(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);
	}

	public static double distance(Vec v1, Vec v2) {
		double dx = v1.x-v2.x;
		double dy = v1.y-v2.y;
		double dz = v1.z-v2.z;
		return Math.sqrt(dx*dx+dy*dy+dz*dz);
	}

	public static Vec div(Vec vec, double d) {
		return new Vec(vec.x/d, vec.y/d, vec.z/d);
	}
	
	public static Vec invert(Vec vec) {
		Vec nvec = new Vec(vec);
		nvec.invert();
		return nvec;
	}

	public static Vec mul(Vec v, double n) {
		Vec vn = new Vec(v);
		vn.mul(n);
		return vn;
	}
	
	public static Vec normalize(Vec vec) {
		if (vec.isNullVector()) return vec;
		return div(vec, vec.length());
	}

	public static Vec sub(Vec v1, Vec v2) {
		return new Vec(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z);
	}

	public double x=0, y=0, z=0;

	public Vec() {
	}

	public Vec(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vec(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vec(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}

	public void add(Vec v) {
		x += v.x;
		y += v.y;
		z += v.z;
	}
	
	public Object clone() {
		return new Vec(this);
	}
	
	public void div(double n) {
		x /= n;
		y /= n;
		z /= n;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Vec other = (Vec) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	public void invert() {
		mul(-1);
	}
	
	public boolean isNullVector() {
		return (x==0&&y==0&&z==0);
	}

	public double length() {
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	public double lengthSquared() {
		return x*x+y*y+z*z;
	}

	public void mul(double n) {
		x *= n;
		y *= n;
		z *= n;
	}

	public void normalize() {
		if (!isNullVector()) div(length());
	}
	
	public void rotX(double rd) {
		double SIN=Math.sin(rd); 
		double COS=Math.cos(rd);
		double yn=y*COS-z*SIN;
		double zn=z*COS+y*SIN;
		y=yn;
		z=zn;
	}
	
	public void rotY(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-z*SIN; 
		double zn=z*COS+x*SIN;
		x=xn;
		z=zn;
	}
	
	public void rotZ(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-y*SIN; 
		double yn=y*COS+x*SIN;
		x=xn;
		y=yn;
	}

	public void set(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}

	public void set(Vec vec) {
		x=vec.x;
		y=vec.y;
		z=vec.z;
	}

	public void setNull() {
		x=0;
		y=0;
		z=0;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public void sub(Vec v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
	}

	public String toString() {
		return x+"\t"+y+"\t"+z;
	}

	public void setLength(double newLength) {
		if (!isNullVector()) mul(newLength/length());
	}
	
}
