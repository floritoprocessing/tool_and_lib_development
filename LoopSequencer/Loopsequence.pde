class Loopsequence {
  
  ArrayList events = new ArrayList();
  
  boolean playing;
  long loopLength;
  float speed = 1;
  
  long startTime;
  float loopPercentage;

  boolean loopEnded;
  long lastTime, currentTime;
  long lastStretchedTime, currentStretchedTime;
  float lastLoopTime, currentLoopTime;
  
  Loopsequence(long length) {
    this.loopLength = length;
  }
  
  void start() {
    startTime = System.currentTimeMillis();
    playing = true;
  }
  
  public void makeStretchedTime() {
    lastTime = currentTime;
    currentTime = System.currentTimeMillis();
    long deltaT = currentTime-lastTime;
    
    lastStretchedTime = currentStretchedTime;
    currentStretchedTime += deltaT * speed;
    currentStretchedTime %= (loopLength * speed);
    
    lastLoopTime = currentLoopTime;
    currentLoopTime = currentStretchedTime / (loopLength * speed);
        
    if (lastStretchedTime > currentStretchedTime) {
      loopEnded = true;
    } else {
      loopEnded = false;
    }
    
    if (loopEnded) {
      for (int i=0;i<events.size();i++) {
        TimedEvent event = (TimedEvent)events.get(i);
        event.setNotPlayed();
      }
    }
  }
  
  public TimedEvent[] getCurrentEvents() {
    makeStretchedTime();
    
    ArrayList out = new ArrayList();
    
    for (int i=0;i<events.size();i++) {
      TimedEvent event = (TimedEvent)events.get(i);
      
      if (event.isBetween(lastLoopTime, currentLoopTime) && event.isNotPlayed()) {
        out.add(event);
        event.setPlayed();
      }
      
    }
    
    return (TimedEvent[])out.toArray(new TimedEvent[0]);
  }
  
  void add(TimedEvent event) {
    events.add(event);
  }
  
}
