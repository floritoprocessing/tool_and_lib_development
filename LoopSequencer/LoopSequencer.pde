Loopsequence seq;

void setup() {
  size(100,100,P3D);
  seq = new Loopsequence(4000);
  seq.add(new TimedEvent(0, "ONE"));
  seq.add(new TimedEvent(0.25, "TWO"));
  seq.add(new TimedEvent(0.5, "THREE"));
  seq.add(new TimedEvent(0.75, "FOUR"));
  
  seq.start();
}



void draw() {
  if (mousePressed) {
    if (mouseButton==LEFT) {
      seq.speed *= 1.01f;
    } else if (mouseButton==RIGHT) {
      seq.speed *= (1.0/1.01);
    }
  }
  
  background(255);
  //println(seq.currentStretchedTime);
  println(seq.getCurrentEvents());
}
