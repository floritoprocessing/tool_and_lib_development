class TimedEvent {
  
  float time; // percentage
  Object eventObject;
  boolean played = false;
  
  TimedEvent(float time, Object eventObject) {
    this.time = time;
    this.eventObject = eventObject;
  }
  
  public boolean isNotPlayed() {
    return !played;
  }
  
  public void setPlayed() {
    played = true;
  }
  
  public void setNotPlayed() {
    played = false;
  }
  
  public boolean isBetween(float lastTime, float cTime) {
    return (time>=lastTime && time<cTime);
  }
  
  public String toString() {
    return (eventObject+" at time "+time);
  }
  
}
