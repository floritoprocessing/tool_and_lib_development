// red grn research

PImage left,right,newleft,newright,combined;

int mode=0; boolean clicked;

void setup() {
  size(310,246);
  background(0);
  left=loadImage("sample_left.jpg");
  right=loadImage("sample_right.jpg");
  newleft=loadImage("sample_left.jpg");
  newright=loadImage("sample_right.jpg");
  combined=loadImage("sample_left.jpg");
  loadPixels();
  newleft.loadPixels();
  newright.loadPixels();

  // create 2 color images:
  background(0); colorMode(HSB,255); tint(0,255,255); image(left,0,0);
  for (int i=0;i<newleft.pixels.length;i++) { newleft.pixels[i]=pixels[i]; }
  background(0); colorMode(HSB,255); tint(64,255,255); image(right,0,0);
  for (int i=0;i<newright.pixels.length;i++) { newright.pixels[i]=pixels[i]; }
  newleft.updatePixels();
  newright.updatePixels();
  
  for (int i=0;i<combined.pixels.length;i++) {
    combined.pixels[i]=exclusion(left.pixels[i],right.pixels[i]);
  }
  combined.updatePixels();
  
  colorMode(RGB,255); noTint(); background(0);
  image(combined,0,0);
}

void draw() {
  
  if (clicked) { clicked=false;
    background(0);
    if (mode==0) {
      image(left,0,0);
    } else if (mode==1) {
      image(right,0,0);
    } else if (mode==2) {
      image(newleft,0,0);
    } else if (mode==3) {
      image(newright,0,0);
    } else  {
      image(combined,0,0);
    }
    mode++; if (mode==5) {mode=0;}
  }
}

void mousePressed() {clicked=true;}

////////// screening modes:

int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

int average(int a, int b) {
  int rr=(ch_red(a)+ch_red(b))>>1;
  int gg=(ch_grn(a)+ch_grn(b))>>1;
  int bb=(ch_blu(a)+ch_blu(b))>>1;
  return (rr<<16|gg<<8|bb);
}

int multiply(int a, int b) {
  int rr=(ch_red(a)*ch_red(b))>>8;
  int gg=(ch_grn(a)*ch_grn(b))>>8;
  int bb=(ch_blu(a)*ch_blu(b))>>8;
  return (rr<<16|gg<<8|bb);
}

int screen(int a, int b) {
  int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
  int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
  int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
  return (rr<<16|gg<<8|bb);
}

int darken(int a, int b) {
  int rr=ch_red(a)<ch_red(b)?ch_red(a):ch_red(b);
  int gg=ch_grn(a)<ch_grn(b)?ch_grn(a):ch_grn(b);
  int bb=ch_blu(a)<ch_blu(b)?ch_blu(a):ch_blu(b);
  return (rr<<16|gg<<8|bb);
}

int lighten(int a, int b) {
  int rr=ch_red(a)>ch_red(b)?ch_red(a):ch_red(b);
  int gg=ch_grn(a)>ch_grn(b)?ch_grn(a):ch_grn(b);
  int bb=ch_blu(a)>ch_blu(b)?ch_blu(a):ch_blu(b);
  return (rr<<16|gg<<8|bb);
}

int difference(int a, int b) {
  int rr=abs(ch_red(a)-ch_red(b));
  int gg=abs(ch_grn(a)-ch_grn(b));
  int bb=abs(ch_blu(a)-ch_blu(b));
  return (rr<<16|gg<<8|bb);
}

int negation(int a, int b) {
  int rr=255 - abs(255-ch_red(a)-ch_red(b));
  int gg=255 - abs(255-ch_grn(a)-ch_grn(b));
  int bb=255 - abs(255-ch_blu(a)-ch_blu(b));
  return (rr<<16|gg<<8|bb);
}

int exclusion(int a, int b) {
  int rr=ch_red(a)+ch_red(b) - ((ch_red(a)*ch_red(b))>>7);
  int gg=ch_grn(a)+ch_grn(b) - ((ch_grn(a)*ch_grn(b))>>7);
  int bb=ch_blu(a)+ch_blu(b) - ((ch_blu(a)*ch_blu(b))>>7);
  return (rr<<16|gg<<8|bb);
}

// overlay:
//if a < 128 then
//  result := (a*b) SHR 7
//else
//  result := 255 - ((255-a) * (255-b) SHR 7);

// hard light:
//if b < 128 then
//  result := (a*b) SHR 7
//else
//  result := 255 - ((255-b) * (255-a) SHR 7);

// color dodge:
//if b = 255 then
//  result := 255
//else begin
//  c := (a SHL 8) DIV (255-b);
//  if c > 255 then result := 255 else result := c;
//end;

// color burn:
//if b = 0 then
//  result := 0
//else begin
//  c := 255 - (((255-a) SHL 8) DIV b);
//  if c < 0 then result := 0 else result := c;
//end;

// inverse color dodge:
//if a = 255 then
//  result := 255
//else begin
//  c := (b SHL 8) DIV (255-a);
//  if c > 255 then result := 255 else result := c;
//end;

// inverse color burn:
//if a = 0 then
//  result := 0
//else begin
//  c := 255 - (((255-b) SHL 8) DIV a);
//  if c < 0 then result := 0 else result := c;
//end;

// check http://www.pegtop.net/delphi/blendmodes/#overlay for more
