Image16bit img=new Image16bit(400,300);
Brush brush1=new Brush(40);
Brush brush2=new Brush(5);

void setup() {
  size(400,300,P3D);
  img.setBoundariesCheck(Image16bit.BOUNDARIES_CHECK);
}

int z=0;

void draw() {
  print("making image nr.: "+z);
  img.clear();
  for (int i=0;i<5000;i++) {
    float x=random(width);
    float y=random(height);
    img.set(x,y,brush1,color16bit(0x02FF,0xFFFF,0x02EF),0.1*noise(10*x/float(width),10*y/float(height),0.001*z));
    img.set(x,y,brush2,color16bit(0x00FF,0x0100,0xFFFF),0.8*noise(10*x/float(width),10*y/float(height),0.001*z));
  }
  image(img.toPImage(),0,0);
  String filename="starfield_"+nf(z,3)+".tga";
  save(filename);
  println("\t "+filename+" saved.");
  z++;
}
