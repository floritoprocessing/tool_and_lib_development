boolean SAVE_IMAGES=false;

Image16bit img;
Brush brush1=new Brush(40);
Brush brush2=new Brush(5);

float a,b,c,d,e,f,z;

void setup() {
  size(768,576,P3D);
  img=new Image16bit(width,height);
  img.setBoundariesCheck(Image16bit.BOUNDARIES_CHECK);
  img.clear();
  next();
}

void next() {
  a = random(-4.0,4.0);
  b = random(-4.0,4.0);
  c = random(-4.0,4.0);
  d = random(-4.0,4.0);
  e = random(-4.0,4.0);
  f = random(-4.0,4.0);
  z = 0.0;
}

void mousePressed() {
  img.clear();
  next();
}

void draw() {
  img.fade();
  z+=0.001;
  for (int i=0;i<50;i++) {
    float x = random(-1.0,1.0);
    float y = random(-1.0,1.0);
    for (int j=0; j<1000; j++) {
      float xp = sin(a*y) + sin(b*x) + sin(c*z); 
      float yp = sin(d*x) + sin(e*y) + sin(f*z); 
      x=xp;
      y=yp;
      img.set(width/2.0+x*100,height/2.0+y*100,color16bit(0xFFFF-random(0x2F00),0x0500+random(0x4000),0x0500),0.1);
    }
  } 
  image(img.toPImage(),0,0);
  if (SAVE_IMAGES) saveFrame("Seed_#####.tga");
}
