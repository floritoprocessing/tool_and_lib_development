class Color16bit {
  int r=0, g=0, b=0;
  
  Color16bit() {
  }
  
  Color16bit(int _i) {
    r=_i;
    g=_i;
    b=_i;
  }
  
  Color16bit(int _r, int _g, int _b) {
    r=_r;
    g=_g;
    b=_b;
  }


  void setColor(Color16bit c) {
    setColor(c.r,c.g,c.b);
  }
  
  void setColor(int _i) {
    setColor(_i,_i,_i);
  }
    
  void setColor(int _r, int _g, int _b) {
    r=_r;
    g=_g;
    b=_b;
  }
  
  void addColor(Color16bit c) {
    addColor(c.r,c.g,c.b);
  }
  
  void addColor(int _r, int _g, int _b) {
    r+=_r;
    g+=_g;
    b+=_b;
    if (r>65535) r=65535; else if (r<0) r=0;
    if (g>65535) g=65535; else if (g<0) g=0;
    if (b>65535) b=65535; else if (b<0) b=0;
  }
  
  color toColor8bit() {
    int ro=(r&0xFF00)>>8;
    int go=(g&0xFF00)>>8;
    int bo=(b&0xFF00)>>8;
    return (ro<<16|go<<8|bo);
  }
  
  String toString() {
    return r+" "+g+" "+b;
  }
  
}


Color16bit color16bit(float r, float g, float b) {
  return new Color16bit((int)r,(int)g,(int)b);
}

Color16bit color16bit(int r, int g, int b) {
  return new Color16bit(r,g,b);
}

Color16bit color16bit(Color16bit c, float p) {  
  return new Color16bit(int(c.r*p),int(c.g*p),int(c.b*p));
}

Color16bit mixColor(Color16bit c1, Color16bit c2, float p1, float p2) {
  int r=(int)(c1.r*p1+c2.r*p2);
  int g=(int)(c1.g*p1+c2.g*p2);
  int b=(int)(c1.b*p1+c2.b*p2);
  if (r>65535) r=65535; else if (r<0) r=0;
  if (g>65535) g=65535; else if (g<0) g=0;
  if (b>65535) b=65535; else if (b<0) b=0;
  return new Color16bit(r,g,b);
}
