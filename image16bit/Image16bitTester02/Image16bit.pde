class Image16bit {
  int wid, hei;
  Color16bit[] pix;
  
  public static final boolean BOUNDARIES_CHECK=true;
  public static final boolean BOUNDARIES_DONT_CHECK=false;
  boolean boundariesCheck=BOUNDARIES_CHECK;
  
  Image16bit(int _w, int _h) {
    wid=_w;
    hei=_h;
    pix=new Color16bit[wid*hei];
    clear();    
  }
  
  void clear() {
    for (int i=0;i<pix.length;i++) pix[i]=new Color16bit();
  }
  
  void setBoundariesCheck(boolean b) {
    boundariesCheck=b;
  }
  
  void set(int x, int y, Color16bit c) {
    if (boundariesCheck) if (x<0||x>=wid||y<0||y>=hei) return;
    pix[x+y*wid].addColor(c);
  }
  
  void set(float x, float y, Color16bit c) {
    set(x,y,c,1.0);
  }
  
  void set(float x, float y, Color16bit c, float pressure) {
    if (boundariesCheck) if (x<0||x>=wid-1||y<0||y>=hei-1) return;
    int x0=(int)Math.floor(x);
    int x1=x0+1;
    int y0=(int)Math.floor(y);
    int y1=y0+1;
    float px1=x-x0;
    float px0=1.0-px1;
    float py1=y-y0;
    float py0=1.0-py1;
    float p00=pressure*px0*py0;
    float p01=pressure*px0*py1;
    float p10=pressure*px1*py0;
    float p11=pressure*px1*py1;
    set(x0,y0,color16bit(c,p00));
    set(x0,y1,color16bit(c,p01));
    set(x1,y1,color16bit(c,p11));
    set(x1,y0,color16bit(c,p10));
  }

  void set(float xb, float yb, Brush b, Color16bit c) {
    set(xb,yb,b,c,1.0);
  }  
  
  void set(float xb, float yb, Brush b, Color16bit c, float p) {
    for (int x=0;x<b.diameter;x++) {
      for (int y=0;y<b.diameter;y++) {
        float sx=xb+x-b.center;
        float sy=yb+y-b.center;
        set(sx,sy,c,p*b.press[x][y]);
      }
    }
  }
  
  
  
  
  
  Color16bit get(int x, int y) {
    return pix[x+y*wid];
  }
  
  void line(float x0, float y0, float x1, float y1, Color16bit c) {
    line(x0,y0,x1,y1,c,1.0);
  }
  
  void line(float x0, float y0, float x1, float y1, Brush b, Color16bit c, float pc) {
    float dx=x1-x0;
    float dy=y1-y0;
    if (dx!=0||dy!=0) {
      float d=sqrt(dx*dx+dy*dy);
      float pd=1.0/sqrt(d);
      for (float p=0;p<1;p+=1.0/d) {
        float x=x0+p*dx;
        float y=y0+p*dy;
        set(x,y,b,c,pc*pd);
      }
    }
  }
  
  void line(float x0, float y0, float x1, float y1, Color16bit c, float pc) {
    float dx=x1-x0;
    float dy=y1-y0;
    if (dx!=0||dy!=0) {
      float d=sqrt(dx*dx+dy*dy);
      for (float p=0;p<1;p+=1.0/d) {
        float x=x0+p*dx;
        float y=y0+p*dy;
        set(x,y,c,pc);
      }
    }
  }
    
  void fillWithCrap() {
    for (int i=0;i<pix.length;i++) {
      pix[i]=new Color16bit((int)(random(0xFFFF)));
    }
  }
  
  void fade() {
    for (int i=0;i<pix.length;i++) {
      pix[i]=mixColor(pix[i],color16bit(0,0,0),0.8,0.2);
    }
  }
  
  PImage toPImage() {
    PImage out=new PImage(wid,hei);
    for (int i=0;i<pix.length;i++) out.pixels[i]=pix[i].toColor8bit();
    return out;
  }
  
}
