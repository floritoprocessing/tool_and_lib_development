Image16bit img=new Image16bit(640,480);
Brush brush1=new Brush(40);
Brush brush2=new Brush(5);

void setup() {
  size(640,480,P3D);
  img.setBoundariesCheck(Image16bit.BOUNDARIES_CHECK);
  //  img.fillWithCrap();
}

void mouseMoved() {
  redraw();
}

float rd1=0, rd2=0;
float x,y,lx,ly;

void draw() {
  for (int z=0;z<100;z++) {
    img.clear();
    for (int i=0;i<10000;i++) {
      rd1+=0.025;
      rd2+=0.00625+0.00005;
      lx=x;
      ly=y;
      //x=width*(2.0+sin(rd1)+sin(rd2))/4.0;
      //y=height*(2.0+cos(rd1)+cos(rd2))/4.0;
      x=random(width);
      y=random(height);
      img.set(x,y,brush1,color16bit(0x02FF,0xFFFF,0x02EF),0.1*noise(10*x/float(width),10*y/float(height),0.1*z));
      img.set(x,y,brush2,color16bit(0x00FF,0x0100,0xFFFF),0.8*noise(10*x/float(width),10*y/float(height),0.1*z));
    }
    
    image(img.toPImage(),0,0);
    save("starfield_"+nf(z,3)+".tga");
    
  }
  
}
