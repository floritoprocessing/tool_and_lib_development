
class Brush {
  int diameter=2;
  float center=0.5;
  
  float[][] press;
  
  Brush(int _d) {
    diameter=_d;
    center=diameter/2.0-0.5;
    press=new float[diameter][diameter];
    float maxD=sqrt(center*center+center*center);
    for (int x=0;x<diameter;x++) {
      for (int y=0;y<diameter;y++) {
        float dx=center-x;
        float dy=center-y;
        float d=sqrt(dx*dx+dy*dy);
        float p=1.0-d/maxD;
        press[x][y]=p*p*p;
      }
    } 
  }
  
  void set(Image16bit im, float xb, float yb, Color16bit c) {
    set(im,xb,yb,c,1.0);
  }
  
  void set(Image16bit im, float xb, float yb, Color16bit c, float p) {
    for (int x=0;x<diameter;x++) {
      for (int y=0;y<diameter;y++) {
        float sx=xb+x-center;
        float sy=yb+y-center;
        im.set(sx,sy,c,p*press[x][y]);
      }
    }
  }
  
}
