import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import processing.core.PApplet;
import sftp.Sftp;


public class FTP extends PApplet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"FTP"});
	}
	
	private static final int DELETE_ALL_FILES_IN_REMOTE_DIRECTORY = 0;
	private static final int COMPARE_DIRECTORIES = 1;
	private static final int COMPARE_AND_DELETE_REMOTE = 2;
	
	String host = "sci2.esa.int";
	String user = "sremedia";
	String pass = "watchm3";
	String subDirectory = "png_fly_stereo2";
	
	int mode = COMPARE_AND_DELETE_REMOTE;
	
	File localDirectory = new File(
		"C:\\Documents and Settings\\mgraf\\My Documents\\" +
		"Bjorn Grieger Moon Movie\\png_fly\\png_fly_stereo2\\"); 
	
	FTPConnection ftp;
		
	String[] remoteList;
	ArrayList<String> localList = new ArrayList<String>();
	
	int index = 0;
	
	public void setup() {
		size(400,400,P3D);
		background(0);
		
		/*
		 * Get remote list
		 */
		ftp = new FTPConnection(false);
		try {
			ftp.connect(host);
			if (ftp.login(user, pass)) {
				println("connected");
				
				String current = ftp.getCurrentDirectory();
				println(current);
				String list = ftp.listSubdirectories();//listFiles("");
				println("SUBDIRECTORIES:");
				println(list);
				
				println("CHANGING TO "+subDirectory);
				println(ftp.changeDirectory(subDirectory));
				list = ftp.listFiles();
				remoteList = list.split("\n");
				Arrays.sort(remoteList);
				println("Files in this directory: "+remoteList.length);
			}
		} catch (Exception e) {
			ftp.disconnect();
		}
		
		/*
		 * Get local list
		 */
		if (mode==COMPARE_DIRECTORIES || mode==COMPARE_AND_DELETE_REMOTE) {
			File[] lst = localDirectory.listFiles();
			for (File f:lst) {
				if (f.isFile()) {
					localList.add(f.getName());
				}
			}
			Collections.sort(localList);
		}
	}
	
	public void mousePressed() {
		close();
	}
	
	private void close() {
		try {
			ftp.logout();
			ftp.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
	
	public void draw() {
		for (int ii=0;ii<10;ii++) {
			if (mode==DELETE_ALL_FILES_IN_REMOTE_DIRECTORY) {
				//int index = frameCount-1;
				if (index<remoteList.length) {
					print("deleting "+remoteList[index]+"" +
							" "+(remoteList.length-index)+"" +
									" ("+nf(100.0f*index/remoteList.length,1,1)+"%) ");
					try {
						println(ftp.deleteFile(remoteList[index]));
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					close();
				}
			}
			
			else if (mode==COMPARE_DIRECTORIES || mode==COMPARE_AND_DELETE_REMOTE) {
				//int index = frameCount-1;
				if (index<remoteList.length) {
					print("downloaded "+remoteList[index]+"?");
					print(" "+(index+1)+"/"+remoteList.length);
					if (localList.contains(remoteList[index])) {
						print(" YES");
						if (mode==COMPARE_AND_DELETE_REMOTE) {
							boolean success = false;
							try {
								success = ftp.deleteFile(remoteList[index]);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if (success) {
								print(" - remote file deleted");	
							} else {
								print(" - problem deleteing remote file");
							}
							localList.remove(remoteList[index]);
						}
					} else {
						print(" no");
					}
					println();
				} else {
					close();
				}
				
				//println(localList[index]+"\t-> remote:\t"+remoteList[index]);
			}
			
			else {
				close();
			}
			
			index++;
		}
	}

}
