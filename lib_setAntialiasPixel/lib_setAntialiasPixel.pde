// pixel anti-alias

void setup() {
  size(100,100);
  colorMode(RGB,255);
}

float px=50, py=50;
float inc=0.15;

void draw() { 
  background(255,255,255);
  setAntialias(px,py,color(255,0,0));
}

void keyPressed() {
  if (key=='a') {px-=inc;} if (key=='d') {px+=inc;}
  if (key=='w') {py-=inc;} if (key=='s') {py+=inc;}
}

void setAntialias(float _x, float _y, color _c) {
  int[] aapx=new int[4];
  int[] aapy=new int[4];
  float[] aapd=new float[4];
  
  //antialias pixel x/y positions:
  aapx[0]=int(floor(_x)); aapy[0]=int(floor(_y));
  aapx[1]=aapx[0]+1; aapy[1]=aapy[0];
  aapx[2]=aapx[0]; aapy[2]=aapy[0]+1;
  aapx[3]=aapx[0]+1; aapy[3]=aapy[0]+1;
  
  float xPartLeft=1.0-(_x-aapx[0]);
  float yPartTop=1.0-(_y-aapy[0]);
  
  aapd[0]=xPartLeft*yPartTop;
  aapd[1]=(1.0-xPartLeft)*yPartTop;
  aapd[2]=xPartLeft*(1.0-yPartTop);
  aapd[3]=(1.0-xPartLeft)*(1.0-yPartTop);
  
  for (int i=0;i<4;i++) {
    int px=int(aapx[i]), py=int(aapy[i]);
    set(px%width,py%height,mixColor(_c,get(px%width,py%height),aapd[i]));
  }
}

color mixColor(color c1, color c2, float p) {
  float q=1.0-p;
  return color(p*red(c1)+q*red(c2),p*green(c1)+q*green(c2),p*blue(c1)+q*blue(c2));
}
