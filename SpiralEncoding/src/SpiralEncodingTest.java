import java.awt.geom.Point2D.Float;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import net.florito.spiralencoding.SpiralSystem;
import processing.core.PApplet;

/**
 * @author Marcus
 *
 */
public class SpiralEncodingTest extends PApplet {
	
	
	float spiralLen = 20000;
	SpiralSystem spiral = new SpiralSystem(0.03);
	Float[] screenSpiralPoints;
	
	
	class LenPoint implements Comparable<LenPoint> {
		float len;
		Float point;
		LenPoint(float len, Float point) {
			this.len = len;
			this.point = point;
		}
		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(LenPoint arg0) {
			if (len<arg0.len) return -1;
			else if (len>arg0.len) return 1;
			else return 0;
		}
	}
	
	Vector<LenPoint> lengths = new Vector<LenPoint>();
	boolean moveFirst = false;
	boolean moveLast = false;
	float moveStep = 1;
	
	public void setup() {
		size(600,600,JAVA2D);
		//smooth();
		
		
		spiral.setDegreeStep(0.5);
		
		screenSpiralPoints = spiral.createSpiral(spiralLen, 10);
		screenSpiralPoints = reducePoints(screenSpiralPoints);
		
		float len = random(0,spiralLen*0.5f);
		Float point = spiral.getPointAt(len);
		lengths.add(new LenPoint(len, point));
		len = random(spiralLen*0.5f,spiralLen);
		point = spiral.getPointAt(len);
		lengths.add(new LenPoint(len, point));
		
	}
	
	public void keyPressed() {
		if (key=='f') {
			moveFirst = !moveFirst;
		} else if (key=='l') {
			moveLast = !moveLast;
		}
	}
	
	public void mousePressed() {
		boolean addSubPoints = false;
		boolean addOnePoint = true;
		if (addSubPoints) {
			//divide;
			Vector<LenPoint> newLengths = new Vector<LenPoint>();
			for (int i=0;i<lengths.size()-1;i++) {
				float f0 = lengths.get(i).len;
				float f1 = lengths.get(i+1).len;
				float len = (f0+f1)*0.5f;
				Float point = spiral.getPointAt(len);
				newLengths.add(new LenPoint(len, point));
			}
			lengths.addAll(newLengths);
			Collections.sort(lengths);
		}
		
		if (addOnePoint) {
			int inbetweenPoints = lengths.size()-1;
			lengths = createPoints(lengths, lengths.size()+1);
		}
	}

	/**
	 * @param inbetweenPoints
	 * @return
	 */
	private Vector<LenPoint> createPoints(Vector<LenPoint> lengths, int newLength) {
		int inbetweenPoints = newLength-2;
		float divider = inbetweenPoints+1;
		Vector<LenPoint> newLengths = new Vector<LenPoint>();
		float len0 = lengths.get(0).len;
		float len1 = lengths.get(lengths.size()-1).len;
		//System.out.println(len0+".."+len1);
		newLengths.add(lengths.get(0));
		for (int i=0;i<inbetweenPoints;i++) {
			// divider=2, inbetweenPoints=1	-> 0.5
			// divider=3, inbetweenPoints=2 -> 0.333 0.666
			// divider=4, inbetweenPoints=3 -> 0.25 0.5 0.75
			float perc = (float)(i+1)/divider;
			float len = len0 + perc*(len1-len0);
			//System.out.println(len+" ("+(perc*100)+"%)");
			Float point = spiral.getPointAt(len);
			newLengths.add(new LenPoint(len, point));
		}
		newLengths.add(lengths.get(lengths.size()-1));
		return newLengths;
	}
	
	public void draw() {
		background(240);
		translate(width/2,height/2);
		noFill();
		stroke(192);
		strokeWeight(1);
		beginShape();
		for (Float point:screenSpiralPoints) {
			vertex(point.x, point.y);
		}
		endShape();
		
		stroke(64,0,0,128);
		for (int i=0;i<2;i++) {
			strokeWeight(i==0?2:8);
			beginShape(i==0?POLYGON:POINTS);
			int pi = 0;
			for (LenPoint lp:lengths) {
				Float pt = lp.point;
				if (i==0) {
					if (pi==0 || pi==lengths.size()-1) {
						curveVertex(pt.x, pt.y);
					}
					curveVertex(pt.x, pt.y);
					pi++;
				} else {
					vertex(pt.x, pt.y);
				}
			}
			endShape();
		}
		
		if (moveFirst) {
			float len = lengths.get(0).len;
			len += moveStep;
			Float point = spiral.getPointAt(len);
			lengths.get(0).len = len;
			lengths.get(0).point = point;
		}
		if (moveLast) {
			float len = lengths.get(lengths.size()-1).len;
			len -= moveStep;
			Float point = spiral.getPointAt(len);
			lengths.get(lengths.size()-1).len = len;
			lengths.get(lengths.size()-1).point = point;
		}
		if (moveLast||moveFirst) {
			lengths = createPoints(lengths, lengths.size());
		}
		if (!moveLast && !moveFirst) {
			moveStep = 1.0f;
		} else {
			moveStep *= 1.01;
		}
		
	}


	/**
	 * @param points
	 * @return
	 */
	private Float[] reducePoints(Float[] points) {
		Vector<Float> pts = new Vector(Arrays.asList(points));
		Vector<Float> ptsNew = new Vector<Float>();
		float inset = -20;
		float w2 = width/2, h2 = height/2;
		for (int i=0;i<pts.size();i++) {
			Float pt = pts.get(i);
			if (pt.x>=-w2+inset&&pt.x<w2-inset&&pt.y>=-h2+inset&&pt.y<=h2-inset) {
				ptsNew.add(pt);
			}
		}
		println("Reducing "+points.length+" points to "+ptsNew.size());
		points = ptsNew.toArray(new Float[0]);
		return points;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"SpiralEncodingTest"});
	}
}
