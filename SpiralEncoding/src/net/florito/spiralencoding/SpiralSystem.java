package net.florito.spiralencoding;

import java.awt.geom.Point2D.Float;
import java.util.Vector;

public class SpiralSystem {
	
	private double degreeStep = 5f;
	private double radiusIncreasePerDegree;
	
	public SpiralSystem(double radiusIncreasePerDegree) {
		this.radiusIncreasePerDegree = radiusIncreasePerDegree;
	}
	
	/**
	 * @param degreeStep the degreeStep to set
	 */
	public void setDegreeStep(double degreeStep) {
		this.degreeStep = degreeStep;
	}
	
	private Vector<Float> tempSpiralPoints = null;
	private float tempSpiralStep = 0, tempLen = 0;
	public Float[] createSpiral(float length, float step) {
		synchronized (this) {
			tempSpiralPoints = new Vector<Float>();
			tempSpiralStep = step;
			tempLen = 0;
			getPointAt(length);
			Float[] points = tempSpiralPoints.toArray(new Float[0]);
			tempSpiralPoints = null;
			return points;
		}
	}
	
	public Float getPointAt(float length) {
		double len = 0;
		double degree = 0;
		double radius = 0;
		Float currentPoint = new Float();
		Float lastPoint = null;
		while (len<length) {
			lastPoint = currentPoint;
			double rd = degToRad(degree);
			float x = (float)(radius*Math.cos(rd));
			float y = (float)(radius*Math.sin(rd));
			currentPoint = new Float(x, y);
			if (tempSpiralPoints!=null) {
				if (tempLen>tempSpiralStep) {
					while (tempLen>tempSpiralStep) {
						tempLen-=tempSpiralStep;
					}
					tempSpiralPoints.add(currentPoint);
				}
			}
			degree += degreeStep;
			radius += radiusIncreasePerDegree * degreeStep;
			if (lastPoint!=null) {
				double dist = currentPoint.distance(lastPoint); 
				len += dist;
				if (tempSpiralPoints!=null) {
					tempLen += dist;
				}
			}
		}
		
		return currentPoint;
		
	}
	
	private static double degToRad(double degree) {
		return Math.PI*degree/180.0;
	}
}