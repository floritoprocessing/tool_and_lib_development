customFunction cf=new customFunction();

void setup() {
  //for (double t=0; t<cf.getTimeAtIndex(cf.last); t+=30) {
  //for (double t=0; t<=cf.getTimeAtIndex(5); t+=30) {
  //for (double t=cf.getTimeAtIndex(5); t<cf.getTimeAtIndex(78); t+=30) {
  //for (double t=0; t<cf.getTimeAtIndex(cf.last); t+=30) {
  for (double t=0; t<cf.getTimeAtIndex(cf.last); t+=150) {
    //println((cf.getForT_is(t)).toString2());
    println((cf.getForT_is(t)).alt);
    //println((cf.getForT_is(t)).vrel+"\t"+(cf.getForT_is(t)).pf);
    //println(0);
  }
}

void draw() {
}

class FuncPoint {
  double ti, x, y, z, alt, vrel;
  boolean pf;
  FuncPoint(double _ti, boolean _pf, double _x, double _y, double _z, double _alt, double _vrel) {
    ti=_ti; 
    pf=_pf;
    x=_x; 
    y=_y; 
    z=_z;
    alt=_alt;
    vrel=_vrel;
  }

  String toString1() {
    return ("time: "+ti+" - pf: "+pf+", x: "+x+", y: "+y+", z: "+z+", alt: "+alt+", vrel: "+vrel);
  }

  String toString2() {
    return (ti+"\t"+pf+"\t"+x+"\t"+y+"\t"+z+"\t"+alt+"\t"+vrel);
  }
}

class customFunction {
  FuncPoint[] funcPoint;
  int first=0, last=170;

  customFunction() {
    funcPoint=new FuncPoint[last+1];
    // data is: time[s], powered flight, x[km], y[km], z[km], alt above average earth surface[km], rel speed[m/s]
    funcPoint[0]=new FuncPoint(533.51,true,-4088.08,267.23,5117.31,177.06,7455.18);
    funcPoint[1]=new FuncPoint(563.51,true,-4070.96,46.19,5138.45,177.66,7454.95);
    funcPoint[2]=new FuncPoint(588.51,true,-4053.86,-138.02,5150.82,178.06,7454.89);
    funcPoint[3]=new FuncPoint(591.21,true,-4051.40,-157.76,5152.31,178.16,7456.43);
    funcPoint[4]=new FuncPoint(608.72,true,-4037.13,-287.26,5158.05,178.26,7500.39);
    funcPoint[5]=new FuncPoint(618.72,true,-4028.43,-361.65,5160.17,178.26,7501.09);
    funcPoint[6]=new FuncPoint(678.72,false,-3966.36,-805.52,5158.05,178.26,7501.09);
    funcPoint[7]=new FuncPoint(738.72,false,-3888.49,-1246.21,5129.75,178.36,7501.20);
    funcPoint[8]=new FuncPoint(798.72,false,-3794.60,-1680.74,5075.93,178.46,7501.44);
    funcPoint[9]=new FuncPoint(858.72,false,-3684.91,-2107.81,4996.88,178.56,7501.78);
    funcPoint[10]=new FuncPoint(918.72,false,-3561.15,-2526.10,4891.82,178.76,7502.23);
    funcPoint[11]=new FuncPoint(978.72,false,-3423.20,-2933.00,4761.94,179.06,7502.78);
    funcPoint[12]=new FuncPoint(1038.72,false,-3270.92,-3326.19,4608.37,179.26,7503.39);
    funcPoint[13]=new FuncPoint(1098.72,false,-3105.34,-3704.74,4431.16,179.56,7504.07);
    funcPoint[14]=new FuncPoint(1158.72,false,-2928.06,-4065.84,4231.16,179.86,7504.80);
    funcPoint[15]=new FuncPoint(1218.72,false,-2738.68,-4408.44,4009.73,180.26,7505.56);
    funcPoint[16]=new FuncPoint(1278.72,false,-2537.47,-4730.37,3768.48,180.56,7506.31);
    funcPoint[17]=new FuncPoint(1338.72,false,-2326.75,-5030.94,3506.81,180.96,7507.06);
    funcPoint[18]=new FuncPoint(1398.72,false,-2106.91,-5307.92,3227.06,181.36,7507.78);
    funcPoint[19]=new FuncPoint(1458.72,false,-1877.68,-5560.14,2931.11,181.76,7508.45);
    funcPoint[20]=new FuncPoint(1518.72,false,-1641.63,-5785.98,2620.11,182.16,7509.06);
    funcPoint[21]=new FuncPoint(1578.72,false,-1398.23,-5984.86,2295.46,182.56,7509.58);
    funcPoint[22]=new FuncPoint(1638.72,false,-1149.74,-6155.44,1958.76,182.96,7510.00);
    funcPoint[23]=new FuncPoint(1698.72,false,-897.26,-6296.56,1612.91,183.36,7510.31);
    funcPoint[24]=new FuncPoint(1758.72,false,-639.64,-6408.40,1257.69,183.76,7510.49);
    funcPoint[25]=new FuncPoint(1818.72,false,-381.01,-6489.69,895.13,184.06,7510.54);
    funcPoint[26]=new FuncPoint(1878.72,false,-119.87,-6540.09,529.74,184.46,7510.45);
    funcPoint[27]=new FuncPoint(1938.72,false,140.84,-6559.43,160.35,184.76,7510.20);
    funcPoint[28]=new FuncPoint(1998.72,false,401.61,-6547.55,-209.59,185.06,7509.82);
    funcPoint[29]=new FuncPoint(2058.72,false,659.56,-6504.57,-578.89,185.36,7509.28);
    funcPoint[30]=new FuncPoint(2118.72,false,915.21,-6430.58,-945.26,185.66,7508.61);
    funcPoint[31]=new FuncPoint(2178.72,false,1166.78,-6326.21,-1305.28,185.86,7507.80);
    funcPoint[32]=new FuncPoint(2238.72,false,1413.49,-6191.49,-1660.18,186.06,7506.87);
    funcPoint[33]=new FuncPoint(2298.72,false,1653.55,-6027.82,-2005.62,186.26,7505.84);
    funcPoint[34]=new FuncPoint(2358.72,false,1888.10,-5835.22,-2340.77,186.46,7504.70);
    funcPoint[35]=new FuncPoint(2418.72,false,2112.99,-5615.57,-2663.82,186.56,7503.50);
    funcPoint[36]=new FuncPoint(2478.72,false,2330.30,-5369.58,-2972.19,186.66,7502.24);
    funcPoint[37]=new FuncPoint(2538.72,false,2537.32,-5097.99,-3266.56,186.76,7500.96);
    funcPoint[38]=new FuncPoint(2598.72,false,2734.74,-4802.29,-3543.78,186.86,7499.64);
    funcPoint[39]=new FuncPoint(2658.72,false,2920.02,-4484.43,-3802.98,186.86,7498.35);
    funcPoint[40]=new FuncPoint(2718.72,false,3093.91,-4146.26,-4041.88,186.96,7497.10);
    funcPoint[41]=new FuncPoint(2778.72,false,3254.84,-3788.11,-4261.08,186.96,7495.89);
    funcPoint[42]=new FuncPoint(2838.72,false,3402.43,-3413.14,-4458.08,186.96,7494.76);
    funcPoint[43]=new FuncPoint(2898.72,false,3536.31,-3022.43,-4632.49,186.96,7493.73);
    funcPoint[44]=new FuncPoint(2958.72,false,3656.21,-2617.59,-4783.40,186.96,7492.82);
    funcPoint[45]=new FuncPoint(3018.72,false,3760.30,-2200.87,-4910.89,186.96,7492.04);
    funcPoint[46]=new FuncPoint(3078.72,false,3850.15,-1774.13,-5012.92,186.96,7491.40);
    funcPoint[47]=new FuncPoint(3138.72,false,3924.51,-1339.07,-5089.76,186.96,7490.91);
    funcPoint[48]=new FuncPoint(3198.72,false,3983.23,-898.39,-5140.75,186.96,7490.60);
    funcPoint[49]=new FuncPoint(3258.72,false,4025.42,-453.66,-5166.31,186.96,7490.46);
    funcPoint[50]=new FuncPoint(3318.72,false,4051.80,-7.07,-5165.60,186.96,7490.48);
    funcPoint[51]=new FuncPoint(3378.72,false,4061.28,439.76,-5139.25,186.86,7490.68);
    funcPoint[52]=new FuncPoint(3438.72,false,4054.90,884.11,-5086.79,186.86,7491.06);
    funcPoint[53]=new FuncPoint(3498.72,false,4031.72,1324.00,-5009.14,186.86,7491.60);
    funcPoint[54]=new FuncPoint(3558.72,false,3992.85,1758.56,-4905.49,186.86,7492.30);
    funcPoint[55]=new FuncPoint(3618.72,false,3937.12,2184.18,-4777.83,186.86,7493.13);
    funcPoint[56]=new FuncPoint(3678.72,false,3865.51,2599.47,-4625.85,186.76,7494.08);
    funcPoint[57]=new FuncPoint(3738.72,false,3778.07,3003.06,-4450.37,186.76,7495.16);
    funcPoint[58]=new FuncPoint(3798.72,false,3675.14,3392.51,-4252.16,186.66,7496.33);
    funcPoint[59]=new FuncPoint(3858.72,false,3557.09,3766.78,-4031.76,186.66,7497.56);
    funcPoint[60]=new FuncPoint(3918.72,false,3423.89,4122.63,-3791.59,186.56,7498.85);
    funcPoint[61]=new FuncPoint(3978.72,false,3276.65,4458.99,-3531.99,186.46,7500.16);
    funcPoint[62]=new FuncPoint(4038.72,false,3115.83,4774.20,-3254.43,186.36,7501.49);
    funcPoint[63]=new FuncPoint(4098.72,false,2941.15,5067.63,-2959.74,186.26,7502.80);
    funcPoint[64]=new FuncPoint(4158.72,false,2754.47,5336.69,-2650.04,186.16,7504.07);
    funcPoint[65]=new FuncPoint(4218.72,false,2556.24,5580.74,-2325.60,185.96,7505.29);
    funcPoint[66]=new FuncPoint(4278.72,false,2346.17,5798.23,-1990.18,185.76,7506.43);
    funcPoint[67]=new FuncPoint(4338.72,false,2126.35,5988.01,-1644.53,185.56,7507.48);
    funcPoint[68]=new FuncPoint(4398.72,false,1896.62,6149.77,-1289.46,185.36,7508.41);
    funcPoint[69]=new FuncPoint(4458.72,false,1658.62,6282.07,-928.18,185.16,7509.23);
    funcPoint[70]=new FuncPoint(4518.72,false,1413.03,6384.32,-562.87,184.86,7509.91);
    funcPoint[71]=new FuncPoint(4578.72,false,1161.65,6456.17,-193.55,184.56,7510.44);
    funcPoint[72]=new FuncPoint(4638.72,false,903.91,6497.46,176.36,184.26,7510.83);
    funcPoint[73]=new FuncPoint(4698.72,false,642.67,6507.72,545.68,183.96,7511.07);
    funcPoint[74]=new FuncPoint(4758.72,false,377.45,6487.03,912.08,183.56,7511.14);
    funcPoint[75]=new FuncPoint(4818.72,false,111.21,6435.70,1273.33,183.26,7511.09);
    funcPoint[76]=new FuncPoint(4878.72,false,-157.50,6353.78,1628.33,182.86,7510.89);
    funcPoint[77]=new FuncPoint(4938.72,false,-425.52,6241.78,1974.99,182.46,7510.56);
    funcPoint[78]=new FuncPoint(4980.74,true,-612.37,6145.99,2211.48,182.26,7510.26);
    funcPoint[79]=new FuncPoint(4990.74,true,-657.41,6121.20,2266.33,182.16,7510.43);
    funcPoint[80]=new FuncPoint(5000.74,true,-702.00,6095.19,2322.08,182.06,7510.59);
    funcPoint[81]=new FuncPoint(5010.74,true,-746.24,6068.87,2376.60,182.06,7510.75);
    funcPoint[82]=new FuncPoint(5020.74,true,-790.03,6041.65,2430.89,181.96,7510.91);
    funcPoint[83]=new FuncPoint(5035.74,true,-855.92,5999.02,2512.52,181.86,7511.14);
    funcPoint[84]=new FuncPoint(5038.44,true,-868.71,5991.41,2526.26,181.86,7512.82);
    funcPoint[85]=new FuncPoint(5040.74,true,-878.40,5984.63,2538.94,181.86,7518.91);
    funcPoint[86]=new FuncPoint(5050.74,true,-922.90,5954.73,2592.64,181.76,7545.54);
    funcPoint[87]=new FuncPoint(5060.74,true,-966.90,5923.93,2646.09,181.56,7572.45);
    funcPoint[88]=new FuncPoint(5070.74,true,-1010.39,5892.44,2699.37,181.46,7599.63);
    funcPoint[89]=new FuncPoint(5080.74,true,-1055.38,5859.71,2752.39,181.26,7627.10);
    funcPoint[90]=new FuncPoint(5090.74,true,-1099.91,5826.66,2804.16,181.06,7654.82);
    funcPoint[91]=new FuncPoint(5100.74,true,-1143.76,5792.35,2856.74,180.86,7682.82);
    funcPoint[92]=new FuncPoint(5110.74,true,-1188.04,5757.05,2909.09,180.66,7711.06);
    funcPoint[93]=new FuncPoint(5120.74,true,-1232.69,5720.75,2961.21,180.46,7739.55);
    funcPoint[94]=new FuncPoint(5130.74,true,-1277.69,5683.43,3013.09,180.26,7768.29);
    funcPoint[95]=new FuncPoint(5140.74,true,-1322.16,5645.93,3063.76,180.16,7797.27);
    funcPoint[96]=new FuncPoint(5150.74,true,-1366.81,5606.89,3115.21,180.06,7826.50);
    funcPoint[97]=new FuncPoint(5160.74,true,-1411.73,5566.83,3166.41,179.96,7855.94);
    funcPoint[98]=new FuncPoint(5170.74,true,-1456.06,5526.60,3216.40,179.96,7885.62);
    funcPoint[99]=new FuncPoint(5180.74,true,-1500.47,5484.80,3267.15,179.96,7915.51);
    funcPoint[100]=new FuncPoint(5190.74,true,-1545.23,5442.59,3316.70,180.06,7945.62);
    funcPoint[101]=new FuncPoint(5200.74,true,-1589.99,5398.86,3367.03,180.26,7975.95);
    funcPoint[102]=new FuncPoint(5210.74,true,-1635.06,5354.71,3416.17,180.56,8006.49);
    funcPoint[103]=new FuncPoint(5220.74,true,-1679.29,5309.88,3465.11,180.96,8037.24);
    funcPoint[104]=new FuncPoint(5230.74,true,-1724.51,5263.69,3513.79,181.36,8068.18);
    funcPoint[105]=new FuncPoint(5240.74,true,-1768.86,5216.89,3562.32,181.96,8099.32);
    funcPoint[106]=new FuncPoint(5250.74,true,-1814.13,5168.78,3610.64,182.66,8130.65);
    funcPoint[107]=new FuncPoint(5260.74,true,-1858.68,5120.59,3657.80,183.46,8162.18);
    funcPoint[108]=new FuncPoint(5270.74,true,-1902.98,5070.87,3705.76,184.46,8193.89);
    funcPoint[109]=new FuncPoint(5280.74,true,-1948.30,5020.41,3752.57,185.56,8225.78);
    funcPoint[110]=new FuncPoint(5290.74,true,-1992.63,4969.35,3799.24,186.86,8257.87);
    funcPoint[111]=new FuncPoint(5300.74,true,-2036.83,4917.35,3845.77,188.36,8290.12);
    funcPoint[112]=new FuncPoint(5310.74,true,-2081.95,4864.59,3891.17,189.96,8322.56);
    funcPoint[113]=new FuncPoint(5320.74,true,-2126.86,4810.87,3936.44,191.76,8355.17);
    funcPoint[114]=new FuncPoint(5330.74,true,-2171.56,4756.26,3981.64,193.86,8387.96);
    funcPoint[115]=new FuncPoint(5340.74,true,-2215.95,4700.61,4026.65,196.06,8420.92);
    funcPoint[116]=new FuncPoint(5350.74,true,-2260.36,4644.70,4070.69,198.56,8454.05);
    funcPoint[117]=new FuncPoint(5360.74,true,-2304.39,4587.75,4114.56,201.16,8487.35);
    funcPoint[118]=new FuncPoint(5370.74,true,-2348.90,4529.55,4158.43,204.16,8520.82);
    funcPoint[119]=new FuncPoint(5380.74,true,-2393.32,4471.01,4201.30,207.36,8554.47);
    funcPoint[120]=new FuncPoint(5390.74,true,-2437.28,4411.48,4244.07,210.76,8588.29);
    funcPoint[121]=new FuncPoint(5400.74,true,-2481.58,4350.66,4286.86,214.56,8622.30);
    funcPoint[122]=new FuncPoint(5410.74,true,-2525.69,4289.49,4328.69,218.56,8656.46);
    funcPoint[123]=new FuncPoint(5420.74,true,-2569.67,4228.03,4369.64,222.86,8690.81);
    funcPoint[124]=new FuncPoint(5430.74,true,-2613.82,4165.18,4410.58,227.46,8725.34);
    funcPoint[125]=new FuncPoint(5440.74,true,-2658.11,4100.97,4451.58,232.46,8760.05);
    funcPoint[126]=new FuncPoint(5450.74,true,-2701.46,4036.91,4491.73,237.76,8794.95);
    funcPoint[127]=new FuncPoint(5460.74,true,-2745.96,3971.60,4531.04,243.36,8830.04);
    funcPoint[128]=new FuncPoint(5470.74,true,-2789.10,3905.85,4570.45,249.36,8865.33);
    funcPoint[129]=new FuncPoint(5480.74,true,-2832.67,3839.34,4609.13,255.76,8900.83);
    funcPoint[130]=new FuncPoint(5490.74,true,-2876.61,3771.99,4647.01,262.46,8936.53);
    funcPoint[131]=new FuncPoint(5500.74,true,-2920.46,3703.25,4685.09,269.66,8972.45);
    funcPoint[132]=new FuncPoint(5510.74,true,-2963.34,3634.71,4722.48,277.26,9008.60);
    funcPoint[133]=new FuncPoint(5520.74,true,-3007.13,3564.76,4759.12,285.16,9044.98);
    funcPoint[134]=new FuncPoint(5530.74,true,-3050.11,3493.94,4796.05,293.66,9081.61);
    funcPoint[135]=new FuncPoint(5540.74,true,-3093.23,3423.35,4831.47,302.46,9118.49);
    funcPoint[136]=new FuncPoint(5550.74,true,-3136.69,3350.77,4867.20,311.86,9155.64);
    funcPoint[137]=new FuncPoint(5560.74,true,-3179.16,3278.35,4902.32,321.66,9193.07);
    funcPoint[138]=new FuncPoint(5570.74,true,-3222.40,3204.45,4936.91,331.96,9230.80);
    funcPoint[139]=new FuncPoint(5580.74,true,-3265.25,3130.17,4970.99,342.76,9268.85);
    funcPoint[140]=new FuncPoint(5590.74,true,-3307.70,3055.47,5004.57,354.06,9307.21);
    funcPoint[141]=new FuncPoint(5600.74,true,-3350.29,2979.76,5037.68,365.86,9345.93);
    funcPoint[142]=new FuncPoint(5610.74,true,-3393.68,2903.60,5069.61,378.26,9385.00);
    funcPoint[143]=new FuncPoint(5620.74,true,-3435.98,2826.37,5101.87,391.16,9424.47);
    funcPoint[144]=new FuncPoint(5630.74,true,-3477.89,2748.64,5133.77,404.66,9464.35);
    funcPoint[145]=new FuncPoint(5640.74,true,-3520.59,2670.34,5164.55,418.76,9504.66);
    funcPoint[146]=new FuncPoint(5650.74,true,-3562.63,2590.30,5195.76,433.46,9545.44);
    funcPoint[147]=new FuncPoint(5660.74,true,-3604.58,2510.85,5225.95,448.86,9586.70);
    funcPoint[148]=new FuncPoint(5670.74,true,-3646.51,2430.08,5255.76,464.76,9628.46);
    funcPoint[149]=new FuncPoint(5680.74,true,-3689.25,2348.50,5284.57,481.36,9670.80);
    funcPoint[150]=new FuncPoint(5690.74,true,-3730.43,2266.35,5313.93,498.66,9713.71);
    funcPoint[151]=new FuncPoint(5700.74,true,-3773.17,2183.71,5341.49,516.56,9757.25);
    funcPoint[152]=new FuncPoint(5710.74,true,-3814.35,2100.43,5369.60,535.16,9801.44);
    funcPoint[153]=new FuncPoint(5720.74,true,-3856.34,2016.05,5396.77,554.46,9846.34);
    funcPoint[154]=new FuncPoint(5730.74,true,-3897.66,1931.43,5423.82,574.56,9891.98);
    funcPoint[155]=new FuncPoint(5740.74,true,-3938.93,1845.13,5450.69,595.36,9938.42);
    funcPoint[156]=new FuncPoint(5750.74,true,-3981.21,1759.24,5475.86,616.86,9985.70);
    funcPoint[157]=new FuncPoint(5760.74,true,-4022.29,1671.85,5501.69,639.16,10033.88);
    funcPoint[158]=new FuncPoint(5770.74,true,-4063.63,1584.34,5526.66,662.26,10083.03);
    funcPoint[159]=new FuncPoint(5780.74,true,-4104.64,1495.59,5551.53,686.16,10133.19);
    funcPoint[160]=new FuncPoint(5790.74,true,-4146.02,1406.58,5575.61,710.96,10184.44);
    funcPoint[161]=new FuncPoint(5800.74,true,-4187.76,1317.18,5598.81,736.56,10236.86);
    funcPoint[162]=new FuncPoint(5810.74,true,-4229.17,1226.29,5621.89,762.96,10290.50);
    funcPoint[163]=new FuncPoint(5820.74,true,-4270.07,1134.58,5644.92,790.26,10345.46);
    funcPoint[164]=new FuncPoint(5833.13,true,-4320.97,1020.64,5672.56,825.36,10415.49);
    funcPoint[165]=new FuncPoint(5845.74,true,-4372.43,903.10,5700.22,862.46,10458.69);
    funcPoint[166]=new FuncPoint(5860.13,true,-4431.75,768.68,5730.23,906.56,10509.21);
    funcPoint[167]=new FuncPoint(5862.35,true,-4440.77,747.12,5734.87,913.46,10505.97);
    funcPoint[168]=new FuncPoint(5870.13,true,-4472.95,674.07,5750.35,938.16,10488.49);
    funcPoint[169]=new FuncPoint(5880.13,true,-4513.39,579.78,5770.19,970.46,10465.71);
    funcPoint[170]=new FuncPoint(5880.25,true,-4513.74,579.02,5770.50,970.86,10465.78);

  }

  double getTimeAtIndex(int i) {
    if (i<first||i>last) {
      return -999.999;
    } 
    else {
      return funcPoint[i].ti;
    }
  }

  FuncPoint getForT_is(double t) {
    // check for false t:
    if (t<funcPoint[first].ti||t>funcPoint[last].ti) {
      return new FuncPoint(t,false,0,0,0,0,0);
    } 
    else {
      //--- find point in array with ti<t and (t-ti) smallest value:
      int i=0; 
      boolean found=false;
      do { 
        found=(funcPoint[i+1].ti-t>0); 
        i++; 
      } 
      while (!found&&i<last-1);
      int i1=i-1, i2=i;

      //--- interpolate between this and next position
      double t1=funcPoint[i1].ti, t2=funcPoint[i2].ti, tr=t2-t1;
      double perc=(t-t1)/tr; 

      boolean pf_out=funcPoint[i1].pf;
      double x1=funcPoint[i1].x, x2=funcPoint[i2].x, xr=x2-x1, x_out=x1+perc*xr;
      double y1=funcPoint[i1].y, y2=funcPoint[i2].y, yr=y2-y1, y_out=y1+perc*yr;
      double z1=funcPoint[i1].z, z2=funcPoint[i2].z, zr=z2-z1, z_out=z1+perc*zr;
      double a1=funcPoint[i1].alt, a2=funcPoint[i2].alt, ar=a2-a1, a_out=a1+perc*ar;
      double v1=funcPoint[i1].vrel, v2=funcPoint[i2].vrel, vrelr=v2-v1, vrel_out=v1+perc*vrelr;

      return new FuncPoint(t,pf_out,x_out,y_out,z_out,a_out,vrel_out);
    }
  }
}
