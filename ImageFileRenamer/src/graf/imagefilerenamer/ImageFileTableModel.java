package graf.imagefilerenamer;

import java.io.File;
import java.util.Arrays;

import javax.swing.table.AbstractTableModel;

class ImageFileTableModel extends AbstractTableModel {
    private String[] columnNames = {"Image name"};
    
    private boolean DEBUG = false;
    
    private Object[][] data = { { "EMPTY" } };

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }
    
    private void insertNewData(File f) {
    	if (data.length==1 && data[0][0].equals("EMPTY"))
    		data[0][0] = f;
    	else {
    		Object[][] newData = new Object[data.length+1][1];
    		System.arraycopy(data, 0, newData, 0, data.length);
    		newData[data.length][0] = f;
    		data = newData;
    	}
    }
    
    public void addImageFile(ImageFile f) {
    	insertNewData(f);
    	fireTableRowsInserted(data.length-1,data.length-1);
    }
    
    public File[] getFiles() {
    	File[] out = new File[data.length];
    	for (int i=0;i<out.length;i++)
    		out[i] = (File)data[i][0];
    	return out;
    }

    /*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        //if (col < 2) {
        //   return false;
        //} else {
        //    return true;
        //}
    	return false;
    }

    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    public void setValueAt(Object value, int row, int col) {
        if (DEBUG) {
            System.out.println("Setting value at " + row + "," + col
                               + " to " + value
                               + " (an instance of "
                               + value.getClass() + ")");
        }

        data[row][col] = value;
        fireTableCellUpdated(row, col);

        if (DEBUG) {
            System.out.println("New value of data:");
            printDebugData();
        }
    }

    private void printDebugData() {
        int numRows = getRowCount();
        int numCols = getColumnCount();

        for (int i=0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j=0; j < numCols; j++) {
                System.out.print("  " + data[i][j]);
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }
    
    
}

