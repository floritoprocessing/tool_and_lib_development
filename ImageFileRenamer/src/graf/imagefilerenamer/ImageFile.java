package graf.imagefilerenamer;

import java.io.File;
import java.net.URI;

public class ImageFile extends File {

	public ImageFile(File arg0, String arg1) {
		super(arg0, arg1);
	}

	public ImageFile(String arg0, String arg1) {
		super(arg0, arg1);
	}

	public ImageFile(String arg0) {
		super(arg0);
	}

	public ImageFile(URI arg0) {
		super(arg0);
	}
	
	public ImageFile(File file) {
		super(file.toURI());
	}

	public String toString() {
		return getName();
	}

}
