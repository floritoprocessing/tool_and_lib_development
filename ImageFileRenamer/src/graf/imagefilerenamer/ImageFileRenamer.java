/*
 * Copyright (c) 1995 - 2008 Sun Microsystems, Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Sun Microsystems nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package graf.imagefilerenamer;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.*;
import javax.swing.filechooser.*;


/*
 * FileChooserDemo2.java requires these files: ImageFileView.java
 * ImageFilter.java ImagePreview.java Utils.java images/jpgIcon.gif (required by
 * ImageFileView.java) images/gifIcon.gif (required by ImageFileView.java)
 * images/tiffIcon.gif (required by ImageFileView.java) images/pngIcon.png
 * (required by ImageFileView.java)
 */
public class ImageFileRenamer extends JPanel implements ActionListener, PropertyChangeListener {

	static private String newline = "\n";
	private JTextArea log;
	private JFileChooser fc;
	private JFileChooser targetDirectoryChooser;
	private ImageFileTable table;
	private ImageFileTableModel tableModel;
	private JButton sendButton, copyButton;
	private ImagePreviewForTable preview;
	
	private ImageFileCopyTask task;
	private JProgressBar progressBar;
	
	private File outputPath ;//= new File("C:\\Documents and Settings\\mgraf\\My Documents\\test\\");
	private String fileBase = "";

	public ImageFileRenamer() {
		super(new GridBagLayout());

		// Create the log first, because the action listener
		// needs to refer to it.
		log = new JTextArea(5, 20);
		log.setMargin(new Insets(5, 5, 5, 5));
		log.setEditable(false);
		JScrollPane logScrollPane = new JScrollPane(log);

		sendButton = new JButton("Add...");
		sendButton.addActionListener(this);

		copyButton = new JButton("Copy");
		copyButton.addActionListener(this);
		copyButton.setEnabled(false);

		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0, 0,
				GridBagConstraints.LINE_START, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0);
		add(sendButton, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0, 0,
				GridBagConstraints.LINE_START, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0);
		add(copyButton, c);
		c = new GridBagConstraints(0, 2, 2, 1, 0, 0,
				GridBagConstraints.LINE_START, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0);
		add(logScrollPane, c);

		tableModel = new ImageFileTableModel();
		table = new ImageFileTable(tableModel);
		table.setPreferredScrollableViewportSize(new Dimension(400, 400));
		table.setFillsViewportHeight(true);
		table.setUI(new DragDropRowTableUI());
		table.setEnabled(false);

		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel.
		c = new GridBagConstraints(0, 1, 2, 1, 0, 0,
				GridBagConstraints.LINE_START, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0);
		add(scrollPane, c);
		
		preview = new ImagePreviewForTable(table.getSelectionModel(), table, tableModel);
		c = new GridBagConstraints(2, 1, 1, 1, 0, 0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0);
		add(preview,c);
		
		c = new GridBagConstraints(0, 3, 1, 1, 0, 0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0);
		progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        progressBar.setVisible(true);
        add(progressBar,c);

	}
	
	public void enableGUI() {
		sendButton.setEnabled(true);
        copyButton.setEnabled(true);
        table.setEnabled(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(sendButton)) {

			// Set up the file chooser.
			if (fc == null) {
				fc = new JFileChooser();

				fc.setMultiSelectionEnabled(true);

				// Add a custom file filter and disable the default
				// (Accept All) file filter.
				fc.addChoosableFileFilter(new ImageFilter());
				fc.setAcceptAllFileFilterUsed(false);

				// Add custom icons for file types.
				fc.setFileView(new ImageFileView());

				// Add the preview pane.
				fc.setAccessory(new ImagePreview(fc));
			}

			// Show it.
			int returnVal = fc.showDialog(ImageFileRenamer.this, "Add file(s)");

			// Process the results.
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File[] file = fc.getSelectedFiles();
				for (int i = 0; i < file.length; i++) {
					log.append("Adding file: " + file[i].getName() + "." + newline);
					table.setEnabled(true);
					copyButton.setEnabled(true);
					tableModel.addImageFile(new ImageFile(file[i]));
				}
			} else {
				log.append("Attachment cancelled by user." + newline);
			}
			log.setCaretPosition(log.getDocument().getLength());

			// Reset the file chooser for the next time it's shown.
			fc.setSelectedFile(null);

			
			
			
			
		} else if (e.getSource().equals(copyButton)) {
			sendButton.setEnabled(false);
	        copyButton.setEnabled(false);
	        table.setEnabled(false);
	        if (targetDirectoryChooser==null) {
	        	targetDirectoryChooser = new JFileChooser();
	        	targetDirectoryChooser.setMultiSelectionEnabled(false);
	        	targetDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        }
	        int returnVal = targetDirectoryChooser.showDialog(ImageFileRenamer.this, "Choose target directory");
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
				outputPath = targetDirectoryChooser.getSelectedFile();
				progressBar.setVisible(true);
				task = new ImageFileCopyTask(this,tableModel.getFiles(),outputPath,fileBase);
		        task.addPropertyChangeListener(this);
		        task.execute();
			} else {
				log.append("Cancelled copy process" + newline);
				enableGUI();
			}
	        
		}
	}
	
	public void addToLog(String arg0) {
		log.append(arg0 + newline);
	}
	
	/**
	 * Invoked from the ImageFileCopyTask
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress" == evt.getPropertyName()) {
            int progress = (Integer) evt.getNewValue();
            progressBar.setValue(progress);
            System.out.println("Progress: "+task.getProgress());
            //taskOutput.append(String.format("Completed %d%% of task.\n", task.getProgress()));
        } 
	}
	

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Image File Renamer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add content to the window.
		frame.add(new ImageFileRenamer());

		// Display the window.
		frame.setLocation(new Point(50,50));
		frame.pack();
		frame.setVisible(true);
		
	}

	public static void main(String[] args) {
		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}
		});
	}

	
}
