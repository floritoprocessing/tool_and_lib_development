package graf.imagefilerenamer;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Random;

import javax.swing.SwingWorker;

class ImageFileCopyTask extends SwingWorker<Void, Void> {
	
	private ImageFileRenamer main;
	private File[] file;
	private File outputPath;
	private String filenameBase;
	private int fileIndex = 0;
	
	private NumberFormat nf;
	
	public ImageFileCopyTask(ImageFileRenamer main, File[] file, File outputPath, String filenameBase) {
		super();
		this.main = main;
		this.file = file;
		this.outputPath = outputPath;
		this.filenameBase = filenameBase;
		nf = NumberFormat.getInstance();
		nf.setParseIntegerOnly(true);
		nf.setMinimumIntegerDigits(5);
		nf.setGroupingUsed(false);
	}
	
    /*
     * Main task. Executed in background thread.
     */
    @Override
    public Void doInBackground() {
        Random random = new Random();
        int progress = 0;
        //Initialize progress property.
        setProgress(0);
        while (progress < 100) {
            //Sleep for up to one second.
            //try {
            //   Thread.sleep(random.nextInt(1000));
            //} catch (InterruptedException ignore) {}
            //Make random progress.
            //progress += random.nextInt(10);
        	
        	File sourceFile = file[fileIndex++];
        	File targetFile = new File(outputPath,filenameBase+nf.format(fileIndex)+"_"+sourceFile.getName());
        	main.addToLog(sourceFile+" -> "+targetFile);
        	try {
				FileUtils.copyFile(sourceFile, targetFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        	progress = (int)(100*fileIndex/file.length);
        	
        	
        	
            setProgress(Math.min(progress, 100));
        }
        return null;
    }

    /*
     * Executed in event dispatching thread
     */
    @Override
    public void done() {
        Toolkit.getDefaultToolkit().beep();
        main.enableGUI();
        //startButton.setEnabled(true);
        //setCursor(null); //turn off the wait cursor
        //taskOutput.append("Done!\n");
    }
}