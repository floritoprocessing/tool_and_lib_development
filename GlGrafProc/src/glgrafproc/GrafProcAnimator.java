package glgrafproc;

import java.util.Timer;
import java.util.TimerTask;

import net.java.games.jogl.Animator;
import net.java.games.jogl.GLDrawable;
import net.java.games.jogl.GLException;

public class GrafProcAnimator extends Animator {
	private GLDrawable drawable;
	private RenderRunnable runnable = new RenderRunnable();
	private Thread thread;
	private boolean shouldStop;
	private long delay;
	private Timer renderTimer;
	//private ExceptionHandler exceptionHandler;



	/** Creates a new Animator for a particular drawable. */
	public GrafProcAnimator(GLDrawable drawable, int fps) {
		super(drawable);
		//this.exceptionHandler = exceptionHandler;
		this.drawable = drawable;
		this.delay = 1000 / fps;
	}

	/** Starts this animator. */
	public synchronized void start() {
		if (thread != null) {
			throw new GLException("Already started");
		}
		thread = new Thread(runnable);
		thread.start();

		renderTimer = new Timer();
		renderTimer.schedule(new TimerTask() {
			public void run() {
				runnable.nextFrame();
			}
		}, 0, delay);
	}

	/** Stops this animator, blocking until the animation thread has
     finished. */
	public synchronized void stop() {
		shouldStop = true;
		while (shouldStop && thread != null) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
			renderTimer.cancel();
		}
		shouldStop = false;
	}

	public boolean isFrameRateLimitEnabled() {
		return runnable.isFrameRateLimitEnabled();
	}

	public void setFrameRateLimitEnabled(boolean frameRateLimit) {
		runnable.setFrameRateLimitEnabled(frameRateLimit);
	}

	private class RenderRunnable implements Runnable {
		private boolean frameRateLimitEnabled = true;

		public boolean isFrameRateLimitEnabled() {
			return frameRateLimitEnabled;
		}

		public void setFrameRateLimitEnabled(boolean frameRateLimit) {
			frameRateLimitEnabled = frameRateLimit;
			nextFrameSync();
		}

		public void nextFrame() {
			if (frameRateLimitEnabled)
				nextFrameSync();
		}

		private synchronized void nextFrameSync() {
			notify();
		}

		public void run() {
			boolean noException = false;
			try {
				// Try to get OpenGL context optimization since we know we
				// will be rendering this one drawable continually from
				// this thread; make the context current once instead of
				// making it current and freeing it each frame.
				drawable.setRenderingThread(Thread.currentThread());

				// Since setRenderingThread is currently advisory (because
				// of the poor JAWT implementation in the Motif AWT, which
				// performs excessive locking) we also prevent repaint(),
				// which is called from the AWT thread, from having an
				// effect for better multithreading behavior. This call is
				// not strictly necessary, but if end users write their
				// own animation loops which update multiple drawables per
				// tick then it may be necessary to enforce the order of
				// updates.
				drawable.setNoAutoRedrawMode(true);

				while (!shouldStop) {
					noException = false;
					drawable.display();
					if (frameRateLimitEnabled) {
						synchronized (this) {
							if (frameRateLimitEnabled) {
								try {
									wait();
								} catch (InterruptedException e) {
								}
							}
						}
					}
					noException = true;
				}
			} catch (Exception e) {
				//if (exceptionHandler != null)
				//exceptionHandler.handleException(e);
				System.out.println("oops");
				System.out.println(e.toString());
			} finally {
				shouldStop = false;
				drawable.setNoAutoRedrawMode(false);
				try {
					// The surface is already unlocked and rendering
					// thread is already null if an exception occurred
					// during display(), so don't disable the rendering
					// thread again.
					if (noException) {
						drawable.setRenderingThread(null);
					}
				} finally {
					synchronized (GrafProcAnimator.this) {
						thread = null;
						GrafProcAnimator.this.notify();
					}
				}
			}
		}
	}
}