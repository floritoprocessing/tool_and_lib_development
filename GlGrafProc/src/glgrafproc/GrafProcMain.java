package glgrafproc;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import net.java.games.jogl.GL;
import net.java.games.jogl.GLCanvas;
import net.java.games.jogl.GLCapabilities;
import net.java.games.jogl.GLDrawable;
import net.java.games.jogl.GLDrawableFactory;
import net.java.games.jogl.GLU;

public class GrafProcMain extends JFrame implements GrafProcConstants {

	private static final long serialVersionUID = -2659536763032826273L;
	private GrafProcEventListener glEventListener = null;
	private GrafProcAnimator animator = null;
	protected long frameCount = 0;
	
	//private GLDrawable drawable;
	public GL gl;
	public GLU glu;
	
	public int width, height;
	
	/**
	 * Starts the main GrafProc application. If not supplied, uses size 640x480 at framerate 25<br>
	 * Argument to supply (as an array of Strings):<br>
	 * <li> Your class name (mandatory!)
	 * <li> "WIDTH = <i>application width</i>"
	 * <li> "HEIGHT = <i>application height</i>"
	 * <li> "FPS = <i>framerate of the application</i>"
	 * @param args (String[]) the arguments to supply
	 */
	public static void main(String[] args) {
		int width=640, height=480;
		int fps=25;
		if (args.length<1) {
			throw new RuntimeException("Please supply the class name as first argument in main");
		} else {
			for (int i=1;i<args.length;i++) {
				args[i] = args[i].replaceAll(" ","");
				int equals = args[i].indexOf("=");
				String var = args[i].substring(0,equals);
				String val = args[i].substring(equals+1,args[i].length());
				
				if (var.equals(APPLICATION_WIDTH)) {
					width = (int)Double.parseDouble(val);
				} else if (var.equals(APPLICATION_HEIGHT)) {
					height = (int)Double.parseDouble(val);
				} else if (var.equals(APPLICATION_FPS)) {
					fps = (int)Double.parseDouble(val);
				}
				//System.out.println(var+"\t"+val);
			}
		}
		
		Class c;
		try {
			c = Class.forName(args[0]);
			try {
				GrafProcMain app = (GrafProcMain) c.newInstance();
				app.init(args[0],width,height,fps);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			System.err.println("You need to supply the class name as first argument in main");
			//e.printStackTrace();
		}
		
	}
	
	//public GrafProcMain() {}
	
	private void init(String title, int width, int height, int fps) {
		setTitle(title);
		setResizable(false);
		
		this.width = width;
		this.height = height;
		
		System.out.println("Starting application:");
		System.out.println("Dimensions: "+width+"/"+height);
		System.out.println("Framerate: "+fps);
		//super("JOGL_MyFirst");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		GLCapabilities glcaps = new GLCapabilities();
		//System.out.println((glcaps.toString()).replace(',','\r'));
		GLCanvas glcanvas = GLDrawableFactory.getFactory().createGLCanvas(glcaps);
		
		glEventListener = new GrafProcEventListener(this);
		glcanvas.addGLEventListener(glEventListener);
		
		//	  add the GLCanvas just like we would any Component
		getContentPane().add(glcanvas, BorderLayout.CENTER);
		setSize(width,height);

		centerWindow(this);
		
		animator = new GrafProcAnimator(glcanvas,fps);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e)
		      {
		        animator.stop();
		        System.out.println("end");
		        System.exit(0);
		      }
		});
		animator.start();
		
		setVisible(true);
	}
	
	public void setupGL(GLDrawable drawable) {
		//this.drawable = drawable;
		gl = drawable.getGL();
		glu = drawable.getGLU();
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(0.0, width, 0.0, height);
	}
	
	public void setup() {}
	
	public void draw() {}
	
	void incFrame() {
		frameCount++;
	}
	
	private void centerWindow(Component frame) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//System.out.println("Screen size: "+screenSize.width+"/"+screenSize.height);
		Dimension frameSize = frame.getSize();
		//System.out.println("Frame size: "+frameSize.width+"/"+frameSize.height);
		frame.setLocation((screenSize.width-frameSize.width)>>1,(screenSize.height-frameSize.height)>>1);
	}
}
