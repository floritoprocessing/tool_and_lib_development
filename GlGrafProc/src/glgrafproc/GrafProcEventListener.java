package glgrafproc;

import net.java.games.jogl.*;

public class GrafProcEventListener implements GLEventListener {
	
	private GrafProcMain main;
	
	public GrafProcEventListener(GrafProcMain main) {
		this.main = main;
	}
	
	public void init(GLDrawable drawable) {
		// TODO Auto-generated method stub
		//System.out.println("init");
		main.setupGL(drawable);
		main.setup();
		
		/*GL gl = drawable.getGL();
		GLU glu = drawable.getGLU();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		gl.glViewport(0, 0, main.width, main.height);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(0.0, main.width, 0.0, main.height);*/
	}

	public void display(GLDrawable drawable) {
		// TODO Auto-generated method stub
		main.draw();
		main.incFrame();
		/*GL gl = drawable.getGL();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT);
		gl.glPointSize(5.0f);
		for (int i=0;i<1000;i++) {
			gl.glColor3d(Math.random(), Math.random(), Math.random());
			gl.glBegin(GL.GL_POINTS);
			gl.glVertex2d(800*Math.random(), 600*Math.random());
			gl.glEnd();
		}*/
	}

	public void displayChanged(GLDrawable drawable, boolean modeChanged, boolean devideChanged) {
		// TODO Auto-generated method stub

	}



	public void reshape(GLDrawable drawable, int x, int y, int width, int height) {
		// TODO Auto-generated method stub

	}

}
