String[] iconDescriptions = loadStrings("Icons.xml");
String iconName = "Icon.png";

String outputXml = "icons/IconsFilled.xml";
String outputImageBase = "icons/icon_";

PImage img = loadImage(iconName);
float aspRat = (float)img.width/(float)img.height;

for (int i=0;i<iconDescriptions.length;i++) {
  String iconDescription = iconDescriptions[i];
//for (String iconDescription : iconDescriptions) {

  if (iconDescription.length()>1) {
    
    //TODO split xml
    String pre = iconDescription.substring(0,iconDescription.indexOf(">")+1);
    String post = iconDescription.substring(2);
    post = post.substring(post.indexOf("<"));
    println(pre+" "+post);
  
    String size = pre.replace("<","").replace(">","").replace("image","");
    int w = Integer.parseInt(size.split("x")[0]);
    int h = Integer.parseInt(size.split("x")[1]);
    float iconAspRat = (float)w/(float)h;
    println("Creating image "+w+" by "+h);
    
    PImage icon = new PImage(w,h);
    icon.copy(img, 0, 0, img.width, img.height, 0, 0, w, h);
    
    String outName = outputImageBase+w+"x"+h+".png";
    icon.save(savePath(outName));
    
    iconDescriptions[i] = iconDescription.replace("><", ">"+outName+"<");
    
  }
  
  saveStrings(outputXml, iconDescriptions);
  
}

exit();
