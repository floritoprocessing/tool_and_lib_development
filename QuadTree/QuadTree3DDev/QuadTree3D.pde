class QuadTree3D {

  final int CAPACITY;
  final float MIN_SIDE_LENGTH;
  final Box box;
  final ArrayList<Point3D> points;

  boolean flag = false;
  boolean divided = false;

  QuadTree3D northWest;
  QuadTree3D northEast;
  QuadTree3D southWest;
  QuadTree3D southEast;
  QuadTree3D farnorthWest;
  QuadTree3D farnorthEast;
  QuadTree3D farsouthWest;
  QuadTree3D farsouthEast;

  QuadTree3D(int capacity, float minSideLength, Box box) {
    this.MIN_SIDE_LENGTH = minSideLength;
    this.CAPACITY = capacity;
    this.box = box;
    points = new ArrayList<Point3D>();
  }

  boolean inRange(float x, float y, float z) {
    return box.contains(x, y, z);
  }
  boolean inRange(Point3D point) {
    return box.contains(point.x, point.y, point.z);
  }

  void insertPoint(float x, float y, float z) {
    insertPoint( new Point3D(x, y, z) );
  }

  void insertPoint(Point3D point) {
    // insert only if within range
    if (inRange(point)) {

      if (points.size()<CAPACITY) {

        points.add( point );
      } else {

        subdivide();
        northWest.insertPoint( point );
        northEast.insertPoint( point );
        southWest.insertPoint( point );
        southEast.insertPoint( point );
        farnorthWest.insertPoint( point );
        farnorthEast.insertPoint( point );
        farsouthWest.insertPoint( point );
        farsouthEast.insertPoint( point );
      }
    }
  }

  void subdivide() {
    if (!divided) {
      int capacity = CAPACITY;
      float newWidth = box.WIDTH/2;
      float newHeight = box.HEIGHT/2;
      float newDepth = box.DEPTH/2;
      float maxSide = max(newWidth, newHeight, newDepth);
      if (maxSide<MIN_SIDE_LENGTH) {
        capacity = Integer.MAX_VALUE;
      }
      northWest = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.X0, box.Y0, box.Z0, newWidth, newHeight, newDepth));
      northEast = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.AVERAGE_X, box.Y0, box.Z0, newWidth, newHeight, newDepth));
      southWest = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.X0, box.AVERAGE_Y, box.Z0, newWidth, newHeight, newDepth));
      southEast = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.AVERAGE_X, box.AVERAGE_Y, box.Z0, newWidth, newHeight, newDepth));
      farnorthWest = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.X0, box.Y0, box.AVERAGE_Z, newWidth, newHeight, newDepth));
      farnorthEast = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.AVERAGE_X, box.Y0, box.AVERAGE_Z, newWidth, newHeight, newDepth));
      farsouthWest = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.X0, box.AVERAGE_Y, box.AVERAGE_Z, newWidth, newHeight, newDepth));
      farsouthEast = new QuadTree3D(capacity, MIN_SIDE_LENGTH, new Box(box.AVERAGE_X, box.AVERAGE_Y, box.AVERAGE_Z, newWidth, newHeight, newDepth));
      divided=true;
    }
  }



  //@see https://github.com/CodingTrain/QuadTree/blob/master/quadtree.js
  void getPointsWithinBounds(Box queryBox, ArrayList<Point3D> foundPoints) {
    //intersects

    if (!queryBox.intersects(box)) {
      return;// found;
    }


    for (Point3D p : points) {
      if (box.contains(p.x, p.y, p.z)) {
        foundPoints.add(p);
      }
    }

    if (divided) {
      northWest.getPointsWithinBounds(queryBox, foundPoints);
      northEast.getPointsWithinBounds(queryBox, foundPoints);
      southWest.getPointsWithinBounds(queryBox, foundPoints);
      southEast.getPointsWithinBounds(queryBox, foundPoints);
      farnorthWest.getPointsWithinBounds(queryBox, foundPoints);
      farnorthEast.getPointsWithinBounds(queryBox, foundPoints);
      farsouthWest.getPointsWithinBounds(queryBox, foundPoints);
      farsouthEast.getPointsWithinBounds(queryBox, foundPoints);
    }
  }



  void show() {
    if (divided) {
      if (flag) {
        stroke(192, 0, 0);
        pushMatrix();
        translate(box.AVERAGE_X, box.AVERAGE_Y, box.AVERAGE_Z);
        box(box.WIDTH,box.HEIGHT,box.DEPTH);
        popMatrix();
      }
      stroke(128);
      
      line(box.X0, box.AVERAGE_Y, box.AVERAGE_Z, box.X1, box.AVERAGE_Y, box.AVERAGE_Z); // horizontal line
      line(box.AVERAGE_X, box.Y0, box.AVERAGE_Z, box.AVERAGE_X, box.Y1, box.AVERAGE_Z); // vertical line
      line(box.AVERAGE_X, box.AVERAGE_Y, box.Z0, box.AVERAGE_X, box.AVERAGE_Y, box.Z1); // depth line
      
      northWest.show();
      northEast.show();
      southWest.show();
      southEast.show();
      farnorthWest.show();
      farnorthEast.show();
      farsouthWest.show();
      farsouthEast.show();
    }
  }
}





class Box {

  final float X0, X1, Y0, Y1, Z0, Z1, WIDTH, HEIGHT, DEPTH;
  final float AVERAGE_X, AVERAGE_Y, AVERAGE_Z;


  Box(float x, float y, float z, float w, float h, float d) {
    if (w<=0||h<=0||d<=0) throw new RuntimeException("Illegal Rect with w="+w+" and h="+h+" and d="+d);
    this.X0=x;
    this.Y0=y;
    this.Z0=z;
    this.X1=x+w;
    this.Y1=y+h;
    this.Z1=z+d;
    this.WIDTH=w;
    this.HEIGHT=h;
    this.DEPTH=d;
    this.AVERAGE_X = x+w/2;
    this.AVERAGE_Y = y+h/2;
    this.AVERAGE_Z = z+d/2;
  }

  boolean contains(float x, float y, float z) {
    return x>=X0&&x<X1&&y>=Y0&&y<Y1 && z>=Z0&&z<Z1;
  }

  boolean contains(Box other) {
    return other.X0>=X0 && other.X1<=X1 && other.Y0>=Y0 && other.Y1<=Y1&& other.Z0>=Z0 && other.Z1<=Z1;
  }

  boolean intersects(Box other) {
    return !(
      other.X0 > X1 ||
      other.X1 < X0 ||
      other.Y0 > Y1 ||
      other.Y1 < Y0 ||
      other.Z0 > Z1 ||
      other.Z1 < Z0);
  }
}



class Point3D {
  float x, y, z;
  Point3D(float x, float y, float z) {
    this.x=x;
    this.y=y;
    this.z=z;
  }
}
