class SpaceMap3D {

  final float UNIT_SIZE;

  final float WIDTH;
  final float HEIGHT;
  final float DEPTH;

  final int unitAmountX;
  final int unitAmountY;
  final int unitAmountZ;

  final float xToUnitIndexFac;
  final float yToUnitIndexFac;
  final float zToUnitIndexFac;

  final SpaceMap3DUnit[][][] units;

  SpaceMap3D(float w, float h, float d, float unitSize) {
    this.UNIT_SIZE = unitSize;
    this.WIDTH = w;
    this.HEIGHT = h;
    this.DEPTH = d;
    this.unitAmountX = (int)(w/unitSize);// i.e. w=100,s=5 -> 20 or w=101,s=5->21
    this.unitAmountY = (int)(h/unitSize);
    this.unitAmountZ = (int)(d/unitSize);
    units = new SpaceMap3DUnit[unitAmountX][unitAmountY][unitAmountZ];
    for (int x=0; x<unitAmountX; x++) {
      for (int y=0; y<unitAmountY; y++) {
        for (int z=0; z<unitAmountZ; z++) {
          units[x][y][z] = new SpaceMap3DUnit();
        }
      }
    }
    xToUnitIndexFac = unitAmountX/w;
    yToUnitIndexFac = unitAmountY/h;
    zToUnitIndexFac = unitAmountZ/d;
  }


  void insertPoint(float x, float y, float z) {
    if (inRange(x, y, z)) {
      int indexX = (int)(x*xToUnitIndexFac); // i.e. w=101,s=5,amount=21: x=100.5 -> 100.5*21/101
      int indexY = (int)(y*yToUnitIndexFac);
      int indexZ = (int)(z*zToUnitIndexFac);
      Point3D point = new Point3D(x, y, z);
      units[indexX][indexY][indexZ].points.add( point );
    }
  }
  

  ArrayList<Point3D> getPointsAround(float x, float y, float z) {
    ArrayList<Point3D> points = new ArrayList<Point3D>();

    int indexX = (int)(x*xToUnitIndexFac); // i.e. w=101,s=5,amount=21: x=100.5 -> 100.5*21/101
    int indexY = (int)(y*yToUnitIndexFac);
    int indexZ = (int)(z*zToUnitIndexFac);
    
    int x0 = max(0, indexX-1);
    int x1 = min(unitAmountX-1, indexX+1);
    int y0 = max(0, indexY-1);
    int y1 = min(unitAmountY-1, indexY+1);
    int z0 = max(0, indexZ-1);
    int z1 = min(unitAmountZ-1, indexZ+1);
    
    for (int xi=x0; xi<=x1; xi++) {
      for (int yi=y0; yi<=y1; yi++) {
        for (int zi=z0; zi<=z1; zi++) {
          points.addAll( units[xi][yi][zi].points );
        }
      }
    }

    return points;
  }
  

  boolean inRange(float x, float y, float z) {
    return x>=0&&x<WIDTH&&y>=0&&y<HEIGHT && z>=0&&z<DEPTH;
  }
}


class SpaceMap3DUnit {

  final ArrayList<Point3D> points = new ArrayList<Point3D>();

  SpaceMap3DUnit() {
  }
}
