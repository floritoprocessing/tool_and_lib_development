class QuadTree {

  final int CAPACITY;
  final Rect bounds;
  final ArrayList<Point2D> points;

  boolean divided = false;
  QuadTree northWest;
  QuadTree northEast;
  QuadTree southWest;
  QuadTree southEast;

  QuadTree(int capacity, Rect bounds) {
    this.bounds = bounds;
    this.CAPACITY = capacity;
    points = new ArrayList<Point2D>();
  }

  boolean inRange(float x, float y) {
    return bounds.contains(x,y);
  }

  void insertPoint(float x, float y) {
    // insert only if within range
    if (inRange(x, y)) {
      if (points.size()<CAPACITY) {
        points.add( new Point2D(x,y) );
      } else {

        subdivide();
        northWest.insertPoint(x, y);
        northEast.insertPoint(x, y);
        southWest.insertPoint(x, y);
        southEast.insertPoint(x, y);
      }
    }
  }

  void subdivide() {
    if (!divided) {
      northWest = new QuadTree(CAPACITY, new Rect(bounds.X0, bounds.Y0, bounds.WIDTH/2, bounds.HEIGHT/2));
      northEast = new QuadTree(CAPACITY, new Rect(bounds.AVERAGE_X, bounds.Y0, bounds.WIDTH/2, bounds.HEIGHT/2));
      southWest = new QuadTree(CAPACITY, new Rect(bounds.X0, bounds.AVERAGE_Y, bounds.WIDTH/2, bounds.HEIGHT/2));
      southEast = new QuadTree(CAPACITY, new Rect(bounds.AVERAGE_X, bounds.AVERAGE_Y, bounds.WIDTH/2, bounds.HEIGHT/2));
      divided=true;
    }
  }


  ArrayList<Point2D> getPointsWithinBounds(Rect queryBounds) {
    ArrayList<Point2D> foundPoints = new ArrayList<Point2D>();
    
    // if the QuadTree areas are inside the bounds
    // or if the QuadTree areas are overlapping with the bounds
    // add the points of this area
    
    // 1. add ALL points of a quadtree of which the bounds are inside the query bounds
    if (queryBounds.contains(bounds)) {
      
      getAllPointsAndSubtreePoints( foundPoints );
      
    }
    
    
    
    return foundPoints;
  }
  
  void getAllPointsAndSubtreePoints( ArrayList<Point2D> output ) {
    output.addAll(points);
    if (divided) {
      northWest.getAllPointsAndSubtreePoints(output);
      northEast.getAllPointsAndSubtreePoints(output);
      southWest.getAllPointsAndSubtreePoints(output);
      southEast.getAllPointsAndSubtreePoints(output);
    }
  }

  /**
   returns a sub group of points of which all are within the supplied distance
   */
  //ArrayList<float[]> getPointsInRange(float x, float y, float distance) {
  //  return getPointsInRangeSquared(x, y, distance*distance);
  //}

  //ArrayList<float[]> getPointsInRangeSquared(float x, float y, float distanceSquared) {

  //  ArrayList<float[]> xyPairs = new ArrayList<float[]>();

  //  for (int i=0; i<size; i++) {
  //    float px = xArr[i];
  //    float py = yArr[i];
  //    float dx = px-x;
  //    float dy = py-y;
  //    float dSq = dx*dx+dy*dy;
  //    if (dSq<distanceSquared) {
  //      xyPairs.add( new float[] { px, py } );
  //    }
  //  }
  //  if (northWest!=null) {
  //    if (inRange(x, y)) {
  //      xyPairs.addAll( northWest.getPointsInRangeSquared(x, y, distanceSquared) );
  //      xyPairs.addAll( northEast.getPointsInRangeSquared(x, y, distanceSquared) );
  //      xyPairs.addAll( southWest.getPointsInRangeSquared(x, y, distanceSquared) );
  //      xyPairs.addAll( southEast.getPointsInRangeSquared(x, y, distanceSquared) );
  //    }
  //  }

  //  return xyPairs;
  //}


  void show() {
    if (divided) {
      line(bounds.AVERAGE_X, bounds.Y0, bounds.AVERAGE_X, bounds.Y1);
      line(bounds.X0, bounds.AVERAGE_Y, bounds.X1, bounds.AVERAGE_Y);
      northWest.show();
      northEast.show();
      southWest.show();
      southEast.show();
    }
  }
}





class Rect {
  final float X0, X1, Y0, Y1, WIDTH, HEIGHT;
  final float AVERAGE_X, AVERAGE_Y;
  Rect(float x, float y, float w, float h) {
    this.X0=x;
    this.Y0=y;
    this.X1=x+w;
    this.Y1=y+w;
    this.WIDTH=w;
    this.HEIGHT=h;
    this.AVERAGE_X = x+w/2;
    this.AVERAGE_Y = y+h/2;
  }
  boolean contains(float x, float y) {
    return x>=X0&&x<X1&&y>=Y0&&y<Y1;
  }
  
  boolean contains(Rect other) {
    return other.X0>=X0 && other.X1<=X1 && other.Y0>=Y0 && other.Y1<=Y1;
  }
  
}


class Point2D {
  float x, y;
  Point2D(float x, float y) {
    this.x=x;
    this.y=y;
  }
}
