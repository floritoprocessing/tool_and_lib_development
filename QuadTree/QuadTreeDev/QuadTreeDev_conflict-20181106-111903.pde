
ArrayList<PVector> points;
ArrayList<PVector> movements;
boolean showTree = true;

void setup() {

  size(800, 400, P3D);
  points = new ArrayList<PVector>();
  movements = new ArrayList<PVector>();

  for (int i=0; i<1; i++) {
    addPoint(random(width), random(height));
  }
}
void addPoint(float x, float y) {
  PVector point = new PVector(x, y);
  points.add( point );
  PVector mov = PVector.random2D();
  movements.add( mov );
}

void mouseDragged() {
  float d = dist(mouseX, mouseY, pmouseX, pmouseY);
  int steps = ceil(d);
  if (steps<2) {
    addPoint(mouseX, mouseY);
  } else {
    for (int i=0; i<steps; i++) {
      float p = map(i, 0, steps-1, 0, 1);
      float x = lerp(pmouseX, mouseX, p);
      float y = lerp(pmouseY, mouseY, p);
      addPoint(x, y);
    }
  }
}
void mousePressed() {
  addPoint(mouseX, mouseY);
}
void keyPressed() {
  if (key=='s') 
    showTree = !showTree;
}


void iterate() {
  // iterate movement
  for (int i=0; i<points.size(); i++) {
    PVector p = points.get(i);
    PVector m = movements.get(i);

    float n = noise(p.x/width*10, p.y/height*10, frameCount*0.01);
    float rot = map(n, 0, 1, -PI, PI)*0.02;
    m.rotate(rot);
    //m.mult(0.999);
    p.add(m);
    if (p.x<0) p.x+=width; 
    else if (p.x>=width) p.x-=width;
    if (p.y<0) p.y+=height; 
    else if (p.y>=height) p.y-=height;
  }
}


QuadTree buildTree() {

  // build tree
  QuadTree tree = new QuadTree(4, 0, width, 0, height);
  for (PVector p : points) {
    tree.insertPoint(p.x, p.y);
  }

  return tree;
}


//ArrayList<PVector> makeLines(float maxLength, QuadTree tree) {
//  ArrayList<PVector> lines = new ArrayList<PVector>();
  
//  // will create duplicate lines, but that' ok for now
//  for (PVector p : points) {
    
//    ArrayList<float[]> xyPairs = tree.getPointsInRange(p.x, p.y, maxLength);
//    for (float[] xyPair : xyPairs) {
//      lines.add( p);
//      lines.add( new PVector(xyPair[0], xyPair[1]) );
//    }
    
//  }
  
//  return lines;
//}


/*
  *  DRAW!
 */


void draw() {

  iterate();
  QuadTree tree = buildTree();
  
  
  float x0 = 55, x1 = 355, y0 = 111, y1=208;
  
  //ArrayList<PVector> lines = makeLines(30, tree);

  background(255);

  

  // show points
  stroke(0);
  strokeWeight(2);
  beginShape(POINTS);
  for (PVector p : points) {
    vertex(p.x, p.y);
  }
  endShape();
  
  // show area
  rectMode(CORNERS);
  noFill();
  stroke(0,255,0);
  rect(x0,y0,x1,y1);
  
  // show points in area
  strokeWeight(4);
  stroke(0,190,0);
  ArrayList<float[]> areaPoints = tree.getPointsWithinBounds(x0,x1,y0,y1);
  beginShape(POINTS);
  for (float[] xyPair : areaPoints) {
    vertex(xyPair[0], xyPair[1]);
  }
  endShape();
  // show lines
  //stroke(0);
  //strokeWeight(1);
  //noFill();
  //beginShape(LINES);
  //for (PVector l : lines)
  //  vertex(l.x,l.y);
  //endShape();

  // show tree
  if (showTree) {
    stroke(192);
    strokeWeight(1);
    tree.show();
  }
  
  translate(0,0,1);
  rectMode(CORNER);
  stroke(0);
  fill(255,64);
  rect(5,5,200,20);
  fill(0);
  text("points: "+points.size(), 10, 20);
}
