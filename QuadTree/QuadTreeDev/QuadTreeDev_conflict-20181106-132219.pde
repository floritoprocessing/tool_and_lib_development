
ArrayList<PVector> points;
ArrayList<PVector> movements;
boolean showTree = true;
boolean iterate = true;
Rect testBounds;
Rect testBounds1;

void setup() {

  size(800, 400, P3D);
  points = new ArrayList<PVector>();
  movements = new ArrayList<PVector>();

  for (int i=0; i<1; i++) {
    addPoint(random(width), random(height));
  }
  newTestBounds();
}

void addPoint(float x, float y) {
  PVector point = new PVector(x, y);
  points.add( point );
  PVector mov = PVector.random2D();
  movements.add( mov );
}

void mouseDragged() {
  float d = dist(mouseX, mouseY, pmouseX, pmouseY);
  int steps = ceil(d);
  if (steps<2) {
    addPoint(mouseX, mouseY);
  } else {
    for (int i=0; i<steps; i++) {
      float p = map(i, 0, steps-1, 0, 1);
      float x = lerp(pmouseX, mouseX, p);
      float y = lerp(pmouseY, mouseY, p);
      addPoint(x, y);
    }
  }
}
void mousePressed() {
  addPoint(mouseX, mouseY);
}
void keyPressed() {
  if (key=='s') 
    showTree = !showTree;
  if (key=='b') {
    newTestBounds();
  } else if (key=='i') {
    iterate = !iterate;
  }
}


void newTestBounds() {
  testBounds = createRandomRect();
  testBounds1 = createRandomRect();
}

Rect createRandomRect() {
  float x0 = random(width);
  float x1 = random(width);
  float y0 = random(height);
  float y1 = random(height);
  float x = min(x0, x1);
  float w = max(x0, x1)-x;
  float y = min(y0, y1);
  float h = max(y0, y1)-y;
  return new Rect(x, y, w, h);
}


void iterate() {
  // iterate movement
  for (int i=0; i<points.size(); i++) {
    PVector p = points.get(i);
    PVector m = movements.get(i);

    float n = noise(p.x/width*10, p.y/height*10, frameCount*0.01);
    float rot = map(n, 0, 1, -PI, PI)*0.02;
    m.rotate(rot);
    //m.mult(0.999);
    p.add(m);
    if (p.x<0) p.x+=width; 
    else if (p.x>=width) p.x-=width;
    if (p.y<0) p.y+=height; 
    else if (p.y>=height) p.y-=height;
  }
}


QuadTree buildTree() {

  // build tree
  QuadTree tree = new QuadTree(4, new Rect(0, 0, width, height) );
  for (PVector p : points) {
    tree.insertPoint(p.x, p.y);
  }

  return tree;
}



ArrayList<PVector> makeConnectingLinesWithTree(float maxLength, QuadTree tree) {
  ArrayList<PVector> lines = new ArrayList<PVector>();
  //Rect bound
  // will create duplicate lines, but that' ok for now


  float ml2 = maxLength*2;

  for (PVector p : points) {

    Rect bounds = new Rect(p.x-maxLength, p.y-maxLength, ml2, ml2);

    ArrayList<Point2D> foundPoints = tree.getPointsWithinBounds(bounds);
    for (Point2D point : foundPoints) {
      lines.add( p);
      lines.add( new PVector(xyPair[0], xyPair[1]) );
    }
  }

  return lines;
}


/*
  *  DRAW!
 */


void draw() {

  if (iterate) iterate();
  QuadTree tree = buildTree();

  ArrayList<PVector> lines = makeConnectingLinesWithTree(50, tree);


  //float x0 = 55, x1 = 355, y0 = 111, y1=208;

  //ArrayList<PVector> lines = makeLines(30, tree);

  background(255);



  // show points
  stroke(0);
  strokeWeight(2);
  beginShape(POINTS);
  for (PVector p : points) {
    vertex(p.x, p.y);
  }
  endShape();

  // show area
  rectMode(CORNER);
  noFill();
  strokeWeight(1);
  stroke(255, 0, 0);
  rect(testBounds.X0, testBounds.Y0, testBounds.WIDTH, testBounds.HEIGHT);
  //boolean testBounds1IntestBounds = testBounds.contains(testBounds1);
  //stroke(0, 255, 0);
  //if (testBounds1IntestBounds) strokeWeight(4);
  //rect(testBounds1.X0, testBounds1.Y0, testBounds1.WIDTH, testBounds1.HEIGHT);


  // show points in area
  strokeWeight(4);
  stroke(255, 0, 0);
  ArrayList<Point2D> areaPoints = tree.getPointsWithinBounds(testBounds);
  beginShape(POINTS);
  for (Point2D point : areaPoints) {
    vertex(point.x, point.y);
  }
  endShape();
  // show lines
  //stroke(0);
  //strokeWeight(1);
  //noFill();
  //beginShape(LINES);
  //for (PVector l : lines)
  //  vertex(l.x,l.y);
  //endShape();

  // show tree
  if (showTree) {
    strokeWeight(1);
    tree.show();
  }

  translate(0, 0, 1);
  rectMode(CORNER);
  strokeWeight(1);
  stroke(0);
  fill(255, 64);
  rect(5, 5, 200, 20);
  fill(0);
  text("points: "+points.size(), 10, 20);
}
