class Clocker {
  
  long lastTime;
  
  Clocker() {
    delta();
  }
  
  long delta() {
    long ti = System.currentTimeMillis();
    long dt = ti-lastTime;
    lastTime = ti;
    return dt;
  }
  
  
}
