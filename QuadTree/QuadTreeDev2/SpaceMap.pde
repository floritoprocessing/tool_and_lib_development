class SpaceMap {

  final float UNIT_SIZE;
  final float WIDTH;
  final float HEIGHT;

  final int unitAmountX;
  final int unitAmountY;
  final float xToUnitIndexFac;
  final float yToUnitIndexFac;

  final SpaceMapUnit[][] units;

  SpaceMap(float w, float h, float unitSize) {
    this.UNIT_SIZE = unitSize;
    this.WIDTH = w;
    this.HEIGHT = h;
    this.unitAmountX = (int)(w/unitSize);// i.e. w=100,s=5 -> 20 or w=101,s=5->21
    this.unitAmountY = (int)(h/unitSize);
    units = new SpaceMapUnit[unitAmountX][unitAmountY];
    for (int x=0; x<unitAmountX; x++) {
      for (int y=0; y<unitAmountY; y++) {
        units[x][y] = new SpaceMapUnit();
      }
    }
    xToUnitIndexFac = unitAmountX/w;
    yToUnitIndexFac = unitAmountY/h;
  }

  void insertPoint(float x, float y) {
    if (inRange(x, y)) {
      int indexX = (int)(x*xToUnitIndexFac); // i.e. w=101,s=5,amount=21: x=100.5 -> 100.5*21/101
      int indexY = (int)(y*yToUnitIndexFac);
      Point2D point = new Point2D(x, y);
      units[indexX][indexY].points.add( point );
    }
  }

  ArrayList<Point2D> getPointsAround(float x, float y) {
    ArrayList<Point2D> points = new ArrayList<Point2D>();
    
    int indexX = (int)(x*xToUnitIndexFac); // i.e. w=101,s=5,amount=21: x=100.5 -> 100.5*21/101
    int indexY = (int)(y*yToUnitIndexFac);
    int x0 = max(0,indexX-1);
    int x1 = min(unitAmountX-1, indexX+1);
    int y0 = max(0,indexY-1);
    int y1 = min(unitAmountY-1, indexY+1);
    for (int xi=x0;xi<=x1;xi++) {
      for (int yi=y0;yi<=y1;yi++) {
        points.addAll( units[xi][yi].points );
      }
    }
    
    return points;
  }

  boolean inRange(float x, float y) {
    return x>=0&&x<WIDTH&&y>=0&&y<HEIGHT;
  }
}


class SpaceMapUnit {

  final ArrayList<Point2D> points = new ArrayList<Point2D>();

  SpaceMapUnit() {
  }
}
