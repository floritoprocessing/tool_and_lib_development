final String ALGORITHM_TREE = "Tree";
final String ALGORITHM_BRUTE_FORCE = "Brute force";
final String ALGORITHM_SPACE_MAP = "Space map";
String algorithm = ALGORITHM_TREE;

ArrayList<PVector> points;
ArrayList<PVector> movements;

boolean showTree = false; // 's'
boolean iterate = true;   // 'i'
boolean edgeWrap = true;  // 'e'
boolean drawPoints = false; //  'p'
boolean drawLines = true; // 'l'

float LINE_DRAWING_STEP_SIZE = 1;
float LINES_MAX_DISTANCE = 10;
int MAX_POINTS = 10000;
int TREE_CAPACITY = 4;
Clocker treeGettingClocker = new Clocker();

int treeComparisons = 0;
int bruteComparisons = 0;
int spaceMapComparisons = 0;

void setup() {

  size(1200, 700, P3D);
  points = new ArrayList<PVector>();
  movements = new ArrayList<PVector>();

  for (int i=0; i<1; i++) {
    addPoint(random(width), random(height));
  }
}

void addPoint(float x, float y) {
  if (points.size()<MAX_POINTS) {
    PVector point = new PVector(x, y);
    points.add( point );
    PVector mov = PVector.random2D();
    movements.add( mov );
  }
}

void mouseDragged() {
  float d = dist(mouseX, mouseY, pmouseX, pmouseY)/LINE_DRAWING_STEP_SIZE;
  int steps = ceil(d);
  if (steps<2) {
    addPoint(mouseX, mouseY);
  } else {
    for (int i=0; i<steps; i++) {
      float p = map(i, 0, steps-1, 0, 1);
      float x = lerp(pmouseX, mouseX, p);
      float y = lerp(pmouseY, mouseY, p);
      addPoint(x, y);
    }
  }
}
void mousePressed() {
  addPoint(mouseX, mouseY);
}

void keyPressed() {
  if (key==CODED) {
    if (keyCode==UP) LINES_MAX_DISTANCE+=1;
    if (keyCode==DOWN) LINES_MAX_DISTANCE=max(1,LINES_MAX_DISTANCE-1);
  }
  else if (key=='1') algorithm = ALGORITHM_TREE;
  else if (key=='2') algorithm = ALGORITHM_BRUTE_FORCE;
  else if (key=='3') algorithm = ALGORITHM_SPACE_MAP;
  else if (key=='p')
    drawPoints = !drawPoints;
  else if (key=='l')
    drawLines = !drawLines;
  else if (key=='s') 
    showTree = !showTree;
  else if (key=='i') {
    iterate = !iterate;
  } else if (key=='e') {
    edgeWrap = !edgeWrap;
  }
}




void iterate() {
  // iterate movement
  for (int i=0; i<points.size(); i++) {
    PVector p = points.get(i);
    PVector m = movements.get(i);

    float n = noise(p.x/width*10, p.y/height*10, frameCount*0.01);
    float rot = map(n, 0, 1, -PI, PI)*0.02;
    m.rotate(rot);
    //m.mult(0.999);
    p.add(m);
    if (edgeWrap) {
      if (p.x<0) p.x+=width; 
      else if (p.x>=width) p.x-=width;
      if (p.y<0) p.y+=height; 
      else if (p.y>=height) p.y-=height;
    } else {
      if (p.x<0||p.x>=width||p.y<=0||p.y>=height) {
        points.remove(i);
        movements.remove(i);
        i--;
      }
    }
  }
}


QuadTree buildTree() {

  // build tree
  QuadTree tree = new QuadTree(TREE_CAPACITY, LINES_MAX_DISTANCE*2, new Rect(0, 0, width, height) );
  for (PVector p : points) {
    tree.insertPoint(p.x, p.y);
  }

  return tree;
}

SpaceMap buildSpaceMap() {
  SpaceMap spaceMap = new SpaceMap(width, height, LINES_MAX_DISTANCE);
  for (PVector p : points) {
    spaceMap.insertPoint(p.x, p.y);
  }
  return spaceMap;
}



ArrayList<PVector> makeConnectingLinesBruteForce(float distance) {
  ArrayList<PVector> lines = new ArrayList<PVector>();
  float distanceSquared = distance*distance;

  for (PVector p : points) {

    float x = p.x;
    float y = p.y;

    for (PVector q : points) {
      float dx = q.x-x;
      float dy = q.y-y;
      float dsq = dx*dx+dy*dy;
      bruteComparisons++;
      if (dsq<distanceSquared) {
        lines.add(p);
        lines.add(q);
      }
    }
  }

  return lines;
}


ArrayList<PVector> makeConnectingLinesWithTree(float distance, QuadTree tree) {
  ArrayList<PVector> lines = new ArrayList<PVector>();
  //Rect bound
  // will create duplicate lines, but that' ok for now


  float d2 = distance*2;
  float distanceSquared = distance*distance;

  for (PVector p : points) {

    float x = p.x;
    float y = p.y;
    Rect bounds = new Rect(x-distance, y-distance, d2, d2);

    ArrayList<Point2D> foundPoints = new ArrayList<Point2D>();
    tree.getPointsWithinBounds(bounds, foundPoints);
    for (Point2D foundPoint : foundPoints) {

      float dx = foundPoint.x-x;
      float dy = foundPoint.y-y;
      float dsq = dx*dx+dy*dy;
      treeComparisons++;
      if (dsq<distanceSquared) {      
        lines.add( p );
        lines.add( new PVector(foundPoint.x, foundPoint.y) );
      }
    }
  }

  return lines;
}




ArrayList<PVector> makeConnectingLinesWithSpaceMap(float distance, SpaceMap spaceMap) {
  ArrayList<PVector> lines = new ArrayList<PVector>();
  for (PVector p : points) {

    float x = p.x;
    float y = p.y;
    float distanceSquared = distance*distance;
    ArrayList<Point2D> foundPoints = spaceMap.getPointsAround(x, y);
    for (Point2D foundPoint : foundPoints) {
      float dx = foundPoint.x-x;
      float dy = foundPoint.y-y;
      float dsq = dx*dx+dy*dy;
      spaceMapComparisons++;
      if (dsq<distanceSquared) {      
        lines.add( p );
        lines.add( new PVector(foundPoint.x, foundPoint.y) );
      }
    }
  }
  return lines;
}


/*
  *  DRAW!
 */


void draw() {

  if (iterate) iterate();


  treeComparisons = 0;
  bruteComparisons = 0;
  spaceMapComparisons = 0;

  long dtConnectingLines = -1;
  long dtBuildingTree = -1;
  ArrayList<PVector> connectingLines = new ArrayList<PVector>();
  QuadTree tree = null;
  SpaceMap spaceMap = null;

  // GET LINES BY TREE
  if (algorithm==ALGORITHM_TREE) {
    treeGettingClocker.delta();
    tree = buildTree();
    dtBuildingTree = treeGettingClocker.delta();
    connectingLines = makeConnectingLinesWithTree(LINES_MAX_DISTANCE, tree);
    dtConnectingLines = treeGettingClocker.delta();
  } 

  // GET LINES BY BRUTE FORCE
  else if (algorithm == ALGORITHM_BRUTE_FORCE) {
    treeGettingClocker.delta();
    connectingLines = makeConnectingLinesBruteForce(LINES_MAX_DISTANCE);
    dtConnectingLines = treeGettingClocker.delta();
  } 

  // GET LINES BY SPACE MAP
  else if (algorithm == ALGORITHM_SPACE_MAP) {
    treeGettingClocker.delta();
    spaceMap = buildSpaceMap();
    dtBuildingTree = treeGettingClocker.delta();
    connectingLines = makeConnectingLinesWithSpaceMap(LINES_MAX_DISTANCE, spaceMap);
    dtConnectingLines = treeGettingClocker.delta();
  }


  background(255);



  // show points
  if (drawPoints) {
    stroke(0);
    strokeWeight(2);
    beginShape(POINTS);
    for (PVector p : points) {
      vertex(p.x, p.y);
    }
    endShape();
  }



  // show lines
  if (drawLines) {
    if (connectingLines!=null) {
      stroke(0, 64);
      strokeWeight(1);
      noFill();
      beginShape(LINES);
      for (PVector l : connectingLines)
        vertex(l.x, l.y);
      endShape();
    }
  }


  // show tree
  if (showTree && tree!=null) {
    strokeWeight(1);
    tree.show();
  }


  // show example line
  strokeWeight(2);
  stroke(0);
  line(23, height-23, 23+LINES_MAX_DISTANCE, height-23);

  // text
  translate(0, 0, 2);
  rectMode(CORNER);
  strokeWeight(1);
  stroke(0);
  fill(255);
  rect(5, 5, 230, 135);
  fill(0);
  String lineAmount = "" + (connectingLines.size()<1000 ? (connectingLines.size()) : ((int)(connectingLines.size()/1000)+"k"));
  text("points: "+points.size()+", dist="+LINES_MAX_DISTANCE+", fps="+nf(frameRate, 1, 1), 10, 20);
  text("[1] tree building: "+(int)(treeComparisons/1000)+"k", 10, 35);
  text("[2] brute-force: "+(int)(bruteComparisons/1000)+"k", 10, 50);
  text("[3] space map: "+(int)(spaceMapComparisons/1000)+"k", 10, 65);
  text("algorithm: "+algorithm, 10, 80);
  text("connectingLines amount: "+lineAmount, 10, 100);
  text("tree/space map building time: "+dtBuildingTree+"ms", 10, 115);
  text("connectingLines time: "+dtConnectingLines+"ms", 10, 130);
}
