class QuadTree {

  final int CAPACITY;
  final Rect bounds;
  final ArrayList<Point2D> points;
  final ArrayList<Point2D> pointsInMeAndSubtrees;

  boolean flag = false;
  boolean divided = false;
  QuadTree northWest;
  QuadTree northEast;
  QuadTree southWest;
  QuadTree southEast;

  QuadTree(int capacity, Rect bounds) {
    this.bounds = bounds;
    this.CAPACITY = capacity;
    points = new ArrayList<Point2D>();
    pointsInMeAndSubtrees = new ArrayList<Point2D>();
  }

  boolean inRange(float x, float y) {
    return bounds.contains(x, y);
  }
  boolean inRange(Point2D point) {
    return bounds.contains(point.x, point.y);
  }

  void insertPoint(float x, float y) {
    insertPoint( new Point2D(x, y) );
  }
  
  void insertPoint(Point2D point) {
    // insert only if within range
    if (inRange(point)) {
      pointsInMeAndSubtrees.add( point );
      
      if (points.size()<CAPACITY) {
        points.add( point );
      } else {

        subdivide();
        northWest.insertPoint( point );
        northEast.insertPoint( point );
        southWest.insertPoint( point );
        southEast.insertPoint( point );
      }
    }
  }

  void subdivide() {
    if (!divided) {
      int capacity = CAPACITY;
      float newWidth = bounds.WIDTH/2;
      float newHeight = bounds.HEIGHT/2;
      northWest = new QuadTree(capacity, new Rect(bounds.X0, bounds.Y0, newWidth, newHeight));
      northEast = new QuadTree(capacity, new Rect(bounds.AVERAGE_X, bounds.Y0, newWidth, newHeight));
      southWest = new QuadTree(capacity, new Rect(bounds.X0, bounds.AVERAGE_Y, newWidth, newHeight));
      southEast = new QuadTree(capacity, new Rect(bounds.AVERAGE_X, bounds.AVERAGE_Y, newWidth, newHeight));
      divided=true;
    }
  }


  ArrayList<Point2D> getPointsWithinBounds(Rect queryBounds) {
    ArrayList<Point2D> foundPoints = new ArrayList<Point2D>();

    // add ALL points of a quadtree of which the bounds are inside the query bounds
    // -> otherwise recurse into the four subtrees
    //    and also add points that are within the overlapping area of the two bounds

    if (queryBounds.contains(bounds)) {

      getAllPointsWithinMeAndSubtree( foundPoints );
    } else {

      Rect intersection = queryBounds.createIntersection( bounds );
      if (intersection!=null) {
        for (Point2D p : points) {
          if (intersection.contains(p.x, p.y)) {
            foundPoints.add(p);
          }
        }
      }

      if (divided) {
        foundPoints.addAll( northWest.getPointsWithinBounds(queryBounds) );
        foundPoints.addAll( northEast.getPointsWithinBounds(queryBounds) );
        foundPoints.addAll( southWest.getPointsWithinBounds(queryBounds) );
        foundPoints.addAll( southEast.getPointsWithinBounds(queryBounds) );
      }
    }





    return foundPoints;
  }


  void getAllPointsWithinMeAndSubtree( ArrayList<Point2D> output ) {
    output.addAll(pointsInMeAndSubtrees);
    //flag = true;
    //output.addAll(points);
    //if (divided) {
    //  northWest.getAllPointsWithinMeAndSubtree(output);
    //  northEast.getAllPointsWithinMeAndSubtree(output);
    //  southWest.getAllPointsWithinMeAndSubtree(output);
    //  southEast.getAllPointsWithinMeAndSubtree(output);
    //}
  }

  /**
   returns a sub group of points of which all are within the supplied distance
   */
  //ArrayList<float[]> getPointsInRange(float x, float y, float distance) {
  //  return getPointsInRangeSquared(x, y, distance*distance);
  //}

  //ArrayList<float[]> getPointsInRangeSquared(float x, float y, float distanceSquared) {

  //  ArrayList<float[]> xyPairs = new ArrayList<float[]>();

  //  for (int i=0; i<size; i++) {
  //    float px = xArr[i];
  //    float py = yArr[i];
  //    float dx = px-x;
  //    float dy = py-y;
  //    float dSq = dx*dx+dy*dy;
  //    if (dSq<distanceSquared) {
  //      xyPairs.add( new float[] { px, py } );
  //    }
  //  }
  //  if (northWest!=null) {
  //    if (inRange(x, y)) {
  //      xyPairs.addAll( northWest.getPointsInRangeSquared(x, y, distanceSquared) );
  //      xyPairs.addAll( northEast.getPointsInRangeSquared(x, y, distanceSquared) );
  //      xyPairs.addAll( southWest.getPointsInRangeSquared(x, y, distanceSquared) );
  //      xyPairs.addAll( southEast.getPointsInRangeSquared(x, y, distanceSquared) );
  //    }
  //  }

  //  return xyPairs;
  //}


  void show() {
    if (divided) {
      if (flag) {
        stroke(192, 0, 0);
        rectMode(CORNER);
        rect(bounds.X0, bounds.Y0, bounds.WIDTH, bounds.HEIGHT);
      }
      stroke(128);
      line(bounds.AVERAGE_X, bounds.Y0, bounds.AVERAGE_X, bounds.Y1);
      line(bounds.X0, bounds.AVERAGE_Y, bounds.X1, bounds.AVERAGE_Y);
      northWest.show();
      northEast.show();
      southWest.show();
      southEast.show();
    }
  }
}





class Rect {
  final float X0, X1, Y0, Y1, WIDTH, HEIGHT;
  final float AVERAGE_X, AVERAGE_Y;
  Rect(float x, float y, float w, float h) {
    if (w<=0||h<=0) throw new RuntimeException("Illegal Rect with w="+w+" and h="+h);
    this.X0=x;
    this.Y0=y;
    this.X1=x+w;
    this.Y1=y+h;
    this.WIDTH=w;
    this.HEIGHT=h;
    this.AVERAGE_X = x+w/2;
    this.AVERAGE_Y = y+h/2;
  }

  boolean contains(float x, float y) {
    return x>=X0&&x<X1&&y>=Y0&&y<Y1;
  }

  boolean contains(Rect other) {
    return other.X0>=X0 && other.X1<=X1 && other.Y0>=Y0 && other.Y1<=Y1;
  }

  Rect createIntersection(Rect other) {
    float tx1 = X0;
    float ty1 = Y0;
    float rx1 = other.X0;
    float ry1 = other.Y0;
    float tx2 = tx1; 
    tx2 += WIDTH;
    float ty2 = ty1; 
    ty2 += HEIGHT;
    float rx2 = rx1; 
    rx2 += other.WIDTH;
    float ry2 = ry1; 
    ry2 += other.HEIGHT;
    if (tx1 < rx1) tx1 = rx1;
    if (ty1 < ry1) ty1 = ry1;
    if (tx2 > rx2) tx2 = rx2;
    if (ty2 > ry2) ty2 = ry2;
    tx2 -= tx1;
    ty2 -= ty1;
    // tx2,ty2 will never overflow (they will never be
    // larger than the smallest of the two source w,h)
    // they might underflow, though...
    if (tx2 < Float.MIN_VALUE) tx2 = Float.MIN_VALUE;
    if (ty2 < Float.MIN_VALUE) ty2 = Float.MIN_VALUE;
    return new Rect(tx1, ty1, tx2, ty2);
  }
}


class Point2D {
  float x, y;
  Point2D(float x, float y) {
    this.x=x;
    this.y=y;
  }
}
