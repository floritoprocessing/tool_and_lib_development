final String ALGORITHM_TREE = "Tree";
final String ALGORITHM_BRUTE_FORCE = "Brute force";
String algorithm = ALGORITHM_TREE;

ArrayList<PVector> points;
ArrayList<PVector> movements;
boolean showTree = true;
boolean iterate = true;
boolean edgeWrap = false;
float LINES_MAX_DISTANCE = 30;
int MAX_POINTS = 5000;
Clocker treeGettingClocker = new Clocker();


void setup() {

  size(800, 400, P3D);
  points = new ArrayList<PVector>();
  movements = new ArrayList<PVector>();

  for (int i=0; i<1; i++) {
    addPoint(random(width), random(height));
  }
}

void addPoint(float x, float y) {
  if (points.size()<MAX_POINTS) {
    PVector point = new PVector(x, y);
    points.add( point );
    PVector mov = PVector.random2D();
    movements.add( mov );
  }
}

void mouseDragged() {
  float d = dist(mouseX, mouseY, pmouseX, pmouseY);
  int steps = ceil(d);
  if (steps<2) {
    addPoint(mouseX, mouseY);
  } else {
    for (int i=0; i<steps; i++) {
      float p = map(i, 0, steps-1, 0, 1);
      float x = lerp(pmouseX, mouseX, p);
      float y = lerp(pmouseY, mouseY, p);
      addPoint(x, y);
    }
  }
}
void mousePressed() {
  addPoint(mouseX, mouseY);
}

void keyPressed() {
  if (key=='1') algorithm = ALGORITHM_TREE;
  else if (key=='2') algorithm = ALGORITHM_BRUTE_FORCE;
  else if (key=='s') 
    showTree = !showTree;
  else if (key=='i') {
    iterate = !iterate;
  }
}




void iterate() {
  // iterate movement
  for (int i=0; i<points.size(); i++) {
    PVector p = points.get(i);
    PVector m = movements.get(i);

    float n = noise(p.x/width*10, p.y/height*10, frameCount*0.01);
    float rot = map(n, 0, 1, -PI, PI)*0.02;
    m.rotate(rot);
    //m.mult(0.999);
    p.add(m);
    if (edgeWrap) {
      if (p.x<0) p.x+=width; 
      else if (p.x>=width) p.x-=width;
      if (p.y<0) p.y+=height; 
      else if (p.y>=height) p.y-=height;
    } else {
      if (p.x<0||p.x>=width||p.y<=0||p.y>=height) {
        points.remove(i);
        movements.remove(i);
        i--;
      }
    }
  }
}


QuadTree buildTree() {

  // build tree
  QuadTree tree = new QuadTree(4, new Rect(0, 0, width, height) );
  for (PVector p : points) {
    tree.insertPoint(p.x, p.y);
  }

  return tree;
}



ArrayList<PVector> makeConnectingLinesBruteForce(float distance) {
  ArrayList<PVector> lines = new ArrayList<PVector>();
  float distanceSquared = distance*distance;
  
  for (PVector p : points) {

    float x = p.x;
    float y = p.y;
    
    for (PVector q : points) {
      float dx = q.x-x;
      float dy = q.y-y;
      float dsq = dx*dx+dy*dy;
      if (dsq<distanceSquared) {
        lines.add(p);
        lines.add(q);
      }
    }
    
  }
  
  return lines;
}
ArrayList<PVector> makeConnectingLinesWithTree(float distance, QuadTree tree) {
  ArrayList<PVector> lines = new ArrayList<PVector>();
  //Rect bound
  // will create duplicate lines, but that' ok for now


  float d2 = distance*2;
  float distanceSquared = distance*distance;

  for (PVector p : points) {

    float x = p.x;
    float y = p.y;
    Rect bounds = new Rect(x-distance, y-distance, d2, d2);

    ArrayList<Point2D> foundPoints = tree.getPointsWithinBounds(bounds);
    for (Point2D foundPoint : foundPoints) {

      float dx = foundPoint.x-x;
      float dy = foundPoint.y-y;
      float dsq = dx*dx+dy*dy;

      if (dsq<distanceSquared) {      
        lines.add( p );
        lines.add( new PVector(foundPoint.x, foundPoint.y) );
      }
    }
  }

  return lines;
}


/*
  *  DRAW!
 */


void draw() {

  if (iterate) iterate();


  long dtConnectingLines = -1;
  long dtBuildingTree = -1;
  ArrayList<PVector> connectingLines = new ArrayList<PVector>();
  QuadTree tree = null;

  if (algorithm==ALGORITHM_TREE) {
    treeGettingClocker.delta();

    tree = buildTree();

    dtBuildingTree = treeGettingClocker.delta();

    connectingLines = makeConnectingLinesWithTree(LINES_MAX_DISTANCE, tree);

    dtConnectingLines = treeGettingClocker.delta();
  }
  
  else if (algorithm == ALGORITHM_BRUTE_FORCE) {
    treeGettingClocker.delta();
    connectingLines = makeConnectingLinesBruteForce(LINES_MAX_DISTANCE);
    dtConnectingLines = treeGettingClocker.delta();
  }


  background(255);



  // show points
  stroke(0);
  strokeWeight(2);
  beginShape(POINTS);
  for (PVector p : points) {
    vertex(p.x, p.y);
  }
  endShape();



  // show lines
  if (connectingLines!=null) {
    stroke(0);
    strokeWeight(1);
    noFill();
    beginShape(LINES);
    for (PVector l : connectingLines)
      vertex(l.x, l.y);
    endShape();
  }


  // show tree
  if (showTree && tree!=null) {
    strokeWeight(1);
    tree.show();
  }


  // text
  translate(0, 0, 1);
  rectMode(CORNER);
  strokeWeight(1);
  stroke(0);
  fill(255, 64);
  rect(5, 5, 200, 120);
  fill(0);
  text("points: "+points.size(), 10, 20);
  text("[1] tree building", 10, 35);
  text("[2] brute-force", 10, 50);
  text("algorithm: "+algorithm, 10, 65);
  text("connectingLines amount: "+connectingLines.size(), 10, 85);
  text("tree building time: "+dtBuildingTree+"ms", 10, 100);
  text("connectingLines time: "+dtConnectingLines+"ms", 10, 115);
}
