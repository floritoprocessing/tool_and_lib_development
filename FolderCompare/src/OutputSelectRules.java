/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public enum OutputSelectRules {
	
	NOT_EQUALITY_FILES_EQUAL;
	
	public boolean select(FileDuo duo) {
		
		switch (this) {
		case NOT_EQUALITY_FILES_EQUAL:
			return duo.equality != Equality.FILES_EQUAL;
			//break;

		default:
			break;
		}
		
		return false;
	}
	
	
}
