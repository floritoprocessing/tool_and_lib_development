import java.io.File;
import java.io.FileFilter;
import java.util.Vector;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class Recurser {
	
	public static FileFilter UNHIDDEN_FILE_FILTER = new FileFilter() {
		public boolean accept(File arg0) {
			return !arg0.isHidden();
		}
	};
	
	private final int maxRecurseDepth;
	private final OutputSelectRules[] outputSelectRules;
	private final DirectoryRules[] directoryRules;
	
	public Recurser(int maxRecurseDepth, OutputSelectRules[] outputSelectRules, DirectoryRules[] directoryRules) {
		this.maxRecurseDepth = maxRecurseDepth;
		
		this.outputSelectRules = new OutputSelectRules[ outputSelectRules==null ? 0 : outputSelectRules.length ];
		if (outputSelectRules!=null)
		for (int i=0;i<outputSelectRules.length;i++) {
			this.outputSelectRules[i] = outputSelectRules[i];
		}
		
		this.directoryRules = new DirectoryRules[ directoryRules==null ? 0 : directoryRules.length ];
		if (directoryRules!=null)
		for (int i=0;i<directoryRules.length;i++) {
			this.directoryRules[i] = directoryRules[i];
		}
	}
	
	
	
	public Vector<FileDuo> go(File f0, File f1) {
		Vector<FileDuo> out = new Vector<FileDuo>();
		recurse(f0, f1, 0, out);
		return out;
	}
	
	private void recurse(File f0, File f1, int depth, Vector<FileDuo> out) {
		
		//System.out.println(f0.getName());
		File[] files0 = f0.listFiles(UNHIDDEN_FILE_FILTER);
		File[] files1 = f1.listFiles(UNHIDDEN_FILE_FILTER);
		
		Vector<FileDuo> duos = FileDuo.createDuos(files0, files1);
		
		for (int i=0;i<duos.size();i++) {
			
			FileDuo duo = duos.get(i);
			System.out.println(duo);
			
			if (duo.isDirectoryPair) {
				
				boolean recurse = depth<maxRecurseDepth;
				/*for (RecurserRules rule : rules) {
					recurse = recurse && rule.appliesTo(duo);
					if (!recurse) {
						break;
					}
				}*/
				addToOut(duo, out);
				if (recurse) {
					recurse(duo.file0, duo.file1, depth+1, out);
				}
			}
			
			else {
				addToOut(duo, out);
			}
			
		}
		
		
		
		
		
		//return out;
	}



	/**
	 * @param duo
	 * @param out
	 */
	private void addToOut(FileDuo duo, Vector<FileDuo> out) {
		boolean add = true;
		for (OutputSelectRules rule : outputSelectRules) {
			add = add && rule.select(duo);
			if (!add) break;
		}
		if (add) {
			out.add(duo);
		}
	}
}
