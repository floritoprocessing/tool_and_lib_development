import java.io.File;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */

public enum Equality {
	
	NONE       			 ("none",									".............."),
	LEFT_ONLY			 ("left only",								"<======......."),
	RIGHT_ONLY			 ("right only",								".......======>"),
	NAMES_DIFFERENT		 ("unequal names",							"~~~~~name~~~~~"),
	TYPES_DIFFERENT		 ("unequal types",							"~~~~~type~~~~~"),
	DIRECTORY_NAMES_EQUAL("equal directory (name)",					"<====[DR]====>"),
	FILES_EQUAL  		 ("equal files",							"<============>"),
	FILE_SIZE_DIFFERENCE  ("different file size",					"<===!SIZE!===>"),
	FILE_DATE_DIFFERENCE ("different file date",					"<===!DATE!===>"),
	FILE_SIZE_AND_DATE_DIFFERENCE ("different file size and date", 	"<==!SIZDAT!==>");
	
	public final String description;
	public final String symbol;
	
	private Equality(String description, String symbol) {
		this.description = description;
		this.symbol = symbol;
	}
	
	/**
	 * @param file0
	 * @param file1
	 * @return 
	 */
	public static Equality getEquality(File file0, File file1) {
		Equality equality;
		if (file0==null && file1==null) {
			equality = Equality.NONE;
		}
		else if (file0!=null && file1==null) {
			equality = Equality.LEFT_ONLY;
		}
		else if (file0==null && file1!=null) {
			equality = Equality.RIGHT_ONLY;
		}
		
		// both is file:
		else {
			
			if (!file0.getName().equals(file1.getName())) {
				equality = Equality.NAMES_DIFFERENT;
			}
			else if (file0.isFile() != file1.isFile()) {
				equality = Equality.TYPES_DIFFERENT;
			}
			else if (file0.isDirectory() != file1.isDirectory()) {
				equality = Equality.TYPES_DIFFERENT;
			}
			
			// both is same type: 
			else {
				
				// DIRECTORIES:
				if (file0.isDirectory()) {
					equality = Equality.DIRECTORY_NAMES_EQUAL;
				}
				
				// FILES
				else if (file0.isFile()){
					long d0 = file0.lastModified();
					long d1 = file1.lastModified();
					long s0 = file0.length();
					long s1 = file1.length();
					
					if (d0==d1 && s0==s1) {
						equality = Equality.FILES_EQUAL;
					}
					else if (d0!=d1 && s0==s1) {
						equality = Equality.FILE_DATE_DIFFERENCE;
					}
					else if (d0==d1 && s0!=s1) {
						equality = Equality.FILE_SIZE_DIFFERENCE;
					}
					else {
						equality = Equality.FILE_SIZE_AND_DATE_DIFFERENCE;
					}
				}
				
				else {
					System.err.println("Equality null for "+file0+" and "+file1);
					equality = null;
				}
				
			}
			
		}
		return equality;
	}
}
	
