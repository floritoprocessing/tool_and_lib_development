import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class FileDuo {
	
	public static int TO_STRING_MIN_NAME_LENGTH = 30;
	
	public final File file0;
	public final File file1;
	public final String name;
	public final boolean isDirectoryPair;
	
	public final Equality equality;
	
	public String getFilename0() {
		return file0==null ? "null" : file0.getAbsolutePath();
	}
	public String getFilename1() {
		return file1==null ? "null" : file1.getAbsolutePath();
	}
	
	public static Comparator<FileDuo> SORT_BY_DIR = new Comparator<FileDuo>() {
		@Override
		public int compare(FileDuo fd0, FileDuo fd1) {
			
			boolean d0 = fd0.isDirectoryPair;
			boolean d1 = fd1.isDirectoryPair;
			
			if (d0&&!d1) {
				return -1;
			} else if (d1&&!d0) {
				return 1;
			} else {
				return SORT_BY_NAME.compare(fd0, fd1);
			}
			
		}
	};
	
	public static Comparator<FileDuo> SORT_BY_NAME = new Comparator<FileDuo>() {
		public int compare(FileDuo o0, FileDuo o1) {
			return o0.name.compareTo(o1.name);
		}
	};
	
	
	
	/**
	 * Factory method
	 * @param list0
	 * @param list1
	 * @return
	 */
	public static Vector<FileDuo> createDuos(File[] files0, File[] files1) {
		
		Vector<FileDuo> out = new Vector<FileDuo>();
		
		Vector<File> list0 = new Vector<File>(Arrays.asList(files0));
		Vector<File> list1 = new Vector<File>(Arrays.asList(files1));
		
		for (int i=list0.size()-1;i>=0;i--) {
			
			File file0 = list0.get(i);
			String name0 = file0.getName();
			boolean dir0 = file0.isDirectory();
			boolean fil0 = file0.isFile();
			
			for (int j=list1.size()-1;j>=0;j--) {
				
				File file1 = list1.get(j);
				String name1 = file1.getName();
				boolean dir1 = file1.isDirectory();
				boolean fil1 = file1.isFile();
				
				if (name0.equals(name1)
						&& dir0 == dir1
						&& fil0 == fil1) {
					
					out.add(new FileDuo(file0, file1));
					list0.remove(file0);
					list1.remove(file1);
				}
				
			}
			
		}
		
		for (int i=list1.size()-1;i>=0;i--) {
			
			File file1 = list1.get(i);
			String name1 = file1.getName();
			boolean dir1 = file1.isDirectory();
			boolean fil1 = file1.isFile();
			
			for (int j=list0.size()-1;j>=0;j--) {
				
				File file0 = list0.get(j);
				String name0 = file0.getName();
				boolean dir0 = file0.isDirectory();
				boolean fil0 = file0.isFile();
				
				FileDuo duo = new FileDuo(file0, file1);
				if (!out.contains(duo)
						&& name1.equals(name0)
						&& dir1 == dir0
						&& fil1 == fil0) {
					
					out.add(new FileDuo(file0, file1));
					list0.remove(file0);
					list1.remove(file1);
				}
				
			}
			
		}
		
		for (File file0 : list0) {
			out.add(new FileDuo(file0, null));
		}
		for (File file1 : list1) {
			out.add(new FileDuo(null, file1));
		}
		
		// sort by dir
		Collections.sort(out, SORT_BY_DIR);

		return out;
		
	}
	
	private FileDuo(File f0, File f1) {
		this.file0 = f0;
		this.file1 = f1;
		this.name = file0!=null ? file0.getName() : (file1!=null ? file1.getName() : "null");
		this.isDirectoryPair = (file0!=null&&file1!=null ? file0.isDirectory() : false);
		this.equality = Equality.getEquality(f0, f1);
	}
	
	public String toString() {
		String s0 = file0==null ? 
				"null" : 
				(isDirectoryPair?"[":"")+file0.getAbsolutePath()+(isDirectoryPair?"]":"");
		String s1 = file1==null ? 
				"null" : 
				(isDirectoryPair?"[":"")+file1.getAbsolutePath()+(isDirectoryPair?"]":"");
		
		String compare = equality.symbol;
		
		while (s0.length()<TO_STRING_MIN_NAME_LENGTH) 
			s0 += " ";
		
		return s0 + compare + " " + s1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file0 == null) ? 0 : file0.hashCode());
		result = prime * result + ((file1 == null) ? 0 : file1.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileDuo other = (FileDuo) obj;
		if (file0 == null) {
			if (other.file0 != null)
				return false;
		} else if (!file0.equals(other.file0))
			return false;
		if (file1 == null) {
			if (other.file1 != null)
				return false;
		} else if (!file1.equals(other.file1))
			return false;
		return true;
	}
	
	
}
