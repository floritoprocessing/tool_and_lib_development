import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class FolderCompare extends JFrame implements MouseListener {
	

	private JButton leftButton;
	private JButton rightButton;
	
	private JLabel leftFolderPath;
	private JLabel rightFolderPath;
	
	private JRadioButton hideEqualFilesOption = new JRadioButton("Hide equal files"); 
	
	private JButton compare;
	
	private JTextArea leftText;
	private JTextArea middleText;
	private JTextArea rightText;
	
	private File leftFolder = new File("/Users/marcus/not iCloud/- Processing");
	private File rightFolder = new File("/Users/marcus/not iCloud/- Processing unpacked");

	public FolderCompare() {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JPanel left = new JPanel();
		left.setBorder(defaultBorder());
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
		leftButton = new JButton("Add Left");
		leftButton.addMouseListener(this);
		left.add(leftButton);
		leftFolderPath = new JLabel("Please select a folder: Path = .....");
		leftFolderPath.setBorder(defaultBorder());
		left.add(leftFolderPath);
		
		JPanel right = new JPanel();
		right.setBorder(defaultBorder());
		right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
		rightButton = new JButton("Add right");
		rightButton.addMouseListener(this);
		right.add(rightButton);
		rightFolderPath = new JLabel("Please select a folder:  Path = .....");
		rightFolderPath.setBorder(defaultBorder());
		right.add(rightFolderPath);
		
		JPanel leftAndRight = new JPanel();
		leftAndRight.setBorder(new LineBorder(new Color(0),1));
		leftAndRight.setLayout(new BoxLayout(leftAndRight, BoxLayout.LINE_AXIS));
		leftAndRight.add(left);
		leftAndRight.add(right);
		main.add(leftAndRight);
		
		JPanel rulePanel = new JPanel();
		rulePanel.setBorder(new LineBorder(new Color(1), 1));
		rulePanel.add(hideEqualFilesOption);
		main.add(rulePanel);
		
		JPanel comparePanel = new JPanel();
		comparePanel.setBorder(defaultBorder());
		compare = new JButton("Compare");
		compare.setEnabled(false);
		compare.setAlignmentX(0.5f);
		compare.addMouseListener(this);
		comparePanel.add(compare);
		main.add(comparePanel);
		
		JPanel texts = new JPanel();
		texts.setLayout(new BoxLayout(texts, BoxLayout.LINE_AXIS));
		leftText = new JTextArea(40, 50);
		leftText.setText("Left result");
		leftText.setFont(new Font("Consolas", Font.PLAIN, 12));
		leftText.setBorder(new LineBorder(new Color(0), 1));
		texts.add(leftText);
		
		middleText = new JTextArea(40, Equality.NONE.symbol.length());
		middleText.setText(Equality.NONE.symbol);
		middleText.setFont(new Font("Consolas", Font.PLAIN, 12));
		middleText.setBorder(new LineBorder(new Color(0), 1));
		texts.add(middleText);
		
		rightText = new JTextArea(40, 50);
		rightText.setText("Right result");
		rightText.setFont(new Font("Consolas", Font.PLAIN, 12));
		rightText.setBorder(new LineBorder(new Color(0), 1));
		texts.add(rightText);
		
		JScrollPane scroll = new JScrollPane(texts);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		main.add(scroll);
				
		setContentPane(main);
		
		testFolders();
		
		
	}
	
	
	
	
	private Border defaultBorder() {
		return new EmptyBorder(10, 10, 10, 10);
	}




	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent arg0) {
		Object source = arg0.getSource();
		if (source.equals(leftButton)) {
			
			leftFolder = chooseFolder("Left Folder", leftFolder, false);
			testFolders();
			
		} else if (source.equals(rightButton)) {
			
			rightFolder = chooseFolder("Right Folder", rightFolder, true);
			testFolders();
			
		} else if (source.equals(compare)) {
			
			leftText.setText("Result:"+NL);
			rightText.setText("Result:"+NL);
			middleText.setText(NL);
			
			Recurser recurser = new Recurser(1,
					new OutputSelectRules[] { 
						OutputSelectRules.NOT_EQUALITY_FILES_EQUAL },
					null);
			Vector<FileDuo> duos = recurser.go(leftFolder, rightFolder);
			
			for (FileDuo duo:duos) {
				leftText.append(duo.getFilename0()+NL);
				middleText.append(duo.equality.symbol+NL);
				rightText.append(duo.getFilename1()+NL);
			}
		}
	}
	
	
	private void testFolders() {
		if (leftFolder!=null) {
			leftFolderPath.setText(leftFolder.getAbsolutePath());
		}
		if (rightFolder!=null) {
			rightFolderPath.setText(rightFolder.getAbsolutePath());
		}
		if (leftFolder!=null && rightFolder!=null && 
				!leftFolder.equals(rightFolder)) {
			compare.setEnabled(true);
		} else {
			compare.setEnabled(false);
		}
	}



	
	
	
	
	

	/**
	 * @param title
	 * @return
	 */
	private File chooseFolder(String title, File root, boolean lastInList) {

		
		if (root==null) {
			File[] rootList = File.listRoots();
			if (lastInList) {
				root = rootList[rootList.length-1];
			} else {
				int prev = Math.max(0, rootList.length-2);
				root = rootList[prev];
			}
		}
		
		JFileChooser chooser = new JFileChooser(root);
		chooser.setDialogTitle(title);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setMultiSelectionEnabled(false);
		int result = chooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile();
		}
		else {
			return null;
		}
	}









	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}




	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}




	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}




	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	private static String NL = System.getProperty("line.separator");
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				
				FolderCompare fc = new FolderCompare();
				fc.setVisible(true);
				fc.pack();
				fc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
			}
		});
	}
}
