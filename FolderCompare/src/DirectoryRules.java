import java.io.File;
import java.util.Vector;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public enum DirectoryRules {

	UNEQUAL_DIRECTORY_LIST;
	
	public boolean appliesTo(FileDuo duo) {
		
		boolean result = false;
		
		switch (this) {
		case UNEQUAL_DIRECTORY_LIST:
			//result = checkUnequalDirectoryList(duo);
			break;

		default:
			result = false;
			break;
		}
		
		return result;
	}

	/**
	 * @return
	 */
//	private boolean checkUnequalDirectoryList(FileDuo duo) {
//		
//		File[] filesInDir0 = dir0.listFiles(Recurser.UNHIDDEN_FILE_FILTER);
//		File[] filesInDir1 = dir1.listFiles(Recurser.UNHIDDEN_FILE_FILTER);
//		
//		Vector<FileDuo> duos = FileDuo.createDuos(filesInDir0, filesInDir1);
//		boolean unequal = false;
//		for (FileDuo duo : duos) {
//			switch (duo.equality) {
//			case LEFT_ONLY:
//			case RIGHT_ONLY:
//			case FILE_DATE_DIFFERENCE:
//			case FILE_SIZE_DIFFERENCE:
//			case FILE_SIZE_AND_DATE_DIFFERENCE:
//				unequal = true;
//				break;
//
//			default:
//				break;
//			}
//			if (unequal) {
//				return unequal;
//			}
//		}
//		
//		return unequal;
//	}
}
