/*
*
* QColor_Tester
*
*/

QUtil qu = new QUtil();
QImage img;
QColor qCol = new QColor(0xFFFF,0x1800,0x1200);

void setup() {
  size(400,300,P3D);
  img = new QImage(400,300);
  frameRate(25);
}

void keyPressed() {
  if (key=='1') qu.setBlendMode(QUtil.BLEND_MODE_NORMAL);
  if (key=='2') qu.setBlendMode(QUtil.BLEND_MODE_ADD);
  if (key=='3') qu.setBlendMode(QUtil.BLEND_MODE_SUBTRACT);
  
  if (key=='c') qCol = new QColor(random(0xFFFF),random(0xFFFF),random(0xFFFF));
  if (key=='w') img.background(0xFFFF,0xFFFF,0xFFFF);
  if (key=='b') img.background(0,0,0);
}


void draw() {
  if (mousePressed) {
    //img.set(mouseX,mouseY,new QColor(0xFFFF,0x1000,0x1200)); // works!
    //qu.set(img,mouseX,mouseY,new QColor(0xFFFF,0x1000,0x1200),0.5); // works!
    //qu.set(img,mouseX+0.5,mouseY+0.5,new QColor(0xFFFF,0x1000,0x1200),0.5); // works!
    //qu.line(img,mouseX,mouseY,pmouseX,pmouseY,new QColor(0xFFFF,0x1000,0x1200),0.5); // works!
    //qu.line(img,(float)mouseX,mouseY,pmouseX,pmouseY,new QColor(0xFFFF,0x1000,0x1200),0.5); // works!
    for (int i=0;i<100;i++) {
      float rd0=random(TWO_PI);
      float ra0=80*sqrt(random(1.0));
      float x0=mouseX + ra0*cos(rd0);
      float y0=mouseY + ra0*sin(rd0);
      float rd1=random(TWO_PI);
      float ra1=80*sqrt(random(1.0));
      float x1=pmouseX + ra1*cos(rd1);
      float y1=pmouseY + ra1*sin(rd1);
      qu.line(img,x0,y0,x1,y1,qCol,0.05); // works!
    }
  }

  image(img.asPImage(),0,0);
}
