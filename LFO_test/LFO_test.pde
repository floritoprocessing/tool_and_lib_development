
LFO lfo1, lfo2;
int lastX=0, lastY=0;

void setup() {
  size(400,400);
  background(255,255,255);
  
  lfo1=new LFO(0,2,0.25);
  lfo2=new LFO(0,2,0);
}

void draw() {
  background(255,255,255);
  
  lfo1.update();
  lfo2.update();
  
  lfo1.addPhaseOffset(0.001);
  
  int nX=200+(int)(150*lfo1.getAmp());
  int nY=200+(int)(150*lfo2.getAmp());
  
  line(nX,nY,lastX,lastY);
  lastX=nX;
  lastY=nY;
}
