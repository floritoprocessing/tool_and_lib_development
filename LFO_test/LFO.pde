class LFO {
  
  // LFO TYPE:
  // 0=sin;
  int type=0;
  
  // FREQUENCY:
  double freq=1;
  
  // PHASE OFFSET in CYCLES (0..1)
  double phaseOffset=0;
  
  
  
  double cycle=0;
  double amp=0;
  long lastUpdate;
  
  LFO(int _type, double _freq, double _phaseOffset) {
    type=_type;
    freq=_freq;
    phaseOffset=_phaseOffset;
    lastUpdate=System.nanoTime();
  }
  
  void setPhaseOffset(double _phaseOffset) {
    phaseOffset=_phaseOffset;
  }
  
  void addPhaseOffset(double _apo) {
    phaseOffset+=_apo;
    if (phaseOffset>=1.0) {phaseOffset-=1.0;}
  }
  
  void update() {
    long now=System.nanoTime();
    long nanoTime=now-lastUpdate;
    double sec=nanoTime/1000000000.0;
    lastUpdate=now;
    
    cycle+=freq*sec;
    if (cycle>=1.0) {cycle-=1.0;}
    amp=Math.sin((cycle+phaseOffset)*2*Math.PI);
  }
    
  double getAmp() {
    return amp;
  }
  
}
