import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import net.florito.filerecurser.FileRecurser;
import net.florito.filerecurser.FileRecurserListener;


public class SourceCodeModifierTester implements FileRecurserListener {

	
	final String ROOT_FOLDER = "D:\\- Java\\SourceCodeModifier\\test\\";
	//final String ROOT_FOLDER = "D:\\Dropbox\\eFashionApp\\eFashionAppNoComments 20140527\\src";
	final String EXTENSION = ".as";
	
	FileRecurser fr = new FileRecurser();
	
	CommentRemover commentRemover = new CommentRemover();
	
	public SourceCodeModifierTester() {
		
		//commentRemover.setTempFileDirectory(new File(ROOT_FOLDER));//new File("D:\\- Java\\SourceCodeModifier\\temp"));
		
		fr.addListener(this);
		fr.useThread(true);
		fr.setFileFilter(new FileFilter() {
			public boolean accept(File arg0) {
				return arg0.getName().toLowerCase().endsWith(EXTENSION);
			}
		});
		fr.start(new File(ROOT_FOLDER));
		
	}
	
	
	
	int selectFile = 0;

	public void onFile(File arg0) {
		// TODO Auto-generated method stub
		
		//if (selectFile==15) {

			System.out.println(arg0);
			
			try {
				commentRemover.readFile(arg0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		//}
		
		selectFile++;
	}
	
	
	

	public void onBeginDirectory(File arg0) {
		// TODO Auto-generated method stub
		
	}




	public void onEndDirectory(File arg0) {
		// TODO Auto-generated method stub
		
	}


	public void onRecursionComplete() {
		// TODO Auto-generated method stub
		
	}




	public void onRecursionInterrupted() {
		// TODO Auto-generated method stub
		
	}
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		new SourceCodeModifierTester();
		
	}



}
