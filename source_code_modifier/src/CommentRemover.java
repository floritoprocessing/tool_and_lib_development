import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;


public class CommentRemover {

	/*private File tempFileDirectory = null;
	
	public void setTempFileDirectory(File tempFileDirectory) {
		if (tempFileDirectory.exists() && tempFileDirectory.isFile()) {
			this.tempFileDirectory = tempFileDirectory;
		}
	}*/

	public CommentRemover() {
	}
	
	
	
	public void readFile(File srcFile) throws IOException {
		
		String fileName = srcFile.getName();
		
		// create a backup file
		File backupFile = new File(srcFile.getParentFile(), srcFile.getName()+".bak");
		Files.copy(srcFile.toPath(), backupFile.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
		
		FileReader reader = new FileReader(backupFile);
		FileWriter writer = new FileWriter(srcFile);
		
		int r;
		char ch=(char)0;
		char lastCh=(char)0;
		boolean firstChar = true;
		boolean inQuote = false;
		boolean inLineComment = false;
		boolean inBlockComment = false;
		
		int skipCount = 0;
		while ((r=reader.read())!=-1) {
			
			ch = (char)r;
			
			if (ch=='"') {
				inQuote = !inQuote;
			}
			
			if (!inQuote) {
				
				if (!inLineComment && !inBlockComment) {
					if (lastCh=='/' && ch=='/') {
						inLineComment = true;
						skipCount = 2;
					}
					else if (lastCh=='/' && ch=='*') {
						inBlockComment = true;
						skipCount = 2;
					}
				}
				
				if (inLineComment && lastCh=='\r' || lastCh=='\n') {
					inLineComment = false;
				}
				else if (inBlockComment && lastCh=='*' && ch=='/') {
					inBlockComment = false;
					skipCount = 2;
				}
			}
			
			
			if (firstChar && !inLineComment && !inBlockComment) {
				writer.write(ch);
			}
			else if (!firstChar && !inLineComment && !inBlockComment && skipCount==0) {
				writer.write(ch);
			}
			
			skipCount--;
			skipCount = Math.max(skipCount, 0);
			
			lastCh = ch;
			firstChar = false;
		}
		
		/*if (lastCh!=(char)0 && !inLineComment && !inBlockComment) {
			writer.write(ch);
		}*/
		
		
		reader.close();
		writer.close();
		
		
		//Files.copy()
		
	}


}
