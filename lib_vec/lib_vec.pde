void setup() {
  size(400,300);
}

void loop() {
}

///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Vec class ////////////////////////////////////////

class Vec {
  float x=0,y=0,z=0;
  Vec() {}
  Vec(float _x, float _y) {x=_x;y=_y;}
  Vec(float _x, float _y, float _z) {x=_x;y=_y;z=_z;}
  Vec(Vec v) {x=v.x;y=v.y;z=v.z;}
  void add(Vec v) {x+=v.x;y+=v.y;z+=v.z;}
  void sub(Vec v) {x-=v.x;y-=v.y;z-=v.z;}
  void mul(float n) {x*=n;y*=n;z*=n;}
  void div(float n) {x/=n;y/=n;z/=n;}
  float magnitude() { return sqrt(x*x+y*y+z*z); }
 
  void rotX(float rd) { float SI=sin(rd), CO=cos(rd); float ny=y*CO-z*SI, nz=z*CO+y*SI; y=ny; z=nz; }
  void rotY(float rd) { float SI=sin(rd), CO=cos(rd); float nx=x*CO-z*SI, nz=z*CO+x*SI; x=nx; z=nz; }
  void rotZ(float rd) { float SI=sin(rd), CO=cos(rd); float nx=x*CO-y*SI, ny=y*CO+x*SI; x=nx; y=ny; }
}

Vec add(Vec v, Vec w) { return (new Vec(v.x+w.x,v.y+w.y,v.z+w.z)); }
Vec sub(Vec v, Vec w) { return (new Vec(v.x-w.x,v.y-w.y,v.z-w.z)); }
Vec mul(Vec v, float n) { return (new Vec(v.x*n,v.y*n,v.z*n)); }
Vec div(Vec v, float n) { return (new Vec(v.x/n,v.y/n,v.z/n)); }

//////////////////////////////////////// end Vec class ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
