PImage mercator; int mercW, mercH;
int scl;
float longStep, latStep, ballRadius, ballCircum, rectSize;
float mainYrot;

void setup() {
  // init screen:
  size(640,480,P3D); background(255); noStroke(); fill(128); rectMode(DIAMETER);
  // load mercator map:
  mercator=loadImage("earth.jpg"); mercW=mercator.width; mercH=mercator.height;
  // init drawing sizes:
  float degStep=360.0;
  longStep=TWO_PI/degStep; latStep=TWO_PI/degStep;
  ballRadius=200; ballCircum=ballRadius*TWO_PI; rectSize=ballCircum/degStep;
  mainYrot=0;
}

void draw() {
  //image(mercator,0,0);
  background(255);
  mainYrot+=0.01;
  translate(width/2,height/2);
  rotateY(mainYrot);
  for (float rLong=0;rLong<TWO_PI;rLong+=longStep) {
    pushMatrix();
    rotateY(rLong);
    for (float rLat=-HALF_PI;rLat<HALF_PI;rLat+=latStep) {
      pushMatrix();
        rotateX(rLat);
        translate(0,0,ballRadius);
        float s=cos(rLat);
        float mapX=rLong/TWO_PI*mercW;
        float mapY=(HALF_PI-rLat)/TWO_PI*mercH;
        fill(mercator.get(int(mapX),int(mapY)));
        rect(0,0,rectSize*s,rectSize*s);
      popMatrix();
    }
    popMatrix();
  }
  
}
