PImage mercator; float mercW, mercH;
int scl;
float longStep, latStep, ballRadius, ballCircum, rectSize;
float mainYrot;

void setup() {
  // init screen:
  size(640,480,P3D); background(255); noStroke(); fill(128); rectMode(DIAMETER);
  // load mercator map:
  mercator=loadImage("earth.jpg"); mercW=mercator.width; mercH=mercator.height;
  // init drawing sizes:
  float degStep=360.0;
  longStep=TWO_PI/mercW; 
  latStep=PI/mercH;
  ballRadius=200; ballCircum=ballRadius*TWO_PI; rectSize=ballCircum/degStep;
  mainYrot=0;
  
  
}

float x,y,z,xn,yn,zn;

void draw() {
  //image(mercator,0,0);
  background(255);
  mainYrot+=0.05;
  rotateY(mainYrot);
  float rLong=0;
  for (int mercX=0;mercX<mercW/2;mercX+=1) {
    rLong-=longStep*1;
    float rLat=-HALF_PI;
    float SIN=sin(rLong+mainYrot), COS=cos(rLong+mainYrot);
    for (int mercY=0;mercY<mercH;mercY+=1) {
      rLat+=latStep*1;
      x=0;y=ballRadius*sin(rLat);z=ballRadius*cos(rLat);
      xn=x*COS-z*SIN; yn=y; zn=z*COS+x*SIN;
      Pos2D dot;
      dot=conv3Dto2D(xn,yn,zn);
      set(int(dot.x)+width/2,int(dot.y)+height/2,mercator.get(mercX,mercY));
    }
  }
  
}

Pos2D conv3Dto2D(float x, float y, float z) {
  float D=500;
  float pRat=D/(D-z);
  return (new Pos2D(x*pRat,y*pRat));
}

class Pos2D {
  float x,y;
  Pos2D(float inX, float inY) {
    x=inX; y=inY;
  }
}
