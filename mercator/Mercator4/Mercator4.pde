PImage mercator; float mercW, mercH;
float mercXoffset, mercXoffAdd;
int fine=1;
float radius=100;
float D=1200;
boolean overArrow;


Pos3D[][] mercDot;

void setup() {
  size(300,300,P3D); background(32);
  initGlobe();
  drawArrow(-1,false); drawArrow(1,false);
  mercXoffset=0; mercXoffAdd=0; overArrow=false;
}

void drawArrow(int dir,boolean active) {
  pushMatrix();
    translate(width/2.0+dir*117,height/2.0);
    noStroke(); 
    if (active) {fill(255);} else {fill(224);}
    beginShape(POLYGON);
      vertex(-dir*10,5); vertex(dir*10,5); 
      vertex(dir*10,15); vertex(dir*25,0); vertex(dir*10,-15);
      vertex(dir*10,-5); vertex(-dir*10,-5);
    endShape();
  popMatrix();
}


void draw() {
  float dir, da1, da2;
  da1=dist(width/2-124,height/2,mouseX,mouseY);
  da2=dist(width/2+124,height/2,mouseX,mouseY);
  if (da1<17) {
    overArrow=true; drawArrow(-1,true);
  } else if (da2<17) {
    overArrow=true; drawArrow(1,true);
  } else {
    overArrow=false; drawArrow(-1,false); drawArrow(1,false);
  }
  
  // set map x-offset:
  if (overArrow) {
    dir=mouseX<width/2?-8:8;
    mercXoffAdd=(12*mercXoffAdd+dir)/13.0;
  } else {
    dir=0;
    mercXoffAdd=(mercXoffAdd+dir)/2.0;
  }
  
  // rotate map on globe:
  mercXoffset+=mercXoffAdd;
  while (mercXoffset<0) {mercXoffset+=mercW;}
  for (int xIndex=0;xIndex<fine*(mercW/2.0);xIndex++) {
    for (int yIndex=0;yIndex<fine*mercH;yIndex++) {
      color c=mercator.get(int((xIndex+mercXoffset)%mercW)/fine,yIndex/fine);
      set(int(mercDot[xIndex][yIndex].x+width/2),int(mercDot[xIndex][yIndex].y+height/2),c);
    }
  }
}

void initGlobe() {
  mercator=loadImage("mars1.gif"); mercW=mercator.width; mercH=mercator.height;
  mercDot=new Pos3D[fine*(int)mercW][fine*(int)mercH];
  for (int xIndex=0;xIndex<fine*(mercW/2.0);xIndex++) {
    float dLong=HALF_PI-(xIndex/(fine*mercW/2.0))*PI;
    float SIN=sin(dLong), COS=cos(dLong);
    for (int yIndex=0;yIndex<fine*mercH;yIndex++) {
      float dLat=(yIndex/(fine*mercH))*PI-HALF_PI;
      float x=0, y=radius*sin(dLat), z=radius*cos(dLat);
      float xn=x*COS-z*SIN, yn=y, zn=z*COS+x*SIN;      // 3D POSITION
      float ar=D/(D-zn);
      float xs=ar*xn, ys=ar*yn;                        // SCREEN POSITION
      mercDot[xIndex][yIndex]=new Pos3D(xs,ys,0);
    }
  }
}

class Pos3D {
  float x,y,z;
  Pos3D(float inx, float iny, float inz) {
    x=inx; y=iny; z=inz;
  }
}

void bigSet(int x, int y, color c) {
  set(x-1,y-1,c); set(x,y-1,c); set(x+1,y-1,c);
  set(x-1,y,c); set(x,y,c); set(x+1,y,c);
  set(x-1,y+1,c); set(x,y+1,c); set(x+1,y+1,c);
}
