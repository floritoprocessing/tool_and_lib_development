PImage mercator; int mercW, mercH;
int scl;
float longStep, latStep, ballRadius, ballCircum, rectXSize, rectYSize;
float mainYrot;

void setup() {
  // init screen:
  size(640,480,P3D); background(255); noStroke(); fill(192); rectMode(DIAMETER);
  // load mercator map:
  mercator=loadImage("earth.jpg"); mercW=mercator.width; mercH=mercator.height;
  // init drawing sizes:
  ballRadius=200; ballCircum=ballRadius*TWO_PI; 
  rectXSize=ballCircum/(float)mercW;
  rectYSize=(ballCircum/2.0)/(float)mercH;
  mainYrot=0;
}

void draw() {
  //image(mercator,0,0);
  background(255);

  translate(width/2,height/2);
  mainYrot+=0.03;
  rotateY(mainYrot);
  for (int x=0;x<mercW;x++) {
    pushMatrix();
    float rLong=x/(float)mercW*TWO_PI;
    
    if (rLong<PI) {
      rotateY(rLong);
      for (int y=0;y<mercH;y++) {
        pushMatrix();
          float rLat=(y/(float)mercH)*PI-HALF_PI;
          rotateX(rLat);
          translate(0,0,ballRadius);
          float s=cos(rLat);
          fill(mercator.get(x,y));
          rect(0,0,s*rectXSize,s*rectYSize);
        popMatrix();
      }
    }
    
    popMatrix();
  }
  
}
