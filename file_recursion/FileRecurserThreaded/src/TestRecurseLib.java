import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

import net.florito.recurse.RecurseListener;
import net.florito.recurse.Recurser;


public class TestRecurseLib implements RecurseListener{

	final List<String> extensions = Arrays.asList(new String[] {
			".wav", ".aif", ".mp3"
	});
	FileFilter audioFileFilter = new FileFilter() {
		@Override
		public boolean accept(File file) {
			String name = file.getName().toLowerCase();
			for (String ext:extensions) {
				if (name.endsWith(ext)) {
					return true;
				}
			}
			return false;
		}
	};
	
	final List<String> names = Arrays.asList(new String[] {
			"$RECYCLE.BIN",
			"- Native Instruments Libraries",
			"- Artificial Composer Knowledge",
			".metadata", ".hg", "tmp"
	});
	FileFilter specialDirectories = new FileFilter() {
		@Override
		public boolean accept(File file) {
			String name = file.getName();
			if (names.contains(name)) {
				return false;
			} else {
				return true;
			}
		}
	};
	
	
	
	
	
	public TestRecurseLib() {
		
		System.out.println("start");
		
		// test threaded recursion
		final Recurser recurser = new Recurser();
		recurser.addListener(this);
		//recurser.setThreadedRecursing(true);
		//recurser.enableThreadedResults(1500);
		//recurser.setFileFilter(audioFileFilter);
		recurser.setDirectoryFilter(specialDirectories);
//		recurser.start(new File("D:\\"),0);
		recurser.start(File.listRoots()[0]);
		
		/*(new Thread(new Runnable() {
			@Override
			public void run() {
				int i=0;
				while (i<5) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i++;
					//System.out.println(i+"------------------------------------------");
				}
				recurser.stop();
			}
		})).start();*/
		
		// test recursion with filters
		/*Recurser recurser = new Recurser();
		recurser.addListener(this);
		recurser.setFileFilter(audioFileFilter);
		recurser.setDirectoryFilter(specialDirectories);
		recurser.start(new File("D:\\"));*/
		
		// test standard recursion:
		/*Recurser recurser = new Recurser();
		recurser.addListener(this);
		recurser.start(new File("D:\\"));*/
		
	}
	
	
	
	



	@Override
	public void enterDirectory(File directory) {
		// TODO Auto-generated method stub
		//System.out.println("Entering directory "+directory);
	}



	@Override
	public void resultFile(File file) {
		// TODO Auto-generated method stub
		System.out.println("Result file "+file.getName()+"\t\t"+file.getPath());
	}



	@Override
	public void exitDirectory(File directory) {
		// TODO Auto-generated method stub
		//System.out.println("Exiting directory "+directory);
	}
	
	
	
	@Override
	public void recursionEnded() {
		// TODO Auto-generated method stub
		System.out.println("Recursion done.");
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestRecurseLib();
	}







	

}
