package net.florito.recurse;

import java.io.File;

public interface RecurseListener {
	
	/**
	 * Get's called when the Recurser enters a new directory
	 * @param directory
	 */
	public void enterDirectory(File directory);
	
	/**
	 * Get's called when the Recurser encounters a new File
	 * @param file
	 */
	public void resultFile(File file);
	
	/**
	 * Get's called when the Recurser leaves the directory (after passing through all sub Files)
	 * @param directory
	 */
	public void exitDirectory(File directory);

	/**
	 * Get's called when the Recurser is done
	 */
	public void recursionEnded();
	
}
