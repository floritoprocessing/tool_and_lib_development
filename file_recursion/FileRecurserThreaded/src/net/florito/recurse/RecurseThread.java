package net.florito.recurse;

public class RecurseThread extends Thread {

	long lastPauseTime;
	boolean running = false;

	public RecurseThread(Runnable target) {
		super(target);
	}
	
	public void start() {
		running = true;
		super.start();
	}
	

}
