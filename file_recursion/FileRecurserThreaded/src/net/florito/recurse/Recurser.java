package net.florito.recurse;

import java.io.File;
import java.io.FileFilter;
import java.util.Vector;

import net.florito.recurse.ResultThread.ResultType;

public class Recurser {
	
	private Vector<RecurseListener> listeners = new Vector<RecurseListener>();
	
	private FileFilter fileFilter;
	private FileFilter directoryFilter;
	
	private boolean running = false;
	
	private ResultThread resultThread = null;
	//private long threadedResultsInterval = 1000;
	
	/**
	 * Pause interval. Pause every n milliseconds for PAUSE TIME
	 */
	public static long RECURSE_THREAD_PAUSE_INTERVAL = 40;
	/**
	 * Puase time. Pause every PAUSE_INTERVAL for n milliseconds;
	 */
	public static long RECURSE_THREAD_PAUSE_TIME = 1;
	
	private RecurseThread recurseThread = null;
	private boolean threadedRecursing = false;
	
	/**
	 * Creates a new Recurser
	 */
	public Recurser() {
		setFileFilter(null);
		setDirectoryFilter(null);
	}
	
	/**
	 * Enable / disable threaded recursing
	 * @param value
	 */
	public void setThreadedRecursing(boolean value) {
		if (!running) {
			threadedRecursing = value;
			if (threadedRecursing) {
				if (recurseThread!=null) {
					recurseThread.running = false;
				}
			}
			recurseThread = null;
		} else {
			throw new RuntimeException("Can't enable/disable threading while running");
		}
	}
	
	
	/**
	 * Enables threading of results. Results will be posted at regular intervals.
	 * @param threadedResultsInterval
	 */
	public void enableThreadedResults(long threadedResultsInterval) {
		if (!running) {
			if (resultThread!=null) {
				resultThread.running = false;
			}
			resultThread = new ResultThread(this, threadedResultsInterval);
			resultThread.start();
			//this.threadedResultsInterval = threadedResultsInterval;
		} else {
			throw new RuntimeException("Can't enable/disable threading while running");
		}
	}
	
	/**
	 * Disables the threading of Results.
	 */
	public void disableThreadedResults() {
		if (!running) {
			if (resultThread!=null) {
				resultThread.running = false;
				resultThread = null;
			}
		} else {
			throw new RuntimeException("Can't enable/disable threading while running");
		}
	}
	
	
	/*
	 * 
	 * 
	 * Filters
	 * 
	 * 
	 */
	
	
	/**
	 * Sets a combined filter. On top of the supplied properties, the file filter will always filter out files where isFile() results true.<br>
	 * Setting filter to null, will result in a file-only file filter.
	 * @param filter
	 */
	public void setFileFilter(final FileFilter filter) {
		if (!running) {
			fileFilter = new FileFilter() {
				public boolean accept(File file) {
					if (file.isFile()) {
						if (filter!=null) {
							return filter.accept(file);
						}
						else {
							return true;
						}
					}
					return false;
				}
			};
		} else {
			throw new RuntimeException("Can't change filter while running!");
		}
	}
	
	/**
	 * Sets a combined filter. On top of the supplied properties, the file filter will always filter out files where isDirectory() results true.<br>
	 * Setting filter to null, will result in a directory-only file filter.
	 * @param filter
	 */
	public void setDirectoryFilter(final FileFilter filter) {
		if (!running) {
			directoryFilter = new FileFilter() {
				public boolean accept(File file) {
					if (file.isDirectory()) {
						if (filter!=null) {
							return filter.accept(file);
						}
						else {
							return true;
						}
					}
					return false;
				}
			};
		} else {
			throw new RuntimeException("Can't change filter while running!");
		}
	}
	
	
	/*
	 * 
	 * 
	 * Listener stuff
	 * 
	 * 
	 */
	
	/**
	 * Remove all listeners
	 */
	public void clearListeners() {
		listeners.clear();
	}
	
	/**
	 * Add a listener
	 * @param listener
	 */
	public void addListener(RecurseListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}
	
	
	/*
	 * 
	 * 
	 * Traversing
	 * 
	 * 
	 */
	
	/**
	 * Start recursing
	 * @param startDirectory
	 */
	public void start(File startDirectory) {
		start(startDirectory, -1);
	}
	
	/** 
	 * Start recursing
	 * @param startDirectory
	 * @param maxRecursionDepth 0 for this directory, 1 for one down, -1 for no limit
	 */
	public void start(final File startDirectory, final int maxRecursionDepth) {
		if (!running) {
			if (	startDirectory==null ||
					!startDirectory.exists() || 
					!startDirectory.isDirectory()) {
				throw new RuntimeException("Start directory is not a directory. "+startDirectory);
			}
			
			if (!threadedRecursing) {
				running = true;
				recurse(startDirectory, 0, maxRecursionDepth);
				running = false;
			} else {
				running = true;
				recurseThread = new RecurseThread(new Runnable() {
					@Override
					public void run() {
						//while (recurseThread!=null && recurseThread.running) {
							recurse(startDirectory, 0, maxRecursionDepth);
							if (resultThread==null) {
								recursionEndedToListeners();
							} else {
								resultThread.enqueueRecursionEnded();
							}
						//}
					}
				});
				recurseThread.lastPauseTime = System.currentTimeMillis();
				recurseThread.start();
			}
			
			if (recurseThread==null) {
				if (resultThread==null) {
					recursionEndedToListeners();
				} else {
					resultThread.enqueueRecursionEnded();
				}
			}
		} else {
			throw new RuntimeException("Can't start while running!");
		}
	}
	
	/**
	 * Stops the Recurser and kills a possible resultThread
	 */
	public void stop() {
		if (!running) {
			// ignoring
		} else {
			if (resultThread!=null) {
				resultThread.running = false;
				resultThread = null;
			}
			if (recurseThread!=null) {
				recurseThread.running = false;
				recurseThread = null;
			}
			running = false;
			System.out.println("STOOOOOOOOOOOOOOOOOOOOOOOOOP!!!!!");
		}
	}

	/*
	 * 
	 * 
	 * Recursion
	 * 
	 * 
	 */

	private void recurse(File dir, int recursionDepth, int maxRecursionDepth) {
		if (running && (maxRecursionDepth==-1 || recursionDepth<=maxRecursionDepth)) {
			
			if (recurseThread!=null) {
				long ti = System.currentTimeMillis();
				if (recurseThread.lastPauseTime + RECURSE_THREAD_PAUSE_INTERVAL < ti) {
					recurseThread.lastPauseTime = ti;
					try {
						Thread.sleep(RECURSE_THREAD_PAUSE_TIME);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			// get files
			File[] files = dir.listFiles(fileFilter);
			if (files!=null) {
				for (File file:files) {
					resultFile(file);
				}
			}
			
			
			
			File[] directories = dir.listFiles(directoryFilter);
			if (directories!=null) {
				for (File directory:directories) {
					enterDirectory(directory);
					recurse(directory, recursionDepth+1, maxRecursionDepth);
					exitDirectory(directory);
				}
			}
			
		}
	}

	/*
	 * 
	 * 
	 * Posting results
	 * 
	 * 
	 */

	private void resultFile(File file) {
		if (resultThread==null) {
			resultFileToListeners(file);
		} else {
			resultThread.addResult(file, ResultType.RESULT_FILE);
		}
	}
	
	void resultFileToListeners(File file) {
		for (RecurseListener listener:listeners) {
			listener.resultFile(file);
		}
	}


	private void enterDirectory(File directory) {
		if (resultThread==null) {
			enterDirectoryToListeners(directory);
		} else {
			resultThread.addResult(directory, ResultType.ENTER_DIRECTORY);
		}
	}
	
	void enterDirectoryToListeners(File directory) {
		for (RecurseListener listener:listeners) {
			listener.enterDirectory(directory);
		}
	}


	private void exitDirectory(File directory) {
		if (resultThread==null) {
			exitDirectoryToListeners(directory);
		} else {
			resultThread.addResult(directory, ResultType.EXIT_DIRECTORY);
		}
	}

	void exitDirectoryToListeners(File directory) {
		for (RecurseListener listener:listeners) {
			listener.exitDirectory(directory);
		}
	}
	
	void recursionEndedToListeners() {
		for (RecurseListener listener:listeners) {
			listener.recursionEnded();
		}
	}


	
	
}
