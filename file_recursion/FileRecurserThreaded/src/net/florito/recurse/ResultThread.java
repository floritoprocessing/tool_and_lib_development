package net.florito.recurse;

import java.io.File;
import java.util.Vector;

class ResultThread extends Thread {

	static enum ResultType {
		ENTER_DIRECTORY,
		RESULT_FILE,
		EXIT_DIRECTORY
	}
	
	private Recurser parent;
	private long interval;
	
	private Vector<File> results = new Vector<File>();
	private Vector<ResultType> resultTypes = new Vector<ResultType>(); 
	boolean endEnqueued = false;
	
	boolean running = false;
	
	ResultThread(Recurser parent, long interval) {
		super();
		this.parent = parent;
		this.interval = Math.max(0,interval);
	}
	
	public void start() {
		running = true;
		super.start();
	}
	
	public void addResult(File file, ResultType resultType) {
		synchronized (this) {
			results.add(file);
			resultTypes.add(resultType);
		}
	}
	
	public void run() {
		while (running) {
			
			synchronized (this) {
				int amount = results.size();
				File file;
				ResultType type;
				for (int i=0;i<amount;i++) {
					file = results.get(i);
					type = resultTypes.get(i);
					switch (type) {
					case ENTER_DIRECTORY:
						parent.enterDirectoryToListeners(file);
						break;
					case RESULT_FILE:
						parent.resultFileToListeners(file);
						break;
					case EXIT_DIRECTORY:
						parent.exitDirectoryToListeners(file);
						break;
					default:
						break;
					}
				}
				results.clear();
				resultTypes.clear();
				
				if (endEnqueued) {
					endEnqueued = false;
					parent.recursionEndedToListeners();
					running = false;
				}
			}
			
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void enqueueRecursionEnded() {
		endEnqueued = true;
	}
	
}
