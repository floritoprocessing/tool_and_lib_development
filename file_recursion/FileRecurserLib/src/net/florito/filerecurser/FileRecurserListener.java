package net.florito.filerecurser;

import java.io.File;

public interface FileRecurserListener {
	
	/**
	 * Gets called when starting to parse a directory
	 * @param directory
	 */
	public void onBeginDirectory(File directory);
	
	/**
	 * Gets called when coming across a file
	 * @param file
	 */
	public void onFile(File file);
	
	/**
	 * Gets called when parsing the entire directory has finished
	 * @param directory
	 */
	public void onEndDirectory(File directory);
	
	/**
	 * Gets called when the recursion has been interrupted through {@link FileRecurser#stopThreadedRecursion()}
	 */
	public void onRecursionInterrupted();
	
	/**
	 * Gets called when recursion has completed.
	 */
	public void onRecursionComplete();

}
