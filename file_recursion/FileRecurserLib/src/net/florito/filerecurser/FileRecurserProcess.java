package net.florito.filerecurser;

import java.io.File;
import java.io.FileFilter;

public class FileRecurserProcess extends Thread {

	private boolean running = true;
	public void stopRunning() {
		running = false;
	}
	
	private boolean interrupted = false;
	
	private final File startDirectory;
	private final FileFilter fileFilter;
	private final FileFilter directoryFilter;
	private final FileRecurserListener[] listeners;
	
	
	/**
	 * 
	 * @param startDirectory
	 * @param includeHiddenFiles use hidden files
	 * @param suppliedFileFilter can be null
	 * @param suppliedDirectoryFilter can be null
	 * @param listeners
	 */
	FileRecurserProcess(
			final File startDirectory, 
			final boolean includeHiddenFiles,
			final FileFilter suppliedFileFilter,
			final FileFilter suppliedDirectoryFilter,
			final FileRecurserListener[] listeners) {
		
		this.startDirectory = startDirectory;
		this.listeners = listeners;
		
		if (suppliedFileFilter==null) {
			this.fileFilter = new FileFilter() {
				public boolean accept(File file) {
					if (includeHiddenFiles) {
						return file.isFile();
					} else {
						return file.isFile() && !file.isHidden();
					}
				}
			};
		} else {
			this.fileFilter = new FileFilter() {
				public boolean accept(File file) {
					if (includeHiddenFiles) {
						return file.isFile() && suppliedFileFilter.accept(file);
					} else {
						return file.isFile() && !file.isHidden() && suppliedFileFilter.accept(file);
					}
				}
			};
		}
		
		if (suppliedDirectoryFilter==null) {
			this.directoryFilter = new FileFilter() {
				public boolean accept(File file) {
					if (includeHiddenFiles) {
						return file.isDirectory();
					} else {
						return file.isDirectory() && !file.isHidden();
					}
				}
			};
		} else {
			this.directoryFilter = new FileFilter() {
				public boolean accept(File file) {
					if (includeHiddenFiles) {
						return file.isDirectory() && suppliedDirectoryFilter.accept(file);
					} else {
						return file.isDirectory() && !file.isHidden() && suppliedDirectoryFilter.accept(file);
					}
				}
			};
		}
		
		
	}
	
	/**
	 * Starts recursing as thread
	 */
	@Override
	public void run() {
		startProcess();
	}
	
	

	/**
	 * If started from here, won't thread!
	 */
	public void startProcess() {
		running = true;
		recurse(startDirectory);
		if (interrupted) {
			for (FileRecurserListener listener:listeners) {
				listener.onRecursionInterrupted();
			}
		}
		else {
			for (FileRecurserListener listener:listeners) {
				listener.onRecursionComplete();
			}
		}
		running = false;
	}	
	
	
	
	private void recurse(File directory) {
		
		if (!running) {
			interrupted = true;
			return;
		}
		
		// begin directory
		for (FileRecurserListener listener:listeners) {
			listener.onBeginDirectory(directory);
		}
		
		// recurse directories
		File[] directories = directory.listFiles(directoryFilter);
		if (directories!=null) {
			for (File dir:directories) {
				recurse(dir);
			}
		}
		
		// parse files
		File[] files = directory.listFiles(fileFilter);
		if (files!=null) {
			for (File file:files) {
				for (FileRecurserListener listener:listeners) {
					listener.onFile(file);
				}
				if (!running) {
					interrupted = true;
					return;
				}
			}
		}
		
		// end directory
		for (FileRecurserListener listener:listeners) {
			listener.onEndDirectory(directory);
		}
	}
	
	
	
	
	
}
