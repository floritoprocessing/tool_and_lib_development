package net.florito.filerecurser;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

/**
 * The FileRecurser starts at a directory and recurses through all sub directories.
 * <p>
 * You can have it threaded or non-threaded by using {@link #usesThread()} (default is no threading).
 * If you are using a thread, you can stop it with {@link #stopThreadedRecursion()}.
 * <p> 
 * You can add special file and directory filters by using {@link #setFileFilter(FileFilter)} and {@link #setDirectoryFilter(FileFilter)}.
 * You can control listeners with {@link #addListener(FileRecurserListener)}, 
 * {@link #removeListener(FileRecurserListener)}, and {@link #clearListeners()}. <br>
 * 
 * 
 * <p>
 * The process of the FileRecurser happens in the following way:
 * <ul>
 * <li>Begin directory - reflected in the method {@link FileRecurserListener#onBeginDirectory(java.io.File)}</li>
 * <li>iterate through sub directories</li>
 * <li>iterate through files - reflected in the method {@link FileRecurserListener#onFile(java.io.File)}</li>
 * <li>End directory - reflected in the method {@link FileRecurserListener#onEndDirectory(java.io.File)} </li>
 * </ul>
 * At the end of the recursion either one of the two methods is called:
 * <ul>
 * <li>{@link FileRecurserListener#onRecursionInterrupted()} - if the recursion was interrupted through {@link #stopThreadedRecursion()}</li>
 * <li>{@link FileRecurserListener#onRecursionComplete()} - if the recursion has completed.</li>
 * </ul>
 * 
 * 
 * @author Marcus Graf
 *
 */
public class FileRecurser implements FileRecurserListener {
		
	//TODO: Add mini example to documentation
	//TODO: Add max depth
	
	/*
	 * 
	 * THREADING
	 * 
	 */
	
	
	/**
	 * Is the FileRecurser using a thread?
	 */
	private boolean useThread = false;
	
	/**
	 * Is the FileRecurser using a thread?
	 * @return if using a thread
	 */
	public boolean usesThread() {
		return useThread;
	}

	/**
	 * Is the FileRecurser using a thread? Can only be set if it is not currently recursing on a thread.
	 * Will return the status of using threads. 
	 * @see #usesThread()
	 * @param useThread
	 * @return the threaded status
	 */
	public boolean useThread(boolean useThread) {
		if (!threadActive) {
			this.useThread = useThread;
		}
		return useThread;
	}

	
	
	
	
	
	/*
	 * 
	 * THREAD ACTIVE
	 * 
	 */
	
	
	/**
	 * FileRecurser currently threading?
	 */
	private boolean threadActive = false;
	
	/**
	 * Returns if true if the thread of the FileRecurser is currently active.
	 * @return if the thread is active
	 */
	public boolean isCurrentlyThreading() {
		return threadActive;
	}

	
	
	
	
	

	/*
	 * 
	 * Listeners
	 * 
	 */
	
	private ArrayList<FileRecurserListener> listeners = new ArrayList<FileRecurserListener>();
	
	/**
	 * Add listener to the FileRecurser.
	 * If the listener is already present, it won't add it.<br>
	 * A listener can only be added when no thread is active, otherwise will throw a {@link RuntimeException}.
	 * @param listener
	 * @throws RuntimeException when thread is active
	 */
	public void addListener(FileRecurserListener listener) {
		if (!threadActive) {
			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		} else {
			throw new RuntimeException(this+".addListener(): Can't add, thread is active.");
		}
	}
	
	/**
	 * Removes a listener. Returns true if it has been successfully removed.<br>
	 * A listener can only be removed when no thread is active, otherwise will throw {@link RuntimeException}.
	 * @param listener
	 * @throws RuntimeException when thread is active
	 * @return if a listener has been removed
	 */
	public boolean removeListener(FileRecurserListener listener) {
		if (!threadActive) {
			return listeners.remove(listener);
		} else {
			throw new RuntimeException(this+".removeListener(): Can't remove, thread is active.");
		}
	}
	
	/**
	 * Removes all listeners.<br>
	 * All listeners can only be removed when no thread is active, otherwise will throw {@link RuntimeException}.
	 * @throws RuntimeException when thread is active
	 */
	public void clearListeners() {
		if (!threadActive) {
			listeners.size();
		} else {
			throw new RuntimeException(this+".clearListeners(): Can't clear, thread is active.");
		}
	}
	
	
	
	
	/*
	 * 
	 * HIDDEN FILES
	 * 
	 */
	
	private boolean includeHiddenFiles;
	
	/**
	 * Returns true if the FileRecurer includes hidden files.
	 * @return if hidden files are included
	 */
	public boolean isIncludeHiddenFiles() {
		return includeHiddenFiles;
	}

	/**
	 * Sets to include/exclude hidden files. <br>
	 * Can only be changed when no thread is active, otherwise will throw {@link RuntimeException}.
	 * @param includeHiddenFiles
	 * @throws RuntimeException when thread is active.
	 */
	public void setIncludeHiddenFiles(boolean includeHiddenFiles) {
		if (!threadActive) {
			this.includeHiddenFiles = includeHiddenFiles;
		} else {
			throw new RuntimeException(this+".setIncludeHiddenFiles(): Can't set, thread is active.");
		}
	}
	
	
	
	
	
	/*
	 * 
	 * FILE AND DIRECTORY FILTERS
	 *  
	 */
	
	
	private FileFilter fileFilter = null;
	
	/**
	 * Returns the fileFilter
	 * @return the fileFilter
	 */
	public FileFilter getFileFilter() {
		return fileFilter;
	}

	/**
	 * Sets the fileFilter, if the thread is not active.
	 * @param fileFilter
	 * @throws RuntimeException if the thread is active
	 */
	public void setFileFilter(FileFilter fileFilter) {
		if (!threadActive) {
			this.fileFilter = fileFilter;
		} else {
			throw new RuntimeException(this+".setFileFilter() ERROR: thread is active.");
		}
	}
	
	
	
	private FileFilter directoryFilter = null;
	
	/**
	 * Returns the directoryFilter
	 * @return the directoryFilter
	 */
	public FileFilter getDirectoryFilter() {
		return directoryFilter;
	}

	/**
	 * Sets the directoryFilter, if the thread is not active.
	 * @param directoryFilter
	 * @throws RuntimeException if the thread is active
	 */
	public void setDirectoryFilter(FileFilter directoryFilter) {
		if (!threadActive) {
			this.directoryFilter = directoryFilter;
		} else {
			throw new RuntimeException(this+".setDirectoryFilter() ERROR: thread is active.");
		}
	}
	
	
	
	
	
	
	/*
	 * 
	 * STOP THREADING
	 * 
	 */
	
	/**
	 * Stops the threaded recursion if active.
	 */
	public void stopThreadedRecursion() {
		if (threadActive) {
			process.stopRunning();
		}
	}
	
	
	
	
	
	
	/*
	 * 
	 * THREAD
	 * 
	 */
	
	FileRecurserProcess process = null;
	
	
	
	
	/*
	 * 
	 * CONSTRUCTOR
	 * 
	 */
	
	

	

	public FileRecurser() {
		
	}
	
	
	
	
	/*
	 * 
	 * START
	 * 
	 */
	
	
	/**
	 * Start recursion on the root directory.<br>
	 * Can only be started if no thread is active, otherwise will throw {@link RuntimeException}.
	 * @param rootDirectory
	 * @throws RuntimeException when thread is active
	 * @throws RuntimeException when directory is null
	 * @throws RuntimeException when directory does not exist
	 * @throws RuntimeException when supplied file is not a directory
	 */
	public void start(File rootDirectory) {
		if (threadActive) {
			throw new RuntimeException(this+".start() ERROR: Thread is active.");
		}
		else if (rootDirectory==null) {
			throw new RuntimeException(this+".start() ERROR: Root directory is null.");
		}
		else if (!rootDirectory.exists()) {
			throw new RuntimeException(this+".start() ERROR: Root directory does not exist: "+rootDirectory);
		} 
		else if (!rootDirectory.isDirectory()) {
			throw new RuntimeException(this+". start() ERROR: Root directory is not a directory: "+rootDirectory);
		}
		/*else if (!includeHiddenFiles && rootDirectory.isHidden()) {
			throw new RuntimeException(this+". start() ERROR: Root directory is hidden: "+rootDirectory);
		}*/
		// OK!
		else {
			
			// add itself to listeners
			FileRecurserListener[] theListeners = new FileRecurserListener[listeners.size()+1];
			for (int i=0;i<theListeners.length-1;i++) {
				theListeners[i] = listeners.get(i);
			}
			theListeners[theListeners.length-1] = this;
			
			process = new FileRecurserProcess(
					rootDirectory, 
					includeHiddenFiles, 
					fileFilter, 
					directoryFilter, 
					listeners.toArray(new FileRecurserListener[0]));
			
			if (useThread) {
				threadActive = true;
				process.start();
			}
			
			else {
				process.startProcess();
			}
			
		}
		
	}

	public void onBeginDirectory(File directory) {
		// TODO Auto-generated method stub
		
	}

	public void onFile(File file) {
		// TODO Auto-generated method stub
		
	}

	public void onEndDirectory(File directory) {
		// TODO Auto-generated method stub
		
	}

	public void onRecursionInterrupted() {
		threadActive = false;
	}

	public void onRecursionComplete() {
		threadActive = false;
	}
	
	
	
	
	
	
}
