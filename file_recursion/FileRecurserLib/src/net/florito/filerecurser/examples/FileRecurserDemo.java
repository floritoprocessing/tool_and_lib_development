package net.florito.filerecurser.examples;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import net.florito.filerecurser.FileRecurser;
import net.florito.filerecurser.FileRecurserListener;

public class FileRecurserDemo implements ActionListener, FileRecurserListener {

	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new FileRecurserDemo();
			}			
		});
		
	}
	
	private JFrame frame;
	private JToggleButton useThread;
	private JToggleButton includeHidden;
	private JToggleButton justMp3s;
	private JButton chooseDirectory;
	private JButton cancelThread;
	
	private JLabel result;
	private JLabel status;
	
	private boolean running = false;
	private int fileCount = 0;
	private int dirCount = 0;
	private FileRecurser recurser;
	
	public FileRecurserDemo() {
		
		Border border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		
		JPanel main = new JPanel();
		main.setBorder(border);
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JLabel title = new JLabel("FileRecurser Demo");
		title.setBorder(border);
		main.add(title);
		
		JPanel options = new JPanel();
		options.setLayout(new BoxLayout(options, BoxLayout.PAGE_AXIS));
		options.setBorder(border);
		
		useThread = new JToggleButton("Don't use thread");
		useThread.setSelected(false);
		useThread.addActionListener(this);
		options.add(useThread);
		
		includeHidden = new JToggleButton("Exclude hidden files");
		includeHidden.setSelected(false);
		includeHidden.addActionListener(this);
		options.add(includeHidden);
		
		justMp3s = new JToggleButton("All files");
		justMp3s.setSelected(false);
		justMp3s.addActionListener(this);
		options.add(justMp3s);
		
		main.add(options);
		
		chooseDirectory = new JButton("Choose directory");
		chooseDirectory.addActionListener(this);
		main.add(chooseDirectory);
		
		cancelThread = new JButton("Cancel thread");
		cancelThread.addActionListener(this);
		cancelThread.setEnabled(false);
		main.add(cancelThread);
		
		result = new JLabel("Files=0, Directories=0");
		result.setBorder(border);
		main.add(result);
		
		status = new JLabel("Status: waiting to start");
		status.setBorder(border);
		main.add(status);
		
		frame = new JFrame("FileRecurser Demo");
		frame.getContentPane().add(main);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = frame.getSize();
		frame.setLocation((screenSize.width-frameSize.width)/2, (screenSize.height-frameSize.height)/2);
	}

	public void actionPerformed(ActionEvent ev) {
		Object source = ev.getSource();
		
		if (source.equals(useThread)) {
			if (useThread.isSelected()) {
				useThread.setText("Use thread");
			} else {
				useThread.setText("Don't use thread");
			}
		}
		
		else if (source.equals(includeHidden)) {
			if (includeHidden.isSelected()) {
				includeHidden.setText("Include hidden files");
			} else {
				includeHidden.setText("Ignore hidden files");
			}
		}
		
		else if (source.equals(justMp3s)) {
			if (justMp3s.isSelected()) {
				justMp3s.setText("Just mp3s");
			} else {
				justMp3s.setText("All files");
			}
		}
		
		else if (source.equals(chooseDirectory)) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Choose a directory");
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = chooser.showOpenDialog(frame);
			if (returnVal==JFileChooser.APPROVE_OPTION) {
				startRecursion(chooser.getSelectedFile(), includeHidden.isSelected());
			}
		}
		
		else if (source.equals(cancelThread) && cancelThread.isEnabled()) {
			recurser.stopThreadedRecursion();
		}
	}

	
	private void startRecursion(File dir, boolean includeHidden) {
		
		//System.out.println("Start recursion, hiddenFiles? "+includeHidden);
		
		setRunning(true);
		
		fileCount=0;
		dirCount=0;
		updateResultLabel();
		
		status.setText("Start recursing");	
		
		recurser = new FileRecurser();
		recurser.useThread(useThread.isSelected());
		if (justMp3s.isSelected()) {
			final FileFilter mp3Filter = new FileFilter() {
				public boolean accept(File file) {
					return file.getName().toLowerCase().endsWith(".mp3");
				}
			};
			recurser.setFileFilter(mp3Filter);
		}
		recurser.addListener(this);
		
		
		recurser.start(dir);
	}
	
	
	private void updateResultLabel() {
		result.setText("Files="+fileCount+", Directories="+dirCount);
	}
	

	private void setRunning(boolean b) {
		this.running = b;
		useThread.setEnabled(!running);
		includeHidden.setEnabled(!running);
		chooseDirectory.setEnabled(!running);
		justMp3s.setEnabled(!running);
		
		if (running && useThread.isSelected()) {
			cancelThread.setEnabled(true);
		}
		else if (!running) {
			cancelThread.setEnabled(false);
		}
	}
	
	/*
	 * 
	 * Recursion listeners
	 * 
	 */
	
	public void onBeginDirectory(File directory) {
		status.setText("Current directory: "+directory.getName());
		dirCount++;
		updateResultLabel(); // just to show that without threading we don't see it
	}

	public void onFile(File file) {
		System.out.println(file);
		fileCount++;
		updateResultLabel(); // just to show that without threading we don't see it
	}

	public void onEndDirectory(File directory) {
	}

	public void onRecursionInterrupted() {
		updateResultLabel();
		status.setText("Recursion interrupted.");
		setRunning(false);
	}

	public void onRecursionComplete() {
		updateResultLabel();
		status.setText("Recursion completed.");
		setRunning(false);
	}

	
	
	
	
}
