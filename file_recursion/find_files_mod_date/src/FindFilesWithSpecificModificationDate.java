import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;


public class FindFilesWithSpecificModificationDate {
	

	private static boolean SHOW_FILENAMES = true;
	private static boolean SHOW_CURRENT_DIRECTORY = true;
	
//	public static File rootPath = new File("D:\\");
	private static Calendar cal = Calendar.getInstance();
	
	public static long minModifiedDate;
	public static long maxModifiedDate;
	static {
		cal = Calendar.getInstance();
		cal.set(2019, 1, 24, 0, 0, 0); // 1 feb 2019
		minModifiedDate = cal.getTime().getTime();
		cal.set(2019, 1, 25, 0, 0, 0); // 25 feb 2019
		maxModifiedDate = cal.getTime().getTime();
	}
	
	public static String resultsDirectoriesFileName = "foldersToBeBackedUp.txt";
	public static String resultsFilesFileName = "filesToBeBackedUp.txt";
	
	private PrintWriter resultsDirectories;
	private PrintWriter resultsFiles;
	
	public static void main(String[] args) throws IOException {
		new FindFilesWithSpecificModificationDate();
	}
	
	ArrayList<File> folders;
	
	public FindFilesWithSpecificModificationDate() throws IOException {
	
		File outputFolder = null;
		try {
			File binFolder = new File(getClass().getResource("").toURI());
			outputFolder = new File(binFolder.getParentFile(), "output");
			outputFolder.mkdir();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File[] roots = File.listRoots();
		System.out.println("File roots:");
		for (int i=0;i<roots.length;i++) {
			System.out.println("["+i+"]: "+roots[i]);
		}
		File rootChosen = null;
		rootChosen = roots[0];
		rootChosen = new File("/Users/marcus/not iCloud");
		
		if (rootChosen==null) {
			System.out.println("Please choose a root drive");
			return;
		}
		if (outputFolder==null) {
			return;
		}
		
		System.out.println("Recurse root: ");
		System.out.println(rootChosen);
		
		File resultsDirectoriesFile = new File(outputFolder, resultsDirectoriesFileName);
		File resultsFilesFile = new File(outputFolder, resultsFilesFileName);
		System.out.println("Creating files: ");
		System.out.println(resultsDirectoriesFile);
		System.out.println(resultsFilesFile);
		
		folders = new ArrayList<File>();
		
		FileWriter fwDirs = new FileWriter(resultsDirectoriesFile, false);
		FileWriter fwFiles = new FileWriter(resultsFilesFile, false);
		resultsDirectories = new PrintWriter(fwDirs);
		resultsFiles = new PrintWriter(fwFiles);
		
		cal = Calendar.getInstance();
		String today = cal.getTime().toString();
		cal.setTimeInMillis(minModifiedDate);
		String from = cal.getTime().toString();
		cal.setTimeInMillis(maxModifiedDate);
		String to = cal.getTime().toString();
		
		resultsDirectories.printf("%s"+"%n", "Directories with files modified\r\nfrom "+from+"\r\nto "+to+".\r\nToday is "+today);
		resultsFiles.printf("%s"+"%n", "Files modified\r\nfrom "+from+"\r\nto "+to+".\r\nToday is "+today);
		resultsDirectories.println();
		resultsFiles.println();
		
		recurse(rootChosen);
		
		resultsDirectories.close();
		resultsFiles.close();
		
		System.out.println("Results written to "+resultsDirectoriesFile.getAbsolutePath());
		System.out.println("Results written to "+resultsFilesFile.getAbsolutePath());
	}
	
	
	void recurse(File folder) {
	
		if (SHOW_CURRENT_DIRECTORY)
			System.out.println("----- "+folder.getAbsolutePath()+" -----");
		
		File[] theFiles = folder.listFiles(filesWithSpecificDate);
		if (SHOW_FILENAMES)
			if (theFiles!=null)
				for (File file : theFiles) {
					cal.setTimeInMillis(file.lastModified());
					System.out.println(file.getName()+"\t\t"+cal.getTime());
				}
		
		if (theFiles!=null && theFiles.length>0) {
			
			System.out.println("FOLDER ADDED: "+folder.getAbsolutePath());
			folders.add(folder);
			resultsDirectories.println(folder.getAbsolutePath());
			
			for (File file : theFiles) {
				resultsFiles.println(file.getAbsolutePath());
			}
			
		}
		
		
		File[] theDirectories = folder.listFiles(directoryFilter);
		if (theDirectories!=null) {
			for (File dir : theDirectories) {
				recurse(dir);
			}
		}
		
	}
	
	
	private static FileFilter filesWithSpecificDate = new FileFilter() {
		@Override
		public boolean accept(File file) {
			if (file.isFile()) {
				long modDate = file.lastModified();
				if (modDate>=minModifiedDate&&modDate<maxModifiedDate) {
					return true;
				}
			}
			return false;
		}
	};
	
	private static FileFilter directoryFilter = new FileFilter() {
		@Override
		public boolean accept(File file) {
			return file.isDirectory() && !file.isHidden();
		}
		
	};
	
	

}
