customFunction cf=new customFunction();

void setup() {
  //for (double t=0; t<cf.getTimeAtIndex(cf.last); t+=30) {
  //for (double t=0; t<=cf.getTimeAtIndex(5); t+=30) {
  //for (double t=cf.getTimeAtIndex(5); t<cf.getTimeAtIndex(78); t+=30) {
  //for (double t=0; t<cf.getTimeAtIndex(cf.last); t+=30) {
  //for (double t=cf.getTimeAtIndex(0); t<cf.getTimeAtIndex(cf.last); t+=1) {
  for (double t=Math.round(cf.getTimeAtIndex(0)+0.5); t<=Math.floor(cf.getTimeAtIndex(cf.last)); t+=1) {
    println(cf.getForT_is(t).toString2());
    //println((cf.getForT_is(t)).toString2());
    //println((cf.getForT_is(t)).alt);
    //println((cf.getForT_is(t)).vrel+"\t"+(cf.getForT_is(t)).pf);
    //println(0);
  }
}

void draw() {
}

class FuncPoint {
  double ti, x, y, z;//, alt, vrel;
//  boolean pf;
//  FuncPoint(double _ti, boolean _pf, double _x, double _y, double _z, double _alt, double _vrel) {
  FuncPoint(double _ti, double _x, double _y, double _z) {
    ti=_ti; 
//    pf=_pf;
    x=_x; 
    y=_y; 
    z=_z;
//    alt=_alt;
//    vrel=_vrel;
  }

  String toString1() {
//    return ("time: "+ti+" - pf: "+pf+", x: "+x+", y: "+y+", z: "+z+", alt: "+alt+", vrel: "+vrel);
    return ("time: "+ti+", x: "+x+", y: "+y+", z: "+z);
  }

  String toString2() {
//    return (ti+"\t"+pf+"\t"+x+"\t"+y+"\t"+z+"\t"+alt+"\t"+vrel);
    return (ti+"\t"+x+"\t"+y+"\t"+z);
  }
}

class customFunction {
  FuncPoint[] funcPoint;
  int first=0, last=157;

  customFunction() {
    funcPoint=new FuncPoint[last+1];
    // time, x, y, z
    funcPoint[0]=new FuncPoint(2128.2651,0.808814,0.577569,-0.000739);
    funcPoint[1]=new FuncPoint(2129.2690,0.799440,0.590180,-0.001004);
    funcPoint[2]=new FuncPoint(2130.2730,0.789811,0.602624,-0.001266);
    funcPoint[3]=new FuncPoint(2131.2769,0.779935,0.614893,-0.001527);
    funcPoint[4]=new FuncPoint(2132.2809,0.769816,0.626981,-0.001786);
    funcPoint[5]=new FuncPoint(2133.2848,0.759458,0.638881,-0.002043);
    funcPoint[6]=new FuncPoint(2134.2888,0.748866,0.650591,-0.002300);
    funcPoint[7]=new FuncPoint(2135.2928,0.738042,0.662104,-0.002555);
    funcPoint[8]=new FuncPoint(2136.2967,0.726991,0.673416,-0.002808);
    funcPoint[9]=new FuncPoint(2137.3007,0.715715,0.684525,-0.003061);
    funcPoint[10]=new FuncPoint(2138.3046,0.704218,0.695424,-0.003312);
    funcPoint[11]=new FuncPoint(2139.3086,0.692503,0.706111,-0.003562);
    funcPoint[12]=new FuncPoint(2140.3125,0.680573,0.716581,-0.003811);
    funcPoint[13]=new FuncPoint(2141.3165,0.668432,0.726831,-0.004058);
    funcPoint[14]=new FuncPoint(2142.3204,0.656084,0.736857,-0.004304);
    funcPoint[15]=new FuncPoint(2143.3244,0.643530,0.746655,-0.004549);
    funcPoint[16]=new FuncPoint(2144.3283,0.630776,0.756221,-0.004791);
    funcPoint[17]=new FuncPoint(2145.3323,0.617824,0.765551,-0.005032);
    funcPoint[18]=new FuncPoint(2146.3362,0.604678,0.774642,-0.005272);
    funcPoint[19]=new FuncPoint(2147.3402,0.591342,0.783491,-0.005510);
    funcPoint[20]=new FuncPoint(2148.3441,0.577819,0.792092,-0.005746);
    funcPoint[21]=new FuncPoint(2149.3481,0.564112,0.800444,-0.005979);
    funcPoint[22]=new FuncPoint(2150.3520,0.550226,0.808542,-0.006211);
    funcPoint[23]=new FuncPoint(2151.3560,0.536164,0.816382,-0.006441);
    funcPoint[24]=new FuncPoint(2152.3600,0.521930,0.823962,-0.006669);
    funcPoint[25]=new FuncPoint(2153.3639,0.507528,0.831277,-0.006895);
    funcPoint[26]=new FuncPoint(2154.3679,0.492962,0.838324,-0.007118);
    funcPoint[27]=new FuncPoint(2155.3718,0.478236,0.845099,-0.007339);
    funcPoint[28]=new FuncPoint(2156.3758,0.463353,0.851599,-0.007557);
    funcPoint[29]=new FuncPoint(2157.3797,0.448319,0.857822,-0.007773);
    funcPoint[30]=new FuncPoint(2158.3837,0.433137,0.863762,-0.007986);
    funcPoint[31]=new FuncPoint(2159.3876,0.417812,0.869417,-0.008197);
    funcPoint[32]=new FuncPoint(2160.3916,0.402348,0.874783,-0.008405);
    funcPoint[33]=new FuncPoint(2161.3955,0.386749,0.879857,-0.008610);
    funcPoint[34]=new FuncPoint(2162.3995,0.371021,0.884636,-0.008812);
    funcPoint[35]=new FuncPoint(2163.4034,0.355166,0.889116,-0.009011);
    funcPoint[36]=new FuncPoint(2164.4074,0.339191,0.893294,-0.009207);
    funcPoint[37]=new FuncPoint(2165.4113,0.323100,0.897168,-0.009399);
    funcPoint[38]=new FuncPoint(2166.4153,0.306897,0.900732,-0.009589);
    funcPoint[39]=new FuncPoint(2167.4193,0.290588,0.903986,-0.009775);
    funcPoint[40]=new FuncPoint(2168.4232,0.274178,0.906924,-0.009958);
    funcPoint[41]=new FuncPoint(2169.4272,0.257671,0.909545,-0.010137);
    funcPoint[42]=new FuncPoint(2170.4311,0.241074,0.911845,-0.010312);
    funcPoint[43]=new FuncPoint(2171.4351,0.224391,0.913820,-0.010484);
    funcPoint[44]=new FuncPoint(2172.4390,0.207627,0.915469,-0.010652);
    funcPoint[45]=new FuncPoint(2173.4430,0.190788,0.916788,-0.010816);
    funcPoint[46]=new FuncPoint(2174.4469,0.173880,0.917775,-0.010977);
    funcPoint[47]=new FuncPoint(2175.4509,0.156908,0.918425,-0.011133);
    funcPoint[48]=new FuncPoint(2176.4548,0.139879,0.918737,-0.011285);
    funcPoint[49]=new FuncPoint(2177.4588,0.122797,0.918708,-0.011433);
    funcPoint[50]=new FuncPoint(2178.4627,0.105669,0.918335,-0.011576);
    funcPoint[51]=new FuncPoint(2179.4667,0.088501,0.917616,-0.011715);
    funcPoint[52]=new FuncPoint(2180.4706,0.071300,0.916547,-0.011850);
    funcPoint[53]=new FuncPoint(2181.4746,0.054071,0.915127,-0.011980);
    funcPoint[54]=new FuncPoint(2182.4786,0.036821,0.913352,-0.012106);
    funcPoint[55]=new FuncPoint(2183.4825,0.019557,0.911221,-0.012226);
    funcPoint[56]=new FuncPoint(2184.4865,0.002284,0.908731,-0.012342);
    funcPoint[57]=new FuncPoint(2185.4904,-0.014989,0.905881,-0.012453);
    funcPoint[58]=new FuncPoint(2186.4944,-0.032256,0.902667,-0.012559);
    funcPoint[59]=new FuncPoint(2187.4983,-0.049510,0.899088,-0.012660);
    funcPoint[60]=new FuncPoint(2188.5023,-0.066744,0.895142,-0.012756);
    funcPoint[61]=new FuncPoint(2189.5062,-0.083950,0.890827,-0.012846);
    funcPoint[62]=new FuncPoint(2190.5102,-0.101121,0.886141,-0.012931);
    funcPoint[63]=new FuncPoint(2191.5141,-0.118250,0.881083,-0.013011);
    funcPoint[64]=new FuncPoint(2192.5181,-0.135329,0.875652,-0.013085);
    funcPoint[65]=new FuncPoint(2193.5220,-0.152350,0.869844,-0.013153);
    funcPoint[66]=new FuncPoint(2194.5260,-0.169305,0.863661,-0.013216);
    funcPoint[67]=new FuncPoint(2195.5299,-0.186186,0.857099,-0.013273);
    funcPoint[68]=new FuncPoint(2196.5339,-0.202984,0.850159,-0.013324);
    funcPoint[69]=new FuncPoint(2197.5378,-0.219692,0.842840,-0.013369);
    funcPoint[70]=new FuncPoint(2198.5418,-0.236301,0.835140,-0.013408);
    funcPoint[71]=new FuncPoint(2199.5458,-0.252802,0.827059,-0.013441);
    funcPoint[72]=new FuncPoint(2200.5497,-0.269187,0.818598,-0.013468);
    funcPoint[73]=new FuncPoint(2201.5537,-0.285446,0.809754,-0.013488);
    funcPoint[74]=new FuncPoint(2202.5576,-0.301571,0.800530,-0.013502);
    funcPoint[75]=new FuncPoint(2203.5616,-0.317552,0.790924,-0.013510);
    funcPoint[76]=new FuncPoint(2204.5655,-0.333380,0.780938,-0.013511);
    funcPoint[77]=new FuncPoint(2205.5695,-0.349046,0.770571,-0.013505);
    funcPoint[78]=new FuncPoint(2206.5734,-0.364540,0.759824,-0.013493);
    funcPoint[79]=new FuncPoint(2207.5774,-0.379852,0.748699,-0.013474);
    funcPoint[80]=new FuncPoint(2208.5813,-0.394973,0.737197,-0.013449);
    funcPoint[81]=new FuncPoint(2209.5853,-0.409892,0.725320,-0.013416);
    funcPoint[82]=new FuncPoint(2210.5892,-0.424600,0.713068,-0.013377);
    funcPoint[83]=new FuncPoint(2211.5932,-0.439087,0.700445,-0.013330);
    funcPoint[84]=new FuncPoint(2212.5971,-0.453342,0.687451,-0.013276);
    funcPoint[85]=new FuncPoint(2213.6011,-0.467355,0.674091,-0.013216);
    funcPoint[86]=new FuncPoint(2214.6051,-0.481116,0.660367,-0.013148);
    funcPoint[87]=new FuncPoint(2215.6090,-0.494613,0.646281,-0.013073);
    funcPoint[88]=new FuncPoint(2216.6130,-0.507837,0.631839,-0.012991);
    funcPoint[89]=new FuncPoint(2217.6169,-0.520777,0.617042,-0.012901);
    funcPoint[90]=new FuncPoint(2218.6209,-0.533421,0.601896,-0.012804);
    funcPoint[91]=new FuncPoint(2219.6248,-0.545760,0.586406,-0.012700);
    funcPoint[92]=new FuncPoint(2220.6288,-0.557783,0.570575,-0.012588);
    funcPoint[93]=new FuncPoint(2221.6327,-0.569478,0.554409,-0.012469);
    funcPoint[94]=new FuncPoint(2222.6367,-0.580835,0.537914,-0.012342);
    funcPoint[95]=new FuncPoint(2223.6406,-0.591843,0.521096,-0.012208);
    funcPoint[96]=new FuncPoint(2224.6446,-0.602491,0.503960,-0.012067);
    funcPoint[97]=new FuncPoint(2225.6485,-0.612768,0.486515,-0.011918);
    funcPoint[98]=new FuncPoint(2226.6525,-0.622664,0.468767,-0.011762);
    funcPoint[99]=new FuncPoint(2227.6564,-0.632168,0.450724,-0.011598);
    funcPoint[100]=new FuncPoint(2228.6604,-0.641269,0.432394,-0.011427);
    funcPoint[101]=new FuncPoint(2229.6644,-0.649957,0.413785,-0.011248);
    funcPoint[102]=new FuncPoint(2230.6683,-0.658221,0.394906,-0.011062);
    funcPoint[103]=new FuncPoint(2231.6723,-0.666051,0.375767,-0.010869);
    funcPoint[104]=new FuncPoint(2232.6762,-0.673437,0.356377,-0.010668);
    funcPoint[105]=new FuncPoint(2233.6802,-0.680370,0.336747,-0.010461);
    funcPoint[106]=new FuncPoint(2234.6841,-0.686838,0.316888,-0.010246);
    funcPoint[107]=new FuncPoint(2235.6881,-0.692834,0.296810,-0.010024);
    funcPoint[108]=new FuncPoint(2236.6920,-0.698347,0.276526,-0.009795);
    funcPoint[109]=new FuncPoint(2237.6960,-0.703368,0.256047,-0.009559);
    funcPoint[110]=new FuncPoint(2238.6999,-0.707890,0.235386,-0.009316);
    funcPoint[111]=new FuncPoint(2239.7039,-0.711903,0.214556,-0.009066);
    funcPoint[112]=new FuncPoint(2240.7078,-0.715400,0.193570,-0.008810);
    funcPoint[113]=new FuncPoint(2241.7118,-0.718373,0.172442,-0.008548);
    funcPoint[114]=new FuncPoint(2242.7157,-0.720814,0.151187,-0.008279);
    funcPoint[115]=new FuncPoint(2243.7197,-0.722718,0.129819,-0.008004);
    funcPoint[116]=new FuncPoint(2244.7236,-0.724077,0.108353,-0.007722);
    funcPoint[117]=new FuncPoint(2245.7276,-0.724886,0.086805,-0.007435);
    funcPoint[118]=new FuncPoint(2246.7316,-0.725140,0.065190,-0.007142);
    funcPoint[119]=new FuncPoint(2247.7355,-0.724833,0.043525,-0.006844);
    funcPoint[120]=new FuncPoint(2248.7395,-0.723962,0.021826,-0.006540);
    funcPoint[121]=new FuncPoint(2249.7434,-0.722522,0.000110,-0.006231);
    funcPoint[122]=new FuncPoint(2250.7474,-0.720512,-0.021607,-0.005917);
    funcPoint[123]=new FuncPoint(2251.7513,-0.717928,-0.043306,-0.005598);
    funcPoint[124]=new FuncPoint(2252.7553,-0.714768,-0.064971,-0.005275);
    funcPoint[125]=new FuncPoint(2253.7592,-0.711032,-0.086583,-0.004948);
    funcPoint[126]=new FuncPoint(2254.7632,-0.706719,-0.108125,-0.004616);
    funcPoint[127]=new FuncPoint(2255.7671,-0.701829,-0.129579,-0.004281);
    funcPoint[128]=new FuncPoint(2256.7711,-0.696363,-0.150927,-0.003941);
    funcPoint[129]=new FuncPoint(2257.7750,-0.690324,-0.172150,-0.003599);
    funcPoint[130]=new FuncPoint(2258.7790,-0.683712,-0.193231,-0.003254);
    funcPoint[131]=new FuncPoint(2259.7829,-0.676532,-0.214152,-0.002906);
    funcPoint[132]=new FuncPoint(2260.7869,-0.668788,-0.234893,-0.002555);
    funcPoint[133]=new FuncPoint(2261.7909,-0.660484,-0.255439,-0.002202);
    funcPoint[134]=new FuncPoint(2262.7948,-0.651625,-0.275769,-0.001847);
    funcPoint[135]=new FuncPoint(2263.7988,-0.642217,-0.295868,-0.001491);
    funcPoint[136]=new FuncPoint(2264.8027,-0.632268,-0.315718,-0.001133);
    funcPoint[137]=new FuncPoint(2265.8067,-0.621784,-0.335300,-0.000774);
    funcPoint[138]=new FuncPoint(2266.8106,-0.610774,-0.354600,-0.000415);
    funcPoint[139]=new FuncPoint(2267.8146,-0.599248,-0.373599,-0.000055);
    funcPoint[140]=new FuncPoint(2268.8185,-0.587213,-0.392282,0.000306);
    funcPoint[141]=new FuncPoint(2269.8225,-0.574682,-0.410633,0.000666);
    funcPoint[142]=new FuncPoint(2270.8264,-0.561664,-0.428637,0.001026);
    funcPoint[143]=new FuncPoint(2271.8304,-0.548171,-0.446279,0.001385);
    funcPoint[144]=new FuncPoint(2272.8343,-0.534215,-0.463544,0.001743);
    funcPoint[145]=new FuncPoint(2273.8383,-0.519809,-0.480419,0.002100);
    funcPoint[146]=new FuncPoint(2274.8422,-0.504965,-0.496891,0.002455);
    funcPoint[147]=new FuncPoint(2275.8462,-0.489698,-0.512946,0.002809);
    funcPoint[148]=new FuncPoint(2276.8502,-0.474021,-0.528573,0.003160);
    funcPoint[149]=new FuncPoint(2277.8541,-0.457949,-0.543760,0.003510);
    funcPoint[150]=new FuncPoint(2278.8581,-0.441497,-0.558496,0.003857);
    funcPoint[151]=new FuncPoint(2279.8620,-0.424679,-0.572772,0.004202);
    funcPoint[152]=new FuncPoint(2280.8660,-0.407512,-0.586578,0.004544);
    funcPoint[153]=new FuncPoint(2281.8699,-0.390011,-0.599906,0.004883);
    funcPoint[154]=new FuncPoint(2282.8739,-0.372192,-0.612747,0.005220);
    funcPoint[155]=new FuncPoint(2283.8778,-0.354070,-0.625096,0.005555);
    funcPoint[156]=new FuncPoint(2284.8818,-0.335662,-0.636946,0.005888);
    funcPoint[157]=new FuncPoint(2285.8857,-0.316984,-0.648297,0.006223);


  }

  double getTimeAtIndex(int i) {
    if (i<first||i>last) {
      return -999.999;
    } 
    else {
      return funcPoint[i].ti;
    }
  }

  FuncPoint getForT_is(double t) {
    // check for false t:
    if (t<funcPoint[first].ti||t>funcPoint[last].ti) {
      //return new FuncPoint(t,false,0,0,0,0,0);
      return new FuncPoint(t,0,0,0);
    } 
    else {
      //--- find point in array with ti<t and (t-ti) smallest value:
      int i=0; 
      boolean found=false;
      do { 
        found=(funcPoint[i+1].ti-t>0); 
        i++; 
      } 
      while (!found&&i<last-1);
      int i1=i-1, i2=i;

      //--- interpolate between this and next position
      double t1=funcPoint[i1].ti, t2=funcPoint[i2].ti, tr=t2-t1;
      double perc=(t-t1)/tr; 

//      boolean pf_out=funcPoint[i1].pf;
      double x1=funcPoint[i1].x, x2=funcPoint[i2].x, xr=x2-x1, x_out=x1+perc*xr;
      double y1=funcPoint[i1].y, y2=funcPoint[i2].y, yr=y2-y1, y_out=y1+perc*yr;
      double z1=funcPoint[i1].z, z2=funcPoint[i2].z, zr=z2-z1, z_out=z1+perc*zr;
//      double a1=funcPoint[i1].alt, a2=funcPoint[i2].alt, ar=a2-a1, a_out=a1+perc*ar;
//      double v1=funcPoint[i1].vrel, v2=funcPoint[i2].vrel, vrelr=v2-v1, vrel_out=v1+perc*vrelr;

//      return new FuncPoint(t,pf_out,x_out,y_out,z_out,a_out,vrel_out);
      return new FuncPoint(t,x_out,y_out,z_out);
    }
  }
}
