

import graf.sequence.FloatValue;
import graf.sequence.Sequence;
import graf.sequence.SequenceEvent;
import graf.sequence.TrackItem;
import graf.sequence.SequenceListener;
import graf.sequence.Track;
import processing.core.PApplet;

/**
 * Sequencer package is compiled level 1.4.2
 * as a library for Processing
 * @author mgraf
 *
 */

public class SequencerTest extends PApplet implements SequenceListener {

	private static final long serialVersionUID = 4989501973064477472L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"SequencerTest"});
	}
	
	Track t1 = new Track("One");
	Track t2 = new Track("Two");
	Sequence seq;
	
	public void setup() {
		size(800,300,P3D);
		
		TrackItem s1 = new TrackItem(40,88,new FloatValue(10,80));
		t1.addEvent(s1);
		println(s1);
		
		println(t1);
		println(t2);
		
		seq = new Sequence(25);
		seq.addTrack(t1);
		seq.addTrack(t2);
		seq.addSequenceListener(this);
		
		//frameRate(5);
		
	}
	
	public void draw() {
		
		
		background(250);
		println(seq.getTimeInMMSSff());
		seq.fireEvents();
		
		stroke(0);
		Track t = seq.getTrack("One");
		TrackItem[] events = t.getEvents();
		for (int i=0;i<events.length;i++) {
			float y = 10+i*5;
			line(events[i].getStartTime(),y,events[i].getStopTime(),y);
		}
		
		stroke(255,0,0,128);
		line(seq.getTime(),0,seq.getTime(),height);
		
		seq.stepTime();
	}

	public void eventRunning(SequenceEvent e) {
		println(e);
	}

	public void eventStarted(SequenceEvent e) {
		println(e);
	}

	public void eventStopped(SequenceEvent e) {
		println(e);
	}
	
}
