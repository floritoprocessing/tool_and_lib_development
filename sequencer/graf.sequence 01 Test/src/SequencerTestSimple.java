import java.net.URISyntaxException;

import graf.sequence.Sequence;
import graf.sequence.SequenceEvent;
import graf.sequence.Track;
import graf.sequence.TrackItem;
import processing.core.PApplet;
import processing.core.PFont;

/**
 * Sequencer package is compiled level 1.4.2
 * as a library for Processing
 * @author mgraf
 *
 */

public class SequencerTestSimple extends PApplet {

	private static final long serialVersionUID = 4989501973064477472L;

	public static void main(String[] args) {
		PApplet.main(new String[] { "SequencerTestSimple" });
	}

	PFont font;
	Sequence seq;
	float[] opac = new float[] {0,0,0};
	float opacMax = 700;
	String[] msg = new String[] {"","",""};

	public void settings() {
		size(640, 480, P3D);
	}
	
	public void setup() {
		font = null;
//		try {
			font = loadFont("Arial-BoldMT-16.vlw");//this.getClass().getResource("./Arial-BoldMT-16.vlw").toURI().toString());
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
		// create a new Sequence with synchronisation on:
		seq = new Sequence(this, 25, true);
		// add a track
		Track track = seq.createTrack("First track");
		// add TrackItems
		track.addItem(new TrackItem("SingleEvent", 10));
		track.addItem(new TrackItem("LongerEvent", 15, 50));
		track.addItem(new TrackItem("AnotherEvent", 25, 150));
		track.addItem(new TrackItem("Event", 17, 350));
	}

	public void draw() {
		background(255);
		seq.drawTrackItems(color(255,0,0), color(0,0,0,32),0,500);
		seq.drawTracks(color(0,0,0), color(0,0,0,16), font);
		seq.drawTime(color(255,0,0,128),0,500);
		fill(0);
		textFont(font);
		text("frame: "+seq.getTime(),20,height-60);
		//println(seq.getTime());
		for (int i=0;i<msg.length;i++) {
			if (opac[i]==opacMax) {
				fill(255,0,0);
			} else {
				fill(0,0,0,min(255,max(0,opac[i])));
			}
			text(msg[i],20,height-40+i*15);
			opac[i]-=10;
		}
		msg[2] = "";
	}
	
	public void sequenceEventStart(SequenceEvent arg0) {
		msg[0] = "Start: "+arg0.getName();
		opac[0] = opacMax;
	}

	public void sequenceEventStop(SequenceEvent arg0) {
		msg[1] = "Stop: "+arg0.getName();
		opac[1] = opacMax;
	}
	
	public void sequenceEventRunning(SequenceEvent arg0) {
		if (msg[2].length()==0) {
			msg[2] = "Running: ";
		}
		msg[2] += arg0.getName()+", ";
		opac[2] = opacMax;
	}
}
