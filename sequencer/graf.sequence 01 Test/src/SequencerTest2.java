
import java.net.URISyntaxException;

import graf.sequence.FloatValue;
import graf.sequence.Sequence;
import graf.sequence.SequenceEvent;
import graf.sequence.Track;
import graf.sequence.TrackItem;
import processing.core.PApplet;
import processing.core.PFont;

/**
 * Sequencer package is compiled level 1.4.2
 * as a library for Processing
 * @author mgraf
 *
 */

public class SequencerTest2 extends PApplet {

	private static final long serialVersionUID = 4989501973064477472L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"SequencerTest2"});
	}
	
	PFont font;
	Sequence seq;
	float x=0, y=0;
	
	public void settings() {
		size(800,400,JAVA2D);	
	}
	
	public void setup() {
		
		//smooth();
		seq = new Sequence(this,25,true);
		
		/*
		 * Create random sequence
		 */
		
		int tracks = 4;
		int itemMin = 5, itemMax = 10;
		int timeMin = 0, timeMax = width, maxEventTime=width/4;
		
		
		for (int i=0;i<tracks;i++) {
			Track track = seq.createTrack("T nr "+i);
			//track[i] = new Track("T nr "+i);
			
			int tiCount = (int)random(itemMin,itemMax);
			for (int ti=0;ti<tiCount;ti++) {
				
				boolean single = random(1.0f)<0.2;
				FloatValue fv = new FloatValue(random(-100,100),random(-100,100));
				
				TrackItem trackItem = null;
				int startTime = (int)random(timeMin,timeMax);
				if (single) {
					trackItem = new TrackItem(startTime, fv);
				} else {
					int stopTime = 0;//(int)random(startTime+1,timeMax);
					while (stopTime<=startTime||stopTime>=timeMax) {
						stopTime = startTime + (int)random(maxEventTime);
					}
					trackItem = new TrackItem(startTime, stopTime, fv);
				}
				track.addItem(trackItem);
			}
			//seq.addTrack(track[i]);
		}
		
		font = null;
//		try {
			font = loadFont("Arial-BoldMT-16.vlw");//this.getClass().getResource("./Arial-BoldMT-16.vlw").toURI().toString());
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
		
		//seq.addTrack(t1);
		println(seq);
		//System.exit(0);
	}
	
	public void draw() {
		background(250);
		
		seq.drawTracks(color(0,0,128),color(0,0,0,32),font);
		seq.drawTrackItems(color(0,0,0),color(0,0,0,32));
		
		seq.drawTime(color(255,0,0,128));
		
		//println(seq.getTime());
	}
	
	public void sequenceEventStart(SequenceEvent e) {
		println(e);
	}
	
	public void sequenceEventStop(SequenceEvent e) {
		println(e);
	}
	
	public void sequenceEventRunning(SequenceEvent e) {
		//println(e);
	}
	
}
