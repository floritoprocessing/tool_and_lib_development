package graf.sequence;

public interface SequenceListener {

	/**
	 * Invoked when the TrackItem has started
	 * @param e
	 */
	public void eventStarted(SequenceEvent e);
	
	/**
	 * Invoked when a TrackItem is running
	 * @param e
	 */
	public void eventRunning(SequenceEvent e);
	
	/**
	 * Invoked when the TrackItem has stopped
	 * @param e
	 */
	public void eventStopped(SequenceEvent e);
	
}
