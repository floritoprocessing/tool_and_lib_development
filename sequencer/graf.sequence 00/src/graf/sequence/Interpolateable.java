package graf.sequence;

interface Interpolateable {

	/**
	 * Returns a new interpolated <code>Value</code> based on the percentage.
	 * By definition, percentage has to be clamped between 0 and 1.
	 * @param percentage
	 * @return
	 */
	public Value interpolate(double percentage);
	
}
