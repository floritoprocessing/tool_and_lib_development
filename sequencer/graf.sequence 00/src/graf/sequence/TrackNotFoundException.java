package graf.sequence;

class TrackNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8235544091170000513L;

	TrackNotFoundException() {
		super("A track was not found!");
	}
	
	TrackNotFoundException(String name) {
		super("A track with the name \""+name+"\" was not found!");
	}
	

}
