package graf.sequence;

public class TrackItem implements Comparable {
	
	final long startTime;
	final long stopTime;
	final Value value;
	
	/**
	 * Creates a new <code>TrackItem</code> with startTime and stopTime = startTime+1;
	 * @param startTime
	 */
	public TrackItem(long startTime) {
		this(startTime,startTime+1, null);
	}
	
	/**
	 * Creates a new code>TrackItem</code> with startTime and stopTime = startTime+1 and the supplied <code>Value</code>;
	 * @param startTime
	 */
	public TrackItem(long startTime, Value value) {
		this(startTime,startTime+1, value);
	}
	
	/**
	 * Creates a new code>TrackItem</code> with a startTime and a stopTime.
	 * @param startTime
	 * @param stopTime
	 */
	public TrackItem(long startTime, long stopTime) {
		this(startTime,stopTime,null);
	}
	
	/**
	 * Creates a new code>TrackItem</code> with the supplied startTime, stopTime and <code>Value</code>.
	 * @param startTime
	 * @param stopTime
	 * @param object
	 */
	public TrackItem(long startTime, long stopTime, Value value) {
		if (stopTime<=startTime) {
			throw new RuntimeException("Illegal arguments: stopTime has to be larger that startTime. " +
					"(startTime="+startTime+", stopTime="+stopTime+")");
		}
		this.startTime = startTime;
		this.stopTime = stopTime;
		if (value==null) {
			this.value = null;
		} else {
			this.value = value.copy();
		}
	}
	
	public int compareTo(Object arg0) {
		TrackItem other = (TrackItem)arg0;
		if (startTime<other.startTime) {
			return -1;
		} else if (startTime>other.startTime) {
			return 1;
		} else {
			// equal startTimes:
			if (stopTime<other.stopTime) {
				return -1;
			} else if (stopTime>other.stopTime) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	
	/**
	 * Returns a copy of this <code>TrackItem</code>
	 * @return
	 */
	public TrackItem copy() {
		if (value==null) {
			return new TrackItem(startTime, stopTime, null);
		} else {
			return new TrackItem(startTime, stopTime, value.copy());
		}
	}
	
	public double getEventPercentageAt(long time) {
		return (time-startTime)/((double)stopTime-startTime);
	}
	
	public long getEventTimeAt(long time) {
		return time-startTime;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public long getStopTime() {
		return stopTime;
	}
	
	public Value getValue() {
		return value;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(getClass().getName());
		result.append("[");
		result.append("startTime="+startTime);
		result.append(",stopTime="+stopTime);
		if (value!=null) {
			result.append(",value="+value);
		}
		result.append("]");
		return result.toString();
	}

	

}
