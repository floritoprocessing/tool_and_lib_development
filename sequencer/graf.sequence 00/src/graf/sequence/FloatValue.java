package graf.sequence;

public class FloatValue extends Value {

	private final float v0;
	private final float v1;
	
	public FloatValue(float v0) {
		this.v0 = v0;
		this.v1 = v0;
	}
	
	public FloatValue(float v0, float v1) {
		this.v0 = v0;
		this.v1 = v1;
	}
	
	public Value copy() {
		return new FloatValue(v0,v1);
	}
	
	
	public Value interpolate(double percentage) {
		return new FloatValue(interpolate(v0, v1, percentage));
	}
	
	public String toString() {
		if (v0==v1) {
			return ""+v0;
		} else {
			return ""+v0+".."+v1;
		}
		
	}

}

