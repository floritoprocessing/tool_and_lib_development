package graf.sequence;

/**
 * An event which indicates that a <code>SequenceEvent</code> occured in a <code>Sequence</code>.
 * This event is used for START, RUNNING and STOP events.
 * <p>
 * A <code>SequenceEvent</code> object is passed to every <code>SequenceListener</code> object which is registered to
 * receive these events using the <code>Sequence</code>'s <code>addSequenceListener</code> method.
 * <p>
 * When the Sequence's <code>fireEvents</code> method is called, 
 * events are generated and sent to the registered <code>SequenceListener</code>s.
 * Details on these events can be retrieved using 
 * {@link #getType()}, {@link #getTrack()}, {@link #getValue()} and {@link #getTime()}
 * 
 * @author mgraf
 * @see Sequence
 * @see Sequence#fireEvents()
 * @see Sequence#addSequenceListener(SequenceListener)
 * @see SequenceListener
 *
 */
public class SequenceEvent {

	public static final String START = "start";
	public static final String RUNNING = "running";
	public static final String STOP = "stop";
	
	private final String type;
	private final Track track;
	private final TrackItem trackItem;
	private final double percentage;
	private final Value value;
	private final long time;
	
	SequenceEvent(String type, Track track, TrackItem trackItem, long time) {
		this.type = type;
		this.track = track;
		this.trackItem = trackItem;
		percentage = trackItem.getEventPercentageAt(time);
		value = trackItem.value.interpolate(percentage);
		this.time = time;
	}
	
	/**
	 * Returns the percentage passed in time of the source trackItem
	 * @return
	 */
	public double getPercentage() {
		return percentage;
	}
	
	/**
	 * Returns the time of this event
	 * @return
	 */
	public long getTime() {
		return time;
	}
	
	/**
	 * Returns the source <code>Track</code> holding the <code>TrackItem</code> at the root of this event
	 * @return
	 */
	public Track getTrack() {
		return track;
	}
	
	/**
	 * Returns the source <code>TrackItem</code> at the root of this event
	 * @return
	 */
	public TrackItem getTrackItem() {
		return trackItem;
	}
	
	/**
	 * Returns the type of this event
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Returns the <b>interpolated</b> value of this event
	 * @return
	 */
	public Value getValue() {
		return value;
	}
	
	/**
	 * Returns a string representation of this event
	 */
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(getClass().getName());
		result.append("[");
		result.append("type="+type);
		result.append(",track="+track.getName());
		result.append(",trackItem="+trackItem);
		result.append(",value="+value);
		result.append(",time="+time);
		result.append("]");
		return result.toString();
	}

}
