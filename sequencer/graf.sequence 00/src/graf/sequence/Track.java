package graf.sequence;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The <code>Track</code> class implements a container for storing <code>TrackItem</code> objects.
 * @see TrackItem 
 * @author mgraf
 *
 */
public class Track {

	/**
	 * The name of the track
	 */
	private final String name;
	
	/**
	 * All events.
	 * This field is "package-private", so that it can be accessed by Sequence
	 */
	final ArrayList events = new ArrayList(); //<TrackItem>
	
	/**
	 * Constructs a new Track with the specified name
	 * @param name the name of the Track
	 */
	public Track(String name) {
		this.name = name;
	}
	
	private Track(String name, ArrayList newEvents) {
		this.name = name;
		events.add(newEvents);
	}

	/**
	 * Adds a <code>TrackItem</code> to this <code>Track</code> by creating a copy of the supplied object.
	 * @param event the TrackItem to be added
	 */
	public void addEvent(TrackItem event) {
		events.add(event.copy());
		Collections.sort(events);
	}
	
	/**
	 * Returns a field-by-field copy of this <code>Track</code> object.
	 * @return
	 */
	public Track copy() {
		ArrayList newEvents = new ArrayList(events.size());
		for (int i=0;i<events.size();i++) {
			newEvents.add( ((TrackItem)events.get(i)).copy() );
		}
		return new Track(name,newEvents);
	}
	
	/**
	 * Returns all SequenceEvents stored in this Track
	 * @return array of TrackItem
	 */
	public TrackItem[] getEvents() {
		return (TrackItem[])events.toArray(new TrackItem[0]);
	}

	/**
	 * Returns the name of this Track
	 * @return the track name
	 */
	public final String getName() {
		return name;
	}
	
	/**
	 * Removes and returns all SequenceEvents with the specified startTime
	 * @param startTime the startTime of the SequenceEvents to be removed
	 * @return array of removed SequenceEvents
	 */
	public TrackItem[] removeEvents(long startTime) {
		ArrayList remove = new ArrayList(); // <TrackItem>
		for (int i=0;i<events.size();i++) {
			if ( ((TrackItem)events.get(i)).startTime == startTime ) {
				remove.add(events.remove(i--));
			}
		}
		return (TrackItem[])remove.toArray(new TrackItem[0]);
	}
	
	/**
	 * Removes and returns all SequenceEvents having the specified startTime and stopTime.
	 * Removes only those SequenceEvents that have the exact supplied times
	 * @param startTime the startTime
	 * @param stopTime the stopTime
	 * @return array of removed SequenceEvents
	 */
	public TrackItem[] removeEvents(long startTime, long stopTime) {
		ArrayList remove = new ArrayList(); // <TrackItem>
		for (int i=0;i<events.size();i++) {
			TrackItem se = (TrackItem)events.get(i);
			if ( se.startTime == startTime
					&& se.stopTime == stopTime ) {
				remove.add(events.remove(i--));
			}
		}
		return (TrackItem[])remove.toArray(new TrackItem[0]);
	}
	
	/**
	 * Returns a String representation of this track
	 */
	public String toString() {
		StringBuilder result = new StringBuilder();
	    String NEW_LINE = System.getProperty("line.separator");
		result.append("Track: "+name+NEW_LINE);
		for (int i=0;i<events.size();i++) {
			result.append("["+i+"] "+events.get(i)+NEW_LINE);
		}
		return result.toString();
	}
	
}
