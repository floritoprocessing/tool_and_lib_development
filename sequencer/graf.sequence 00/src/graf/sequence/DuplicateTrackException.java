package graf.sequence;

class DuplicateTrackException extends RuntimeException {

	private static final long serialVersionUID = -9103863296565710287L;

	DuplicateTrackException() {
		super("A track was not added!");
	}
	
	DuplicateTrackException(String name) {
		super("\r\nA track with the name \""+name+"\" already exists! The track will not be added!");
	}

	DuplicateTrackException(Track track) {
		super("\r\nA track with the name \""+track.getName()+"\" already exists. This track will not be added: \r\n"+track);
	}
	
}
