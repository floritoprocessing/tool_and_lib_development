package graf.sequence;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Goal of this class is to create a sequencer.
 * <p>
 * Fields:
 * <ul>
 * <li> time (in frames) 
 * <li> frameBase (fps)
 * </ul>
 * 
 * A Sequence always has at least one Track
 * 
 * 
 * @author mgraf
 *
 */
public class Sequence {

	/**
	 * NumberFormat used for formatting to 2 digits
	 */
	private static final NumberFormat nf2;
	static {
		nf2 = NumberFormat.getInstance();
		nf2.setGroupingUsed(false);
		nf2.setParseIntegerOnly(true);
		nf2.setMinimumIntegerDigits(2);
	}
	
	/**
	 * Time in frames
	 */
	private long time = 0;
	
	/**
	 * Frame base
	 */
	private final int frameBase;
	
	/**
	 * The tracks
	 */
	private final Hashtable tracks = new Hashtable(); // <String,Track>
	
	/**
	 * The listeners
	 */
	private final ArrayList listeners = new ArrayList(); //SequenceListener
	
	public Sequence(int frameBase) {
		this.frameBase = frameBase;
	}
	
	/**
	 * Adds a SequenceListener to this Sequence
	 * @param listener
	 */
	public void addSequenceListener(SequenceListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Adds a new Track to the Sequence
	 * @param name the name of the Track
	 */
	public void addTrack(String name) {
		if (tracks.containsKey(name)) {
			throw new DuplicateTrackException(name);
		}
		tracks.put(name, new Track(name));
	}
	
	/**
	 * Adds the track to the Sequence
	 * @param track the Track
	 */
	public void addTrack(Track track) {
		if (tracks.contains(track)) {
			throw new DuplicateTrackException(track);
		}
		tracks.put(track.getName(), track);
	}
	
	/**
	 * Returns the frameBase of this Sequence
	 * @return
	 */
	public int getFrameBase() {
		return frameBase;
	}
	
	/**
	 * Returns the time in frames
	 * @return
	 */
	public long getTime() {
		return time;
	}
	
	/**
	 * Returns the time in the supplied pattern.
	 * <p>
	 * First occurences of 'H', 'M', 'S' and 'f' are replaced by hours, minnutes, seconds and frames.
	 * <p>
	 * Lets say the time is 120224 frames at a frameBase of 25:<br>
	 * "HH:MM:SS:ff" would result in 01:20:08:24<br>
	 * "SSSSS:ff" would result in 04808:24<br>
	 * and "MMM:SS ff" would result in 080:08 24
	 * @param pattern
	 * @return
	 */
	/*public String getTimeIn(String pattern) {
		
		boolean hasFrames = pattern.indexOf("f")>=0;
		boolean hasSeconds = pattern.indexOf("S")>=0;
		boolean hasMinutes = pattern.indexOf("M")>=0;
		boolean hasHours = pattern.indexOf("H")>=0;
		
		if (hasFrames) {
			if (!hasSeconds) {
				if (hasMinutes||hasHours) {
					throw new RuntimeException("Missing seconds in pattern \""+pattern+"\"");
				}
			}
		}
		
		if (hasSeconds) {
			if (!hasMinutes&&hasHours) {
				throw new RuntimeException("Missing minutes in pattern \""+pattern+"\"");
			}
		}
		
		if (hasSeconds) {
			pattern = replaceAndFormat(pattern, "f", time%frameBase);
		} else {
			pattern = replaceAndFormat(pattern, "f", time);
		}
		
		if (hasMinutes) {
			pattern = replaceAndFormat(pattern, "S", (time/frameBase)%60);
		} else {
			pattern = replaceAndFormat(pattern, "S", (time/frameBase));
		}
		
		if (hasHours) {
			pattern = replaceAndFormat(pattern, "M", (time/frameBase/60)%60);
		} else {
			pattern = replaceAndFormat(pattern, "M", (time/frameBase/60));
		}
		
		pattern = replaceAndFormat(pattern, "H", time/frameBase/3600);
		
		return pattern;
	}*/
	
	/**
	 * Returns the time as a string in the format HH:MM:SS:ff
	 * @return
	 */
	public String getTimeInHHMMSSff() {
		long frames = time%frameBase;
		long seconds = (time/frameBase)%60;
		long minutes = (time/frameBase/60)%60; 
		long hours = (time/frameBase/3600);
		return nf2.format(hours)+":"+nf2.format(minutes)+":"+nf2.format(seconds)+":"+nf2.format(frames);
	}
	
	/**
	 * Returns the time as a string in the format MM:SS:ff
	 * @return
	 */
	public String getTimeInMMSSff() {
		long frames = time%frameBase;
		long seconds = (time/frameBase)%60;
		long minutes = (time/frameBase/60); 
		return nf2.format(minutes)+":"+nf2.format(seconds)+":"+nf2.format(frames);
	}
	
	/**
	 * Returns the time in seconds
	 * @return
	 */
	public float getTimeInSeconds() {
		return (float)time/frameBase;
	}
	
	/**
	 * Returns the time as a string in the format SS:ff
	 * @return
	 */
	public String getTimeInSSff() {
		long frames = time%frameBase;
		long seconds = (time/frameBase);
		return nf2.format(seconds)+":"+nf2.format(frames);
	}
	
	/**
	 * Returns the Track with the supplied name
	 * @param name
	 * @return
	 */
	public Track getTrack(String name) {
		Track out = (Track)tracks.get(name); 
		if (out==null) {
			throw new TrackNotFoundException(name);
		} else {
			return out;
		}
	}
	
	/**
	 * Returns all Track names as an array of Strings
	 * @return
	 */
	public String[] getTrackNames() {
		Enumeration e = tracks.keys();
		ArrayList out = new ArrayList();
		while (e.hasMoreElements()) {
			out.add(e.nextElement());
		}
		return (String[])out.toArray(new String[0]);
	}
	
	/**
	 * Removes a SequenceListener from this Sequence
	 * @param listener
	 */
	public void removeSequenceListener(SequenceListener listener) {
		listeners.remove(listener);
	}
	
	/**
	 * Removes and returns the Track with the supplied name
	 * @param name
	 * @return
	 */
	public Track removeTrack(String name) {
		//Track out = (Track)tracks.get(name);
		Track out = (Track)tracks.remove(name);
		if (out==null) {
			throw new TrackNotFoundException(name);
		} else {
			return out;
		}
	}

	/**
	 * Replaces the first following occurences of the searchCharacter with a 
	 * String representation of the value. Used by {@link #getTimeIn(String)}
	 * @param pattern
	 * @param searchCharacter
	 * @param value
	 * @see #getTimeIn(String)
	 * @return
	 */
	/*private String replaceAndFormat(String pattern, String searchCharacter, long value) {
		if (searchCharacter.length()!=1) throw new RuntimeException("searchCharachter has to be of length 1");
		int i0 = pattern.indexOf(searchCharacter);
		int i1 = i0+1;
		if (i0>=0) {
			while (i1<pattern.length() && pattern.substring(i1,i1+1).equals(searchCharacter)) {
				i1++;
			}
			String subString = pattern.substring(i0,i1);
			NumberFormat nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setParseIntegerOnly(true);
			nf.setMinimumIntegerDigits(subString.length());
			return pattern.replaceFirst(subString,nf.format(value));
		} else {
			return pattern;
		}
	}*/

	/**
	 * Sets the time in frames
	 * @param time
	 */
	public void setTime(long time) {
		this.time = time;
	}
	
	/**
	 * Increases the time by one frame
	 */
	public void stepTime() {
		time++;
	}
		
	/**
	 * Core event firing routing
	 * Call this every time after (or before) you change the time to fire new <code>SequencerEvent</code>
	 * @see #stepTime()
	 * @see #stepTime(int)
	 * @see #setTime(long)
	 */
	public void fireEvents() {
		//TODO: fire all events!
		Enumeration en = tracks.elements();
		while (en.hasMoreElements()) {
			Track track = (Track)en.nextElement();
			for (int i=0;i<track.events.size();i++) {
				TrackItem trackItem = (TrackItem)track.events.get(i);
				
				//TrackItem event = storedEvent.copyAtTime(storedEvent);
				
				/*
				 * Fire eventStarted
				 */
				if (trackItem.startTime==time) {
					for (int l=0;l<listeners.size();l++) {
						((SequenceListener)listeners.get(l)).eventStarted(
								new SequenceEvent(SequenceEvent.START,track,trackItem,time)
								);
					}
				}
				
				/*
				 * Fire eventRunning
				 */
				if (time>=trackItem.startTime && time<=trackItem.stopTime) {
					for (int l=0;l<listeners.size();l++) {
						((SequenceListener)listeners.get(l)).eventRunning(
								new SequenceEvent(SequenceEvent.RUNNING,track,trackItem,time)
								);
					}
				}
				
				/*
				 * Fire eventStopped
				 */
				if (trackItem.stopTime==time) {
					for (int l=0;l<listeners.size();l++) {
						((SequenceListener)listeners.get(l)).eventStopped(
								new SequenceEvent(SequenceEvent.STOP,track,trackItem,time)		
								);
					}
				}
				
				
			}
		}
		
		
	}

	/**
	 * Increases the time by the specified frames
	 * @param frames
	 */
	public void stepTime(int frames) {
		time += frames;
	}
}
