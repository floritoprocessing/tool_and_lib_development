package graf.sequence;

/**
 * <dl>
 * <dt><b>Direct Known Subclasses:</b></dt>
 * <dd>{@link BooleanValue}, {@link FloatValue}, {@link IntegerValue}, {@link StringValue}</dd>
 * </dl>
 * @author mgraf
 *
 */
public abstract class Value implements Interpolateable {

	public abstract float asFloat();
	
	/**
	 * Returns a copy of this <code>Value</code> object
	 * @return
	 */
	public abstract Value copy();
	
	public abstract String toString();
	
	/**
	 * Interpolates between <code>v0</code> and <code>v1</code>. Percentages outside 0..1 are clamped.
	 * @param v0 value 0
	 * @param v1 value 1
	 * @param percentage the percentage (0..1)
	 * @return interpolated value
	 */
	protected double interpolate(double v0, double v1, double percentage) {
		return v0 + (v1-v0)*Math.max(0,Math.min(1,percentage));
	}
	
	/**
	 * @see #interpolate(double, double, double)
	 * @param f0
	 * @param f1
	 * @param percentage
	 * @return
	 */
	protected float interpolate(float f0, float f1, double percentage) {
		return (float)interpolate((double)f0, f1, percentage);
	}
	
	/**
	 * @see #interpolate(double, double, double)
	 * @param i0
	 * @param i1
	 * @param percentage
	 * @return
	 */
	protected int interpolate(int i0, int i1, double percentage) {
		return (int)interpolate((double)i0, i1, percentage);
	}
	
	/**
	 * @see #interpolate(double, double, double)
	 * @param b0
	 * @param b1
	 * @param percentage
	 * @return
	 */
	protected boolean interpolate(boolean b0, boolean b1, double percentage) {
		int i0 = (b0?1:0);
		int i1 = (b1?1:0);
		int i = (int)interpolate((double)i0, i1, percentage); 
		return i==1;
	}
	
}
