package graf.sequence;

public class FloatValue extends Value {

	private final float v0;
	private final float v1;
	
	public FloatValue(float v0) {
		this.v0 = v0;
		this.v1 = v0;
	}
	
	public FloatValue(float v0, float v1) {
		this.v0 = v0;
		this.v1 = v1;
	}
	
	public float asFloat() {
		return v0;
	}
	
	public Value copy() {
		return new FloatValue(v0,v1);
	}
	
	
	public Value interpolate(double percentage) {
		return new FloatValue(interpolate(v0, v1, percentage));
	}
	
	public String toString() {
		if (v0==v1) {
			return ""+v0;
		} else {
			return ""+v0+".."+v1;
		}
		
	}

	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + Float.floatToIntBits(v0);
		result = PRIME * result + Float.floatToIntBits(v1);
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final FloatValue other = (FloatValue) obj;
		if (Float.floatToIntBits(v0) != Float.floatToIntBits(other.v0))
			return false;
		if (Float.floatToIntBits(v1) != Float.floatToIntBits(other.v1))
			return false;
		return true;
	}

}

