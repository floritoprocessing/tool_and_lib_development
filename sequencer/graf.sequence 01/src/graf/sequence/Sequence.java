package graf.sequence;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import processing.core.PApplet;
import processing.core.PFont;

/**
 * 
 * 
 * @author mgraf
 * 
 */
public class Sequence {

	/**
	 * Reference to parent PApplet for checking for the existance of
	 * eventMethods
	 */
	private final PApplet parent;

	/**
	 * Time in frames
	 */
	private int time = 0;
	
	/**
	 * startTime of very first TrackItem
	 */
	private int minItemTime = Integer.MAX_VALUE;

	/**
	 * endTime of very last TrackItem
	 */
	private int maxItemTime = Integer.MIN_VALUE;
	
	/**
	 * Flag if Sequence has a <code>TrackItem</code>
	 */
	private boolean hasItem = false;
	
	/**
	 * An array holding if singleEvents have been fired at the indexed time This
	 * to avoid double firing of these events.
	 */
	public boolean[] singleEventFired;

	/**
	 * Frame base
	 */
	private final int frameBase;
	
	/**
	 * Determining if the sequence is synced in time to the PApplet
	 */
	private boolean syncToTime = false;

	/**
	 * The tracks
	 */
	private final Hashtable tracks = new Hashtable(); // <String,Track>

	private Method startMethod;
	private String startMethodName = "sequenceEventStart";
	private Class[] startMethodTypes = new Class[] { SequenceEvent.class };

	private Method stopMethod;
	private String stopMethodName = "sequenceEventStop";
	private Class[] stopMethodTypes = new Class[] { SequenceEvent.class };

	private Method runningMethod;
	private String runningMethodName = "sequenceEventRunning";
	private Class[] runningMethodTypes = new Class[] { SequenceEvent.class };

	/**
	 * Creates a new Sequence with the specified frameBase.
	 * <p>
	 * A Sequence is container for individual <code>Track</code> objects.
	 * 
	 * Each <code>Track</code> can hold several <code>TrackItem</code> objects.
	 * <code>TrackItem</code> objects have a startTime, a stopTime, and optional name and Value.
	 * <P>
	 * At the end of each call to <code>PApplet</code>'s <code>draw</code> method, 
	 * <code>SequenceEvent</code> objects are created which are  based 
	 * the time of the <code>Sequence</code> and all <code>TrackItem</code> objects. 
	 * These events are passed to the implemented <code>sequenceEventStart</code>, 
	 * <code>sequenceEventStop</code> and/or <code>sequenceEventRunning</code> methods.
	 * <p>
	 * If syncToTime is true, all <code>setTime</code> and <code>advanceFrame</code>(s) methods are ignore, 
	 * time is set by calling the millis() method of the parent <code>PApplet</code>
	 * <p>
	 * A small example:
	 * <pre><code>
	 * Sequence seq;
	 * void setup() {
	 *   size(640,480,P3D);
	 *   // create a new Sequence with synchronisation on:
	 *   seq = new Sequence(this, 25, true);
	 *   // add a track
	 *   Track track = seq.createTrack("First track");
	 *   // add TrackItems
	 *   track.addItem( new TrackItem("SingleEvent",10) );
	 *   track.addItem( new TrackItem("LongerEvent",15,50) );
	 * }
	 * 
	 * void draw() {
	 *   println(seq.getTimeInSeconds());
	 * }
	 * 
	 * </code></pre>
	 * 
	 * @param parent
	 *            the PApplet
	 * @param frameBase
	 *            frame base in frames per second
	 * @param syncToTime synchronize time to the parent PApplet
	 * 
	 */
	public Sequence(PApplet parent, int frameBase, boolean syncToTime) {
		this.parent = parent;
		if (parent == null) {
			throw new RuntimeException("PApplet cannot be null!!!");
		}

		this.frameBase = frameBase;
		if (frameBase <= 0) {
			throw new RuntimeException(
					"frameBase is not allowed to be less than 1");
		}
		
		this.syncToTime = syncToTime;
		
		parent.registerMethod("draw", this);
//		parent.registerDraw(this);

		PApplet.println("Sequence - written in 2009 by Marcus P. Graf, www.florito.net");
		PApplet.println("Checking methods: ");

		startMethod = registerMethod(startMethodName, startMethodTypes);
		stopMethod = registerMethod(stopMethodName, stopMethodTypes);
		runningMethod = registerMethod(runningMethodName, runningMethodTypes);

		singleEventFired = new boolean[frameBase * 60 * 5]; // preset 5 minutes
	}
	
	/**
	 * The draw method is called at the end of <code>PApplet</code>'s <code>draw()</code> method
	 *
	 */
	public void draw() {
		fireEvents();
		if (syncToTime) {
			setTimeAndArray((int) (parent.millis() * frameBase / 1000.0));
		}
	}
	
	/**
	 * Creates a new <code>Track</code> in the <code>Sequence</code> and returns it.
	 * 
	 * @param name the name of the Track
	 * @throws DuplicateTrackException when a track with the specified name already exists
	 */
	public Track createTrack(String name) {
		if (tracks.containsKey(name)) {
			throw new DuplicateTrackException(name);
		}
		Track track = new Track(this,name);
		tracks.put(name, track);
		return track;
	}

	/**
	 * Adds the track to the Sequence
	 * 
	 * @param track
	 *            the Track
	 */
	/*public void addTrack(Track track) {
		if (tracks.containsKey(track.getName())) {
			throw new DuplicateTrackException(track);
		}
		tracks.put(track.getName(), track);
	}*/

	/**
	 * Increases the time by one frame
	 */
	public void advanceFrame() {
		if (!syncToTime) setTimeAndArray(time + 1);
	}

	/**
	 * Increases the time by the specified frames
	 * 
	 * @param frames
	 */
	public void advanceFrames(int frames) {
		if (!syncToTime) setTimeAndArray(time + frames);
	}
	
	void calcMinAndMaxItemTime() {
		minItemTime = Integer.MAX_VALUE;
		maxItemTime = Integer.MIN_VALUE;
		Enumeration en = tracks.elements();
		while (en.hasMoreElements()) {
			Track track = (Track)en.nextElement();
			TrackItem[] item = track.getItems();
			for (int i=0;i<item.length;i++) {
				hasItem = true;
				minItemTime = Math.min(item[i].startTime,minItemTime);
				maxItemTime = Math.max(item[i].stopTime, maxItemTime);
			}
		}
	}

	private void checkAndGrowBoolean(int futureTime) {
		if (futureTime>=singleEventFired.length) {
			boolean[] newSingle = new boolean[singleEventFired.length*2];
			System.arraycopy(singleEventFired, 0, newSingle, 0, singleEventFired.length);
			singleEventFired = newSingle;
		}
	}
	
	public void drawTime(int strokeColor) {
		drawTime(strokeColor, minItemTime, maxItemTime);
	}
	
	public void drawTime(int strokeColor, int minTime, int maxTime) {
		parent.stroke(strokeColor);
		float timeScale = (float)parent.width/(maxTime-minTime);
		float x = (time-minTime)*timeScale;
		parent.line(x, 0, x, parent.height);
	}
	
	
	public void drawTracks(Integer strokeAndTextColor, Integer fillColor, PFont font) {
	
		if (font!=null) {
			parent.textFont(font);
		}

		//PApplet.println(tracks.size());
		
		
		Enumeration en = tracks.elements();
		float y=0;
		float ystep = (float)parent.height/tracks.size();
		while (en.hasMoreElements()) {
			
			if (fillColor==null) {
				parent.noFill();
			} else {
				parent.fill(fillColor.intValue());
			}
			
			if (strokeAndTextColor==null) {
				parent.noStroke();
			} else {
				parent.stroke(strokeAndTextColor.intValue());
			}
			
			parent.rectMode(PApplet.CORNER);
			parent.rect(0,y,parent.width-1,ystep-1);
			
			if (font!=null) {
				parent.fill( (strokeAndTextColor==null?0xff000000:strokeAndTextColor.intValue()) );
				parent.text( ((Track)en.nextElement()).getName(), 5, y+20 );
			}
			
			y+=ystep;
		}
		
	}
	
	public void drawTrackItems(Integer strokeColor, Integer fillColor) {
		drawTrackItems(strokeColor, fillColor, minItemTime, maxItemTime);
	}
	
	public void drawTrackItems(Integer strokeColor, Integer fillColor, int minTime, int maxTime) {
		
		
		if (fillColor==null) {
			parent.noFill();
		} else {
			parent.fill(fillColor.intValue());
		}
		
		if (strokeColor==null) {
			parent.noStroke();
		} else {
			parent.stroke(strokeColor.intValue());
		}
		
		
		
		if (hasItem) {
			
			//float timeScale = (float)parent.width/(maxItemTime-minItemTime);
			float timeScale = (float)parent.width/(maxTime-minTime);
			float y=0;
			float ystep = (float)parent.height/tracks.size();
			parent.rectMode(PApplet.CORNERS);
			
			Enumeration en = tracks.elements();
			while (en.hasMoreElements()) {
				Track track = (Track)en.nextElement();
				TrackItem[] item = track.getItems();
				
				float trackHeight = ystep-2;
				float itemHeight = trackHeight/item.length;
				
				for (int i=0;i<item.length;i++) {
					int startTime = item[i].startTime;
					int stopTime = item[i].stopTime;
					float x0 = (startTime-minTime)*timeScale;
					float x1 = (stopTime-minTime)*timeScale;
					float y0 = y+1 + i*itemHeight;
					float y1 = y0 + itemHeight-1;
					parent.rect(x0,y0,x1,y1);
				}
				y+=ystep;
			}
		}
	}

	private void fireEventRunning(SequenceEvent event) {
		if (runningMethod != null) {
			try {
				runningMethod.invoke(parent, new Object[] { event });
			} catch (Exception e) {
				System.err.println("Disabling " + runningMethodName
						+ " because of an error.");
				e.printStackTrace();
				runningMethod = null;
			}
		}
	}

	/**
	 * Core event firing routing Call this every time after (or before) you
	 * change the time to fire new <code>SequencerEvent</code>
	 * 
	 * @see #advanceFrame()
	 * @see #advanceFrames(int)
	 * @see #setTime(int)
	 */
	private void fireEvents() {

		/*
		 * Get all tracks:
		 */
		Enumeration en = tracks.elements();

		/*
		 * Loop through all tracks
		 */
		while (en.hasMoreElements()) {

			Track track = (Track) en.nextElement();

			/*
			 * Loop through all TrackItems
			 */
			for (int i = 0; i < track.events.size(); i++) {

				TrackItem trackItem = (TrackItem) track.events.get(i);

				/*
				 * Fire eventStarted
				 */
				if (trackItem.startTime == time && !singleEventFired[time]) {
					singleEventFired[time]=true;
					fireEventStarted(new SequenceEvent(SequenceEvent.START,
							track, trackItem, time));
				}

				/*
				 * Fire eventStopped
				 */
				if (trackItem.stopTime == time && !singleEventFired[time]) {
					singleEventFired[time]=true;
					fireEventStopped(new SequenceEvent(SequenceEvent.STOP,
							track, trackItem, time));
				}
				
				/*
				 * Fire eventRunning
				 */
				if (time >= trackItem.startTime && time < trackItem.stopTime) {
					fireEventRunning(new SequenceEvent(SequenceEvent.RUNNING,
							track, trackItem, time));
				}

			}
		}

	}

	private void fireEventStarted(SequenceEvent event) {
		if (startMethod != null) {
			try {
				startMethod.invoke(parent, new Object[] { event });
			} catch (Exception e) {
				System.err.println("Disabling " + startMethodName
						+ " because of an error.");
				e.printStackTrace();
				startMethod = null;
			}
		}
	}

	private void fireEventStopped(SequenceEvent event) {
		if (stopMethod != null) {
			try {
				stopMethod.invoke(parent, new Object[] { event });
			} catch (Exception e) {
				System.err.println("Disabling " + stopMethodName
						+ " because of an error.");
				e.printStackTrace();
				stopMethod = null;
			}
		}
	}

	/**
	 * Returns the frameBase of this Sequence
	 * 
	 * @return
	 */
	public int getFrameBase() {
		return frameBase;
	}

	/**
	 * Returns the time in frames
	 * 
	 * @return
	 */
	public int getTime() {
		return time;
	}

	/**
	 * Returns the time in seconds
	 * 
	 * @return
	 */
	public float getTimeInSeconds() {
		return (float) time / frameBase;
	}

	/**
	 * Returns the <code>Track</code> with the supplied name
	 * 
	 * @param name the Track to return
	 * @throws TrackNotFoundException when a Track with the supplied name was not found
	 * @return the Track with the supplied name
	 */
	public Track getTrack(String name) {
		Track out = (Track) tracks.get(name);
		if (out == null) {
			throw new TrackNotFoundException(name);
		} else {
			return out;
		}
	}

	/**
	 * Returns all Track names as an array of Strings
	 * 
	 * @return
	 */
	public String[] getTrackNames() {
		Enumeration e = tracks.keys();
		ArrayList out = new ArrayList();
		while (e.hasMoreElements()) {
			out.add(e.nextElement());
		}
		return (String[]) out.toArray(new String[0]);
	}

	private Method registerMethod(String name, Class[] params) {
		boolean present = true;
		Method method = null;
		try {
			method = parent.getClass().getMethod(name, params);
		} catch (Exception e) {
			present = false;
		}

		PApplet.print("- void " + name + "(");
		for (int i = 0; i < params.length; i++) {
			PApplet.print(params[i].getSimpleName() + " arg" + i);
			if (i < params.length - 1 && params.length > 1) {
				PApplet.print(", ");
			}
		}
		PApplet.println(")\t" + (present ? "IMPLEMENTED!" : "unimplemented"));
		return method;
	}

	/**
	 * Removes and returns a <code>Track</code> with the supplied name
	 * 
	 * @param name
	 * @throws TrackNotFoundException when a Track with the supplied name was not found
	 * @return the removed Track
	 */
	public Track removeTrack(String name) {
		Track out = (Track) tracks.remove(name);
		if (out == null) {
			throw new TrackNotFoundException(name);
		} else {
			calcMinAndMaxItemTime();
			return out;
		}
	}

	/**
	 * Sets the time (in frames) of the Sequence, when not in sync mode
	 * @param time the time in frames
	 * @see #syncOff()
	 * @see #syncOn()
	 */
	public void setTime(int time) {
		if (!syncToTime) setTimeAndArray(time);
	}
	

	/*
	 * All time setting routines call this one. Here you can enable/disable
	 * events to avoid double triggering of start events
	 */
	private void setTimeAndArray(int time) {
		int oldTime = this.time;
		int futureTime = time;
		checkAndGrowBoolean(futureTime);
		
		/*
		 * Going forward in time:
		 * set all eventFired between oldTime(including) and futureTime(excluding) to true 
		 */
		if (futureTime>=oldTime) {
			singleEventFired[oldTime]=true;
			for (int i=oldTime;i<futureTime;i++) {
				singleEventFired[i]=true;
			}
		}
		
		/*
		 * Going backward in time:
		 * set all eventFired betwen futureTime(including) and oldTime(including) to false 
		 */
		else {
			//PApplet.println("SET TIME: "+oldTime+".."+futureTime);
			singleEventFired[futureTime] = false;
			for (int i=futureTime;i<=oldTime;i++) {
				singleEventFired[i] = false;
			}
		}
		
		this.time = futureTime;
	}
	

	/**
	 * Sets the time (in milliseconds) of the Sequence, when not in sync mode
	 * @param milliseconds the time in milliseconds
	 * @see #syncOff()
	 * @see #syncOn()
	 */
	public void setTimeInMilliseconds(long milliseconds) {
		if (!syncToTime) setTimeAndArray((int) (milliseconds * frameBase / 1000.0));
	}

	/**
	 * Sets the time (in seconds) of the Sequence, when not in sync mode
	 * @param timeInSeconds the time in seconds
	 * @see #syncOff()
	 * @see #syncOn()
	 */
	public void setTimeInSeconds(double timeInSeconds) {
		if (!syncToTime) setTimeAndArray((int) (timeInSeconds * frameBase));
	}
	
	/**
	 * Turns <code>PApplet</code> synchronisation on:
	 * <ul> 
	 * <li>The time is set by the parent <code>PApplet</code>'s time (as called by {@link PApplet#millis()})
	 * </ul>
	 * The {@link #setTime(int)}, {@link #setTimeInMilliseconds(long)} and {@link #setTimeInSeconds(double)}
	 * methods are disabled.
	 */
	public void syncOn() {
		syncToTime = true;
	}
	
	/**
	 * Turns PApplet synchronisation off.
	 * @see #syncOn() 
	 */
	public void syncOff() {
		syncToTime = false;
	}

	

	

	
}
