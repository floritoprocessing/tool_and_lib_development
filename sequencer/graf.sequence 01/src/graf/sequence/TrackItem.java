package graf.sequence;

public class TrackItem implements Comparable {
	
	final String name;
	final int startTime;
	final int stopTime;
	final Value value;
	
	public TrackItem(int startTime) {
		this(null,startTime,startTime+1, null);
	}
	
	public TrackItem(String name, int startTime) {
		this(name,startTime,startTime+1, null);
	}
	
	public TrackItem(int startTime, Value value) {
		this(null,startTime,startTime+1, value);
	}
	
	public TrackItem(String name, int startTime, Value value) {
		this(name,startTime,startTime+1, value);
	}
	
	public TrackItem(int startTime, int stopTime) {
		this(null,startTime,stopTime,null);
	}
	
	public TrackItem(String name, int startTime, int stopTime) {
		this(name,startTime,stopTime,null);
	}
	
	public TrackItem(int startTime, int stopTime, Value value) {
		this(null,startTime,stopTime,value);
	}
	
	/**
	 * Creates a new code>TrackItem</code> with the supplied startTime, stopTime and <code>Value</code>.
	 * @param startTime
	 * @param stopTime
	 * @param object
	 */
	public TrackItem(String name, int startTime, int stopTime, Value value) {
		if (stopTime<=startTime) {
			throw new RuntimeException("Illegal arguments: stopTime has to be larger that startTime. " +
					"(startTime="+startTime+", stopTime="+stopTime+")");
		}
		this.name = name;
		this.startTime = startTime;
		this.stopTime = stopTime;
		if (value==null) {
			this.value = null;
		} else {
			this.value = value.copy();
		}
	}
	
	public int compareTo(Object arg0) {
		TrackItem other = (TrackItem)arg0;
		if (startTime<other.startTime) {
			return -1;
		} else if (startTime>other.startTime) {
			return 1;
		} else {
			// equal startTimes:
			if (stopTime<other.stopTime) {
				return -1;
			} else if (stopTime>other.stopTime) {
				return 1;
			} else {
				return 0;//name.compareTo(other.name);
			}
		}
	}
	
	/**
	 * Returns a copy of this <code>TrackItem</code>
	 * @return
	 */
	public TrackItem copy() {
		if (value==null) {
			return new TrackItem(name, startTime, stopTime, null);
		} else {
			return new TrackItem(name, startTime, stopTime, value.copy());
		}
	}
	
	public double getEventPercentageAt(int time) {
		return (time-startTime)/((double)stopTime-startTime);
	}
	
	/*public int getEventTimeAt(int time) {
		return time-startTime;
	}*/
	
	public int getStartTime() {
		return startTime;
	}
	
	public int getStopTime() {
		return stopTime;
	}
	
	public Value getValue() {
		return value;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(getClass().getName());
		result.append("[");
		result.append("name="+name);
		result.append(",startTime="+startTime);
		result.append(",stopTime="+stopTime);
		if (value!=null) {
			result.append(",value="+value);
		}
		result.append("]");
		return result.toString();
	}

	/**
	 * Returns the name of this <code>TrackItem</code>
	 * @return
	 */
	public String getName() {
		return name;
	}

	

}
