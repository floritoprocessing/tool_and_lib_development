/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// ANAGLYPH IMAGE CREATION ////////////////////////////////////////

////////// red/green BImage creation

void createAnaglyph(PImage img1, PImage img2, PImage img) {
  createRed(img1);
  createGreen(img2);
  screen(img1,img2,img);
}

// create red channel: (uses image buffer);
PImage createRed(PImage img) {
  loadPixels();
  background(0); colorMode(HSB,255); tint(0,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  img.updatePixels();
  return img;
}

// create green channel: (uses image buffer);
PImage createGreen(PImage img) {
  loadPixels();
  background(0); colorMode(HSB,255); tint(90,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  img.updatePixels();
  return img;
}

// screen two images:
PImage screen(PImage img_a, PImage img_b, PImage img) {
  int a,b;
  img_a.loadPixels();
  img_b.loadPixels();
  for (int i=0;i<img_a.pixels.length;i++) {
    a=img_a.pixels[i]; b=img_b.pixels[i];
    int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
    int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
    int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
    img.pixels[i]=(rr<<16|gg<<8|bb);
  }
  img.updatePixels();
  return img;
}

// color functions:
int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

//////////////////////////////////////// end ANAGLYPH IMAGE CREATION ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
