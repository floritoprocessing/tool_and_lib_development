package processing.graf;

import processing.core.PApplet;

public class ColorFunctions {
	
	public enum Blendmode {
		NORMAL,
		ADD,
	}
	
	private static Blendmode mode=Blendmode.NORMAL;
	
	public static Blendmode getBlendmode() {
		return mode;
	}
	
	public static void setBlendmode(Blendmode blendmode) {
		mode = blendmode;
	}
	
	public static int blendNormal(int background, int color) {
		return color;
	}
	
	public static int blendNormal(int background, int color, float opac) {
		float bo = 1-opac;
		return rgb(
				max(bo*r(background)+opac*r(color)),
				max(bo*g(background)+opac*g(color)),
				max(bo*b(background)+opac*b(color)));
	}
	
	private static int max(int v) {
		return (v<256?v:255);
	}
	
	private static int max(float v) {
		return (v<256?(int)v:255);
	}
	
	public static int blendAdd(int background, int color) {
		return rgb(
				max(r(background)+r(color)),
				max(g(background)+g(color)),
				max(b(background)+b(color)));
	}
	
	public static int blendAdd(int background, int color, float opac) {
		return rgb(
				max(r(background)+opac*r(color)),
				max(g(background)+opac*g(color)),
				max(b(background)+opac*b(color)));
	}
	
	public static int rgb(int r, int g, int b) {
		return r<<16|g<<8|b;
	}
	
	public static int mix(int... colors) {
		int r=0, g=0, b=0;
		for (int c:colors) {
			r += r(c);
			g += g(c);
			b += b(c);
		}
		return rgb(r/colors.length, g/colors.length, b/colors.length);
	}	
	
	public static int r(int c) {
		return c>>16&0xff;
	}
	public static int g(int c) {
		return c>>8&0xff;
	}
	public static int b(int c) {
		return c&0xff;
	}
	
	/**
	 * Returns a blend of the two colors depending on current blendMode
	 * @param background
	 * @param color
	 * @return
	 * @see #setBlendmode(processing.graf.ColorFunctions.Blendmode)
	 * @see #getBlendmode()
	 */
	public static int blend(int background, int color) {
		switch (mode) {
		case NORMAL: return blendNormal(background, color);
		case ADD: return blendAdd(background, color);
		default: throw new RuntimeException("Unimplemented blend mode "+mode);
		}
	}
	
	public static int blend(int background, int color, float opac) {
		switch (mode) {
		case NORMAL: return blendNormal(background, color, opac);
		case ADD: return blendAdd(background, color, opac);
		default: throw new RuntimeException("Unimplemented blend mode "+mode);
		}
	}

	public static void dot(PApplet pa, int x, int y, int c) {
		pa.set(x,y,blend(pa.get(x,y),c));
	}
	
	public static void dot(PApplet pa, int x, int y, int c, float opac) {
		pa.set(x,y,blend(pa.get(x,y),c,opac));
	}
	
	public static void dot(PApplet pa, float x, float y, int c, float opac) {
		if (x<0 || y<0) return; 
		
		int x0 = (int)x;
		int x1 = x0+1;
		int y0 = (int)y;
		int y1 = y0+1;
		
		float px1 = x-x0;
		float px0 = 1-px1;
		float py1 = y-y0;
		float py0 = 1-py1;
		
		float p00 = opac*px0*py0;
		float p01 = opac*px0*py1;
		float p10 = opac*px1*py0;
		float p11 = opac*px1*py1;
		
		pa.set(x0,y0,blend(pa.get(x0,y0),c,p00));
		pa.set(x0,y1,blend(pa.get(x0,y1),c,p01));
		pa.set(x1,y0,blend(pa.get(x1,y0),c,p10));
		pa.set(x1,y1,blend(pa.get(x1,y1),c,p11));
	}
	
	/*public static int brightness(int c) {
		return (r(c)+r(c)+r(c)+g(c)+g(c)+g(c)+g(c)+g(c)+b(c))<<3;
	}*/
	
	public static void threshold(PApplet pa, int threshold) {
		pa.loadPixels();
		int br=0;
		for (int i=0;i<pa.pixels.length;i++) {
			br = r(pa.pixels[i])+g(pa.pixels[i])+b(pa.pixels[i]);
			if (br<threshold*3) pa.pixels[i]=0;
			else pa.pixels[i]=0xffffff;
		}
		pa.updatePixels();
	}
	
	public static void hBlur(PApplet pa) {
		int[] row = new int[pa.width];
		pa.loadPixels();
		int i=0;
		for (int y=0;y<pa.height;y++) {
			i++;
			for (int x=1;x<pa.width-1;x++) {
				row[x] = mix(pa.pixels[i-1],pa.pixels[i],pa.pixels[i+1]);
				i++;
			}
			i++;
			row[0] = row[1];
			row[pa.width-1] = row[pa.width-2];
			System.arraycopy(row, 0, pa.pixels, y*pa.width, pa.width);
		}
		pa.updatePixels();
	}
	
	public static void vBlur(PApplet pa) {
		int[] col = new int[pa.height];
		pa.loadPixels();
		int i=0;
		for (int x=0;x<pa.width;x++) {
			i = pa.width+x;
			for (int y=1;y<pa.height-1;y++) {
				col[y] = mix(pa.pixels[i-pa.width],pa.pixels[i],pa.pixels[i+pa.width]);
				i+=pa.width;
			}
			col[0]=col[1];
			col[pa.height-1] = col[pa.height-2];
			i = x;
			for (int y=0;y<pa.height;y++) {
				pa.pixels[i] = col[y];
				i += pa.width;
			}
		}
		pa.updatePixels();
	}
}
