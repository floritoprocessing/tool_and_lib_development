package graf.processing;

import processing.core.PApplet;

public class Grafix {

	static final double DEG_TO_RAD = Math.PI/180.0;

	static final double RAD_TO_DEG = 180.0f/Math.PI;

	static final protected double SINCOS_PRECISION = 0.5;

	static final protected int SINCOS_LENGTH = (int) (360.0 / SINCOS_PRECISION);

	static final protected double sinLUT[];

	static final protected double cosLUT[];

	public static final int CORNER = 0;

	public static final int CORNERS = 1;

	public static final int RADIUS = 2;

	public static final int CENTER = 3;
	
	public static final int OPEN = 1;
	public static final int CLOSE = 2;
	static {
	    sinLUT = new double[SINCOS_LENGTH];
	    cosLUT = new double[SINCOS_LENGTH];
	    for (int i = 0; i < SINCOS_LENGTH; i++) {
	      sinLUT[i] = Math.sin(i * DEG_TO_RAD * SINCOS_PRECISION);
	      cosLUT[i] = Math.cos(i * DEG_TO_RAD * SINCOS_PRECISION);
	    }
	  }
	private static int alpha(int c) {
		return c>>24&0xff;
	}
	private static int blue(int c) {
		return c&0xff;
	}
	private static int color(int r, int g, int b, int alpha) {
		return alpha<<24|r<<16|g<<8|b;
	}
	private static int green(int c) {
		return c>>8&0xff;
	}
	
	private static int mix(int c0, int c1, double p0) {
		double p1 = 1.0-p0;
		int r = (int)Math.min(255, red(c0)*p0 + red(c1)*p1);
		int g = (int)Math.min(255, green(c0)*p0 + green(c1)*p1);
		int b = (int)Math.min(255, blue(c0)*p0 + blue(c1)*p1);
		return color(r,g,b,0xff);
	}
	private static int red(int c) {
		return c>>16&0xff;
	}

	
	
	private static void set(PApplet pa, int x, int y, int color) {
		double p = alpha(color)/255.0;
		int bgColor=pa.get(x, y);
		int drawColor = mix(color,bgColor,p);
		pa.set(x, y, drawColor);
	}
	private final PApplet pa;
	private boolean smoothing = false;
	//private boolean strokeActive = true;
	private boolean fillActive = true;
	
	private int VERTEX_COUNT = 0;
	private double[][] vertices;

	private int ellipseMode = RADIUS;

	private int stroke = 0xffffffff;

	private int fill = 0xffffffff;

	public Grafix(PApplet pa) {
		this.pa = pa;
	}

	public Grafix(PApplet pa, boolean smoothing) {
		this(pa);
		this.smoothing = smoothing;
	}

	private void beginShape() {
		vertices = new double[16][2];
		VERTEX_COUNT = 0;
	}

	public void ellipse(double a, double b, double c, double d) {
		double x = a;
		double y = b;
		double w = c;
		double h = d;

		if (ellipseMode == CORNERS) {
			w = c - a;
			h = d - b;

		} else if (ellipseMode == RADIUS) {
			x = a - c;
			y = b - d;
			w = c * 2;
			h = d * 2;

		} else if (ellipseMode == CENTER) {
			x = a - c/2f;
			y = b - d/2f;
		}

		if (w < 0) {  // undo negative width
			x += w;
		w = -w;
		}

		if (h < 0) {  // undo negative height
			y += h;
			h = -h;
		}

		ellipseImpl(x, y, w, h);
	}
	
	
	
	protected void ellipseImpl(double x1, double y1, double w, double h) {
		double hradius = w / 2;
		double vradius = h / 2;

		double centerX = x1 + hradius;
		double centerY = y1 + vradius;
		int accuracy = (int)(4+Math.sqrt(hradius+vradius)*3);
		double inc = (float)SINCOS_LENGTH / accuracy;
		double val = 0;

		val = 0;
		beginShape(); //LINE_LOOP);
		for (int i = 0; i < accuracy; i++) {
			vertex(centerX + cosLUT[(int) val] * hradius,
					centerY + sinLUT[(int) val] * vradius);
			val += inc;
		}
		endShape(CLOSE);
	}
	
	public void ellipseMode(int mode) {
		ellipseMode = mode;
	}
	
	private void endShape(int closeMode) {
		if (closeMode!=CLOSE) throw new RuntimeException("Only use CLOSE as parameter!");
		double x0 = vertices[0][0];
		double y0 = vertices[0][1];
		
		for (int i=0;i<VERTEX_COUNT-1;i++) {
			line(vertices[i][0],vertices[i][1],vertices[i+1][0],vertices[i+1][1]);
		}
		line(x0,y0,vertices[VERTEX_COUNT-1][0],vertices[VERTEX_COUNT-1][1]);
	}

	public void fill(int fill) {
		this.fill = fill;
		fillActive = true;
	}

	public void line(double x0, double y0, double x1, double y1) {
		double dx = x1-x0;
		double dy = y1-y0;
		double d = Math.sqrt(dx*dx+dy*dy);
		double step = 1/d;
		for (double p=0;p<=1;p+=step) {
			double x = x0 + p*dx;
			double y = y0 + p*dy;
			if (!smoothing)
				set(pa,(int)x,(int)y,stroke);
			else
				set(pa,x,y,stroke);
		}
	}

	public void noFill() {
		fillActive = false;
	}

	public void noSmooth() {
		smoothing = false;
	}

	private void set(PApplet pa, double x, double y, int color) {
		int x0=(int)x, x1=x0+1;
		int y0=(int)y, y1=y0+1;
		double px1=x-x0, px0=1.0-px1;
		double py1=y-y0, py0=1.0-py1;
		double p = alpha(color)/255.0;
		double p00=p*px0*py0;
		double p01=p*px0*py1;
		double p10=p*px1*py0;
		double p11=1-p00-p01-p10;

		pa.set(x0, y0, mix(color,pa.get(x0,y0),p00));
		pa.set(x0, y1, mix(color,pa.get(x0,y1),p01));
		pa.set(x1, y0, mix(color,pa.get(x1,y0),p10));
		pa.set(x1, y1, mix(color,pa.get(x1,y1),p11));
	}

	public void smooth() {
		smoothing = true;
	}

	/**
	 * @param stroke
	 *            the stroke to set
	 */
	public void stroke(int grayValue) {
		if (grayValue<0x100)
			stroke(grayValue, grayValue, grayValue);
		else if (grayValue<0x1000000)
			stroke = 0xff<<24 | grayValue;
		else
			stroke = grayValue;
	}

	public void stroke(int red, int green, int blue) {
		stroke(red, green, blue, 255);
	}

	public void stroke(int red, int green, int blue, int alpha) {
		stroke = Math.min(255, Math.max(0, alpha)) << 24
		| Math.min(255, Math.max(0, red)) << 16
		| Math.min(255, Math.max(0, green)) << 8
		| Math.min(255, Math.max(0, blue));
	}

	private void vertex(double x, double y) {
		if (VERTEX_COUNT==vertices.length) {
			double[][] temp = new double[vertices.length<<1][2];
			System.arraycopy(vertices, 0, temp, 0, vertices.length);
			vertices=temp;
		}
		vertices[VERTEX_COUNT][0]=x;
		vertices[VERTEX_COUNT][1]=y;
		VERTEX_COUNT++;
	}

}
