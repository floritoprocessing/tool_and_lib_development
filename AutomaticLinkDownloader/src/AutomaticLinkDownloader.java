import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class AutomaticLinkDownloader extends JFrame implements ActionListener, FileChangeMonitorListener, PropertyChangeListener {
	public static boolean DEBUG;

	public AutomaticLinkDownloader() {
	}
	
	private JButton butSelectFile;
	
	private FileChangeMonitor fileMonitor;
	private DownloadDescriptionFileParser fileParser = new DownloadDescriptionFileParser();
	private DownloaderViewController downloader;
	
	public void createGUI() {
		
		setTitle("Automatic link downloader");
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		JTextArea area = new JTextArea();
		area.setBorder(new LineBorder(Color.BLACK, 2));
		String nl = System.getProperty("line.separator");
		area.setText(
				"The file should contain only links and should look something like this:" + nl +
				nl + 
				"maxdownloads 5" + nl +
				"targetdir D:\\temp" + nl +
				"link https://archive.org/download/FireCUPhotoJPEG/Fire%20CU%20H264.mp4" + nl +
				"link https://archive.org/download/FireCU2PhotoJPEG/Fire%20CU%202%20H264.mp4" + nl +
				nl +
				"The file will be polled every few seconds for changes" + nl
				);
		panel.add(area);
		
		butSelectFile = new JButton("Select file to monitor");
		butSelectFile.addActionListener(this);
		panel.add(butSelectFile);
		
		downloader = new DownloaderViewController();
		downloader.addPropertyChangeListener(this);
		panel.add(downloader);
		
		setContentPane(panel);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	
	
	
	

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src.equals(butSelectFile)) {
			selectFile();
		}
	}
	
	
	
	
	
	

	/**
	 * 
	 */
	private void selectFile() {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Select the file to monitor");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileFilter() {
			@Override
			public String getDescription() {
				return "Text files";
			}
			@Override
			public boolean accept(File arg0) {
				return arg0.isDirectory() || (arg0.isFile() && arg0.getName().toLowerCase().endsWith(".txt"));
			}
		});
		int result = chooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			startFileMonitor(chooser.getSelectedFile());
		}
	}




	/**
	 * 
	 */
	private void startFileMonitor(File file) {
		
		if (fileMonitor!=null && fileMonitor.isRunning()) {
			fileMonitor.stop();
			fileMonitor = null;
		}
		
		fileMonitor = new FileChangeMonitor(file, this);
		fileMonitor.start();
		
	}

	/* (non-Javadoc)
	 * @see FileChangeMonitorListener#fileChangeMonitorError(java.lang.String)
	 */
	@Override
	public void fileChangeMonitorError(String errorMessage) {
		System.err.println("FileChangeMonitorError: "+errorMessage);
	}

	/* (non-Javadoc)
	 * @see FileChangeMonitorListener#fileHasChanged(java.io.File)
	 */
	@Override
	public void fileHasChanged(File file) {
		System.out.println("The file "+file.getAbsolutePath()+" has changed!");
		
		DownloadDescription descr = fileParser.parseFile(file);
		if (descr != null) {
			System.out.println(descr);
			
			downloader.update(descr);
		}
	}

	
	
	
	
	


	/* (non-Javadoc)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		if (evt.getSource().equals(downloader)) {
			
			if (evt.getPropertyName().equals(DownloaderViewController.DOWNLOADER_COUNT)) {
				System.out.println("Downloader count changed");
				pack();
			}
			
		}
		// TODO Auto-generated method stub
		
	}











	private static void setupGui() {
		
		AutomaticLinkDownloader main = new AutomaticLinkDownloader();
		main.createGUI();
		main.pack();
		main.setVisible(true);
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				setupGui();
			}

		});
	}







}
