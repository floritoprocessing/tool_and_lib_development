import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;

import javax.swing.JProgressBar;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */

public class Downloader implements Runnable {
	
	String src;
	File target;
	String name;
	JProgressBar bar;
	long totalBytes;
	
	public Downloader(String src, File target, String name) {
		this.src = src;
		this.target = target;
		this.name = name;
		bar = new JProgressBar(0,100);
		bar.setStringPainted(true);
		bar.setString(name);
	}
	
	
	public String toString() {
		return "Downloader("+src+" -> "+target.getAbsolutePath()+")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((src == null) ? 0 : src.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Downloader other = (Downloader) obj;
		if (src == null) {
			if (other.src != null)
				return false;
		} else if (!src.equals(other.src))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}



	public void checkTargetAndStart() {
		
		while (target.exists()) {
			
			String filename = target.getName();
			String fileWithoutExtension = filename;
			String extension = "";
			int lastDot = filename.lastIndexOf(".");
			if (lastDot>=0) {
				fileWithoutExtension = filename.substring(0,lastDot);
				extension = filename.substring(lastDot+1);
			}
			
			fileWithoutExtension += "_";
			String newFilename = fileWithoutExtension + "." + extension;
			
			target = new File(target.getParentFile(), newFilename);
			bar.setString(newFilename);
			
		}
		
		Thread t = new Thread(this);
		t.start();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
		try {
			URL url = new URL(src);
			connection = (HttpURLConnection)url.openConnection();
			connection.connect();
			
			// expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                dispatchDone(new RuntimeException("Illegal response code"));
            } else {
            	totalBytes = connection.getContentLength();
            	System.out.println("fileLength="+totalBytes);
            	
            	input = connection.getInputStream();
                output = new FileOutputStream(target);
                
                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
//                    if (isCancelled()) {
//                        input.close();
//                        dispatchDone();
//                    }
                    total += count;
                    // publishing the progress....
                    if (totalBytes > 0) // only if total length is known
                        publishProgress(total, totalBytes);
                    output.write(data, 0, count);
                }
            }
			
			
            
		} catch (MalformedURLException e) {
			dispatchDone(e);
		} catch (IOException e) {
			dispatchDone(e);
		} finally {
		 try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
		}
		
		dispatchDone(null);
//		try {
//			URL u = new URL(src);
//			URLConnection uc = u.openConnection();
//		    int contentLength = uc.getContentLength();
//		    //String contentType = uc.getContentType();
////		    if (contentType.startsWith("text/") || contentLength == -1) {
////		      throw new IOException("This is not a binary file.");
////		    }
//		    System.out.println("Content length = "+contentLength);
//		    InputStream raw = uc.getInputStream();
//		    InputStream in = new BufferedInputStream(raw);
//		    byte[] data = new byte[contentLength];
//		    int bytesRead = 0;
//		    int offset = 0;
//		    while (offset < contentLength) {
//		      bytesRead = in.read(data, offset, data.length - offset);
//		      System.out.println("Read "+bytesRead+" of "+contentLength);
//		      if (bytesRead != -1) {
//		    	  offset += bytesRead;
//		      }
//		    }
//		    in.close();
//		} catch (MalformedURLException e) {
//			dispatchDone(e);
//		} catch (IOException e) {
//			dispatchDone(e);
//		}
	}

	
	private static NumberFormat nf;
	static {
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
	}
	
	/**
	 * @param i
	 */
	private void publishProgress(long downloaded, long total) {
		
		float percentage = (float)downloaded / (float)total;
		
		String downString = stringifyBytes(downloaded);
		String totalString = stringifyBytes(total);
		
		bar.setValue((int)(percentage*100));
		bar.setString(name+": "+downString+" of "+totalString+" ("+nf.format(percentage*100)+"%)");
	}

	
	private static int KB = 1024;
	private static int MB = 1024*1024;
	
	/**
	 * @param total
	 * @return
	 */
	private static String stringifyBytes(long bytes) {
		if (bytes<KB) {
			return bytes+" bytes";
		} else if (bytes<MB) {
			return nf.format((float)bytes/KB)+" kBytes";
		} else {
			return nf.format((float)bytes/MB)+" MB";
		}
	}

	/**
	 * 
	 */
	private void dispatchDone(Exception e) {
		if (e!=null) {
			System.out.println("Down with error "+e);
		} else {
			System.out.println("Done!");
			bar.setValue(100);
		}
		
		//bar.firePropertyChange(propertyName, oldValue, newValue)
	}
	
}