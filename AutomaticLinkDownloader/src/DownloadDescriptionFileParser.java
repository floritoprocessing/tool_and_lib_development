import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class DownloadDescriptionFileParser {
	public static boolean DEBUG;

	/*
	maxdownloads 5
	targetdir D:\temp
	link https://archive.org/download/FireCUPhotoJPEG/Fire%20CU%20H264.mp4
	link https://archive.org/download/FireCU2PhotoJPEG/Fire%20CU%202%20H264.mp4
	 */
	
	public DownloadDescriptionFileParser() {
	}
	
	
	public DownloadDescription parseFile(File file) {
		
		DownloadDescription descr = null;
		BufferedReader reader = null;
		FileReader fr = null;
		try {
			fr = new FileReader(file);
			reader = new BufferedReader(fr);
			String line = null;
			
			descr = new DownloadDescription();
			while ((line=reader.readLine())!=null) {
				extract(line, descr);
			}
			postCheck(file,descr);
			
		} catch (FileNotFoundException e) {
			// from fr = new FileReader(file);
			e.printStackTrace();
		} catch (IOException e) {
			// from reader.readLine()
			e.printStackTrace();
		} finally {
			if (reader!=null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return descr;
	}

	
	
	

	private static final String MAXDOWNLOADS = "maxdownloads";
	private static final String TARGETDIR = "targetdir";
	private static final String LINK = "link";

	/**
	 * @param line
	 * @param descr
	 */
	private void extract(String line, DownloadDescription descr) {
		
		String firstWord = line.split(" ")[0];
		String rest = line.substring(firstWord.length()+1);
		if (firstWord.equals(MAXDOWNLOADS)) {
			try {
				descr.maxDownloads = Integer.parseInt(rest);
			} catch (NumberFormatException e) {
				descr.maxDownloads = 1;
			}
		}
		else if (firstWord.equals(TARGETDIR)) {
			File dir = new File(rest);
			if (!dir.exists()) {
				System.out.println(this+" making directory "+rest);
				dir.mkdirs();
			}
			descr.targetDir = dir;
		}
		else if (firstWord.equals(LINK)) {
			descr.links.add(rest);
		}
		
	}

	
	/**
	 * @param file
	 * @param descr
	 */
	private void postCheck(File file, DownloadDescription descr) {
		if (descr.maxDownloads<1) descr.maxDownloads=1;
		if (descr.targetDir==null) {
			descr.targetDir = file.getParentFile();
		}
	}


	
}
