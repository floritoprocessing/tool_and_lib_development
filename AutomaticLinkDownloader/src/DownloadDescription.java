import java.io.File;
import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class DownloadDescription {
	
	private static final String NL = System.getProperty("line.separator");
	public int maxDownloads = 2;
	public File targetDir;
	public ArrayList<String> links = new ArrayList<String>();

	public DownloadDescription() {
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("maxdownloads "+maxDownloads);
		sb.append(NL);
		sb.append("targetdir "+targetDir.getAbsolutePath());
		sb.append(NL);
		for (String link:links) {
			sb.append("link "+link);
			sb.append(NL);
		}
		return sb.toString();
	}
}
