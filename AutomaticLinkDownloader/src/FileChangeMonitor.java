import java.io.File;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class FileChangeMonitor implements Runnable {
	
	public static long POLLING_INTERVAL = 1000;
	
	public static boolean DEBUG;

	private long fileTimeStamp = 0;
	private File file;
	private FileChangeMonitorListener listener;
	private boolean running = false;
	
	public FileChangeMonitor(File file, FileChangeMonitorListener listener) {
		this.file = file;
		this.listener = listener;
	}
	
	public void start() {
		if (!running) {
			running = true;
			Thread thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop() {
		running = false;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		
		while (running) {
			if (!file.exists()) {
				listener.fileChangeMonitorError("The file "+file.getAbsolutePath()+" does not exist!");
			} else if (file.isDirectory()) {
				listener.fileChangeMonitorError("The file "+file.getAbsolutePath()+" is a directory!");
			} else {
				long timeStamp = file.lastModified();
				if (timeStamp != fileTimeStamp) {
					fileTimeStamp = timeStamp;
					listener.fileHasChanged(file);
				}
			}
			
			try {
				Thread.sleep(POLLING_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isRunning() {
		return running;
	}
	

}
