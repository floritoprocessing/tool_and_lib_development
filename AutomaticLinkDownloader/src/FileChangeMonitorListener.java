import java.io.File;

/**
 * @author Marcus Graf (florito.net)
 *
 */
public interface FileChangeMonitorListener {

	public void fileChangeMonitorError(String errorMessage);
	public void fileHasChanged(File file);
	
}
