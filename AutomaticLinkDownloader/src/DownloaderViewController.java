import java.awt.AWTEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * 
 */

/**
 * @author Marcus Graf (florito.net)
 *
 */
public class DownloaderViewController extends JPanel implements PropertyChangeListener {
	
	
	private static final long serialVersionUID = 2322084875223329057L;




	public static final String DOWNLOADER_COUNT = "downloaderCount";
	
	private int maxDownloads;
	private ArrayList<Downloader> queue = new ArrayList<Downloader>();
	private ArrayList<Downloader> downloads = new ArrayList<Downloader>();
	
	public DownloaderViewController() {
		super();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	}
	
	public void update(DownloadDescription description) {
		this.maxDownloads = description.maxDownloads;
		
		for (String link:description.links) {
			
			try {
				
				String src = link;
				String linkFilename = URLDecoder.decode(link, "UTF-8");
				linkFilename = linkFilename.substring(linkFilename.lastIndexOf("/")+1);
				File target = new File(description.targetDir, linkFilename);
				//System.out.println(src+" -> "+target);
				
				Downloader downloader = new Downloader(src, target, linkFilename);
				if (!queue.contains(downloader)) {
					System.out.println("Adding "+downloader);
					queue.add(downloader);
				} else {
					System.out.println("Skipping already present "+downloader);
				}
				
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
		}
		
		parseNext();
	}
	
	
	
	private void parseNext() {
		
		while (downloads.size() < maxDownloads && queue.size()>0) {
			
			int oldValue = downloads.size();
			Downloader downloader = queue.remove(0);
			downloads.add(downloader);
			this.add(downloader.bar);
			int newValue = downloads.size();
			
			downloader.checkTargetAndStart();
			downloader.bar.addPropertyChangeListener(this);
			
			firePropertyChange(DOWNLOADER_COUNT, oldValue, newValue);
			
			
		}
		
	}

	/* (non-Javadoc)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		if (arg0.getSource().getClass().equals(JProgressBar.class)) {
			JProgressBar bar = (JProgressBar)arg0.getSource();
			if (bar.getValue()==100) {
				
				System.out.println("100 percent reached");
				int oldValue = downloads.size();
				
				for (int i=downloads.size()-1;i>=0;i--) {
					if (downloads.get(i).bar.equals(bar)) {
						
						downloads.remove(i);
					}
				}
				this.remove(bar);
				int newValue = downloads.size();
				firePropertyChange(DOWNLOADER_COUNT, oldValue, newValue);
				
				parseNext();
			}
		}
	}
}
