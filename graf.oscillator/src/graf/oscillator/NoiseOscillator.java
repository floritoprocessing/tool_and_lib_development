package graf.oscillator;

public class NoiseOscillator extends Oscillator {

	public double getAmplitude() {
		return 2*Math.random()-1;
	}

	public String toString() {
		return ("Noise Oscillator");
	}

}
