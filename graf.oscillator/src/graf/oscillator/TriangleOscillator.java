package graf.oscillator;

public class TriangleOscillator extends Oscillator {

	public TriangleOscillator(double frequency) {
		super(frequency);
	}

	public TriangleOscillator(double frequency, double phaseOffset) {
		super(frequency, phaseOffset);
	}

	public double getAmplitude() {
		double p = getCurrentPhase();
		double p4 = p*4;
		if (p<=0.25) return p4;
		else if (p<=0.75) return 2-p4;
		return p4-4;
	}
	
	public String toString() {
		return ("Triangle Oscillator f="+frequency+", phase="+phase);
	}

}
