package graf.oscillator;

public class SineOscillator extends Oscillator {

	public SineOscillator(double frequency) {
		super(frequency);
	}

	public SineOscillator(double frequency, double phase) {
		super(frequency, phase);
	}

	public double getAmplitude() {
		return Math.sin(Math.PI*2*getCurrentPhase());
	}
	
	public String toString() {
		return ("Sine Oscillator f="+frequency+", phase="+phase);
	}

}
