package graf.oscillator;

public class Signal {

	public static double add(Oscillator a, Oscillator b, double facA, double facB) {
		return a.getAmplitude()*facA + b.getAmplitude()*facB;
	}

	public static double rectify(double signal) {
		return Math.abs(signal);
	}
	
	public static double rectify(Oscillator osc) {
		return Math.abs(osc.getAmplitude());
	}
	
}
