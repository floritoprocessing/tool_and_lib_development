package graf.oscillator;

import javax.management.RuntimeErrorException;


public abstract class Oscillator {
	
	protected double frequency = 2;
	protected double phase = 0;
	protected double phaseOffset = 0;
	
	public static final int SAW = 0;
	public static final int PULSE = 1;
	public static final int SINE = 2;
	public static final int TRIANGLE = 3;
	public static final int NOISE = 4;
	public static final int SAMPLE_AND_HOLD = 5;
	
	public static final int ADDITIVE = 10;
	
	public static Oscillator createSingle(int type, double frequency) {
		switch (type) {
		case SAW: return new SawOscillator(frequency);
		case PULSE: return new PulseOscillator(frequency);
		case SINE: return new SineOscillator(frequency);
		case TRIANGLE: return new TriangleOscillator(frequency);
		case NOISE: return new NoiseOscillator();
		case SAMPLE_AND_HOLD: return new SampleAndHoldOscillator(frequency);
		case ADDITIVE: throw new RuntimeException("For additive oscillators, use createAdditive()");
		default: throw new RuntimeException("type "+type+": No such oscillator type");
		}
	}
	
	public static AdditiveOscillator createAdditive(Oscillator[] oscillators, double[] ratios) {
		return new AdditiveOscillator(oscillators,ratios);
	}
	
	public Oscillator() {
	}
	
	/**
	 * @param frequency
	 */
	public Oscillator(double frequency) {
		this.frequency = frequency;
	}
	
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	/**
	 * @param frequency
	 * @param phase
	 */
	public Oscillator(double frequency, double phaseOffset) {
		this.frequency = frequency;
		if (phaseOffset<0) throw new RuntimeException("phaseOffset has to be a positive value");
		this.phaseOffset = phaseOffset;
	}
	
	public void integrate(double time) {
		phase += (time*frequency);
		while (phase+phaseOffset>1) phase-=1;
	}
	
	protected double getCurrentPhase() {
		return (phase+phaseOffset)%1.0;
	}

	public abstract double getAmplitude();
	
	public abstract String toString();
	
}
