// get colorcodes from jpgs

int nrOfSamples=20;
PImage[] img=new PImage[6]; 
int maxPixels[]=new int[6];
String ln;

void setup() {
  for (int n=0;n<6;n++) {
    img[n]=loadImage("hair_"+n+".jpg");
    maxPixels[n]=img[n].width*img[n].height;
  }

  for (int n=0;n<6;n++) {
    if (n==0) {ln="color mainCol[][] = { {";} else {ln="{ ";}
     for (int i=0;i<nrOfSamples;i++) {
      int c=img[n].pixels[int(maxPixels[n]*i/(float)nrOfSamples)];
//    ln+=("{"+String((c>>16)&255)+","+String((c>>8)&255)+","+String(c&255)+"}, ");
      ln+=("0x"+toHex((c>>16)&255)+toHex((c>>8)&255)+toHex(c&255)+", ");
      if ((i+1)%4==0) { println(ln); ln=""; }
    }
    if (n<6-1) {ln+=" },";} else {ln+=" } }";}
    println(ln);  
  }
}


String toHex(int a) {
  String s_msb, s_lsb;
  int msb=a>>4, lsb=a&15;
  if (msb>9) {
    s_msb=""+(char(msb+55));
  } else {
    s_msb=nf(msb);
  }
  if (lsb>9) {
    s_lsb=""+(char(lsb+55));
  } else {
    s_lsb=nf(lsb);
  }
  
  return (s_msb+s_lsb);
}
