package graf.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class FloatFileReader {

	private InputStream input;
	private boolean eof = false;

	public FloatFileReader(String filename) throws FileNotFoundException {
		input = new FileInputStream(new File(filename));
	}

	private float bytesToFloat(byte[] bytes) {
		int[] b = new int[] { (int) bytes[0], (int) bytes[1], (int) bytes[2], (int) bytes[3] };
		for (int i = 0; i < 4; i++) if (b[i] < 0) b[i] += 256;
		int bits = b[3] << 24 | b[2] << 16 | b[1] << 8 | b[0];
		return Float.intBitsToFloat(bits);
	}
	
	/**
	 * Reads the next Float from the FloatFile. Returns NaN if end of file is reached
	 * @return
	 */
	public float readNextFloat() {
		byte[] bytes = new byte[4];
		try {
			int bytesRead = input.read(bytes);
			if (bytesRead == 4) {
				return bytesToFloat(bytes);
			} else {
				eof=true;
				return Float.NaN;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		eof=true;
		return Float.NaN;
	}
	
	/**
	 * Reads the next <i>amount</i> floats and returns them as an array.
	 * If the end of file is reached the returning array's size might be smaller than <i>amount</i>
	 * @param amount
	 * @return array of floats
	 */
	public float[] readNextFloats(int amount) {
		byte[] bytes = new byte[4*amount];
		float[] maxOut = new float[amount];
		float[] out = new float[amount];
		try {
			int bytesRead = input.read(bytes);
			int floatsReadAmount = bytesRead/4;
			for (int i=0;i<floatsReadAmount;i++) {
				maxOut[i] = bytesToFloat(new byte[] { bytes[i*4], bytes[i*4+1], bytes[i*4+2], bytes[i*4+3] });
				out[i] = maxOut[i];
			}
			if (floatsReadAmount<amount) {
				eof=true;
				out = new float[floatsReadAmount];
				System.arraycopy(maxOut, 0, out, 0, floatsReadAmount);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out;
	}
	
	/**
	 * Reads all floats in the FloatFile and returns them as float[]
	 * @return
	 */
	public float[] readAllFloats() {
		Vector<Float> allFloats = new Vector<Float>();
		float readFloat;// = readNextFloat();
		while (!Float.isNaN(readFloat=readNextFloat())) {
			allFloats.add(new Float(readFloat));
		}
		float[] out = new float[allFloats.size()];
		for (int i=0;i<allFloats.size();i++) out[i]=allFloats.elementAt(i).floatValue();
		return out;
	}

	/**
	 * Returns the status of the FloatFileReader if the end of file has been reached
	 * @return
	 */
	public boolean endOfFile() {
		return eof;
	}
	
	public void close() throws IOException {
		input.close();
	}

	

}
