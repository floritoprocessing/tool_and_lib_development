package graf.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FloatFileWriter {

	private OutputStream output;

	public FloatFileWriter(String filename) {
		File file = new File(filename);
		if (file.exists())
			file.delete();
		try {
			output = new FileOutputStream(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(float[] v) {
		try {
			for (int i = 0; i < v.length; i++)
				write(v[i]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void write(float v) throws IOException {
		int bits = Float.floatToIntBits(v);
		int b3 = bits >> 24 & 0xff;
		int b2 = bits >> 16 & 0xff;
		int b1 = bits >> 8 & 0xff;
		int b0 = bits & 0xff;
		byte[] bytes = new byte[] { (byte) b0, (byte) b1, (byte) b2, (byte) b3 };
		output.write(bytes);
	}

	public void close() {
		try {
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
