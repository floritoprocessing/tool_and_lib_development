package graf.animation.test;

import graf.animation.ValueAnimator;
import graf.animation.visual.processing.ValueAnimatorVisualizer;
import processing.core.*;

public class Main extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.animation.test.Main"});
	}

	ValueAnimator anim;
	ValueAnimatorVisualizer animViz;

	public void settings() {
		size(640,480);
	}
	
	public void setup(){
		
		smooth();
		anim = new ValueAnimator();
		animViz = new ValueAnimatorVisualizer(this,anim,40,40,width-40,height-40,0,0,1000,700);
		for (int i=0;i<5;i++) {
			double x = 1000*Math.random();
			double y = 700*Math.random();
			anim.addKeyframe(x,y,x-40*Math.random(),y-20+40*Math.random(),x+40*Math.random(),y-20+40*Math.random());
		}
	}

	public void keyPressed() {
		if (key==' ') setup();
	}

	public void mousePressed() {
		if (mouseButton==LEFT) {
			double[] xy = animViz.mouseToValues();
			anim.addKeyframe(xy[0],xy[1]);
		}

		if (mouseButton==RIGHT)
			for (int i=0;i<anim.getNumberOfKeyframes();i++)
				if (keyPressed&&key=='1') anim.changeTypeOfKeyframeAt(anim.getTimeOfKeyframe(i), ValueAnimator.Keyframe.TYPE_HOLD);
				else if (keyPressed&&key=='2') anim.changeTypeOfKeyframeAt(anim.getTimeOfKeyframe(i), ValueAnimator.Keyframe.TYPE_LINEAR);
				else if (keyPressed&&key=='3') anim.changeTypeOfKeyframeAt(anim.getTimeOfKeyframe(i), ValueAnimator.Keyframe.TYPE_BEZIER);
				else if (keyPressed&&key=='4') anim.changeKeyframeAtToHorizontalBezier(anim.getTimeOfKeyframe(i));
				else if (keyPressed&&key=='5') anim.resetKeyframeAtToBezier(anim.getTimeOfKeyframe(i));
				
	}

	public void draw() {
		background(240);
		noFill();
		
		stroke(0);
		rectMode(CORNERS);
		rect(40,40,width-40,height-40);
		//ValueAnimatorVisualizer.show(this,anim,40,40,width-40,height-40);
		//ValueAnimatorVisualizer.show(this,anim,40,40,width-40,height-40,0,0,1000,700);
		
		animViz.show();
		
	}
}
