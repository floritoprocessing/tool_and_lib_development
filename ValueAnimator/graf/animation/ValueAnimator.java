package graf.animation;

import java.util.Vector;

public class ValueAnimator {

	public class Keyframe {

		public static final int TYPE_LINEAR = 0;
		public static final int TYPE_BEZIER = 1;
		public static final int TYPE_HOLD = 2;
		
		private int type = TYPE_BEZIER;
		private double x, y;
		private double[] bezierX = new double[2], bezierY = new double[2];

		public Keyframe(double time, double value) {
			this.x = time;
			this.y = value;
			bezierX = new double[] {x,x};
			bezierY = new double[] {y,y};
		}

		public Keyframe(double time, double value, double bezierTime0, double bezierValue0, double bezierTime2, double bezierValue2) {
			this(time,value);
			bezierX = new double[] { bezierTime0, bezierTime2 };
			bezierY = new double[] { bezierValue0, bezierValue2 };
			type = TYPE_BEZIER;
		}

		private Keyframe(Keyframe kf) {
			type=kf.type;
			x=kf.x;
			y=kf.y;
			for (int i=0;i<2;i++) {
				bezierX[i] = kf.bezierX[i];
				bezierY[i] = kf.bezierY[i];
			}
		}

		private void set(Keyframe kf) {
			type=kf.type;
			x=kf.x;
			y=kf.y;
			for (int i=0;i<2;i++) {
				bezierX[i] = kf.bezierX[i];
				bezierY[i] = kf.bezierY[i];
			}
		}

		public Keyframe copy()  {
			return new Keyframe(this);
		}

		public void setType(int type) {
			this.type = type;
		}
		
		public void resetToBezier() {
			type = TYPE_BEZIER;
			bezierX = new double[] {x,x};
			bezierY = new double[] {y,y};
		}
		

		public void toHorizontalBezier() {
			type = TYPE_BEZIER;
			bezierY = new double[] { y,y };
		}

	}

	private Vector keyframe;
	private double time = 0;

	/**
	 * Constructor. Creates a new empty ValueAnimator
	 *
	 */
	public ValueAnimator() {
		keyframe = new Vector();
	}



	/**
	 * Constructor. Creates a new ValueAnimator with <i>value</i> at time 0
	 * @param value
	 */
	public ValueAnimator(double value) {
		this();
		keyframe.add(new Keyframe(0,value));
	}
	
	
	/***************************************************
	 * 
	 * Private methods
	 * 
	 ***************************************************/

	
	
	private Keyframe getKeyframeAtIndex(int i) {
		return (Keyframe)keyframe.elementAt(i);
	}

	private void sortKeyframes() {
		for (int i=0;i<keyframe.size()-1;i++) {
			Keyframe kf1 = getKeyframeAtIndex(i);
			for (int j=i+1;j<keyframe.size();j++) {
				Keyframe kf2 = getKeyframeAtIndex(j);
				if (kf1.x>kf2.x) {
					Keyframe tmp = kf1.copy();
					kf1.set(kf2);
					kf2.set(tmp);
				}
			}

		}
	}

	private int getIndexOfKeyframeAt(double time) {
		for (int i=0;i<keyframe.size();i++)
			if (getTimeOfKeyframe(i)==time) return i;
		return -1;
	}

	/*private Keyframe getKeyframeAt(double time) {
		int i = getIndexOfKeyframeAt(time);
		if (i>=0) return getKeyframeAtIndex(i);
		return null;
	}*/

	private double[] PointOnCubicBezier( double[][] cp, double t ) {
		
		/*
		cp is a 4 element array where:
		cp[0] is the starting point, or P0 in the above diagram
		cp[1] is the first control point, or P1 in the above diagram
		cp[2] is the second control point, or P2 in the above diagram
		cp[3] is the end point, or P3 in the above diagram
		t is the parameter value, 0 <= t <= 1
		taken from http://en.wikipedia.org/wiki/B%C3%A9zier_curve
		 */
		
		double   ax, bx, cx;
		double   ay, by, cy;
		double   tSquared, tCubed;
		double[] result = new double[2];

		/* calculate the polynomial coefficients */

		cx = 3.0 * (cp[1][0] - cp[0][0]);
		bx = 3.0 * (cp[2][0] - cp[1][0]) - cx;
		ax = cp[3][0] - cp[0][0] - cx - bx;

		cy = 3.0 * (cp[1][1] - cp[0][1]);
		by = 3.0 * (cp[2][1] - cp[1][1]) - cy;
		ay = cp[3][1] - cp[0][1] - cy - by;

		/* calculate the curve point at parameter value t */

		tSquared = t * t;
		tCubed = tSquared * t;

		result[0] = (ax * tCubed) + (bx * tSquared) + (cx * t) + cp[0][0];
		result[1] = (ay * tCubed) + (by * tSquared) + (cy * t) + cp[0][1];

		return result;
	}
	
	

	
	/**********************************************
	 * 
	 * Public Methods
	 * 
	 **********************************************/
	
	
	
	
	/**
	 * Sets the time
	 * @param time
	 */
	public void setTime(double time) {
		this.time = time;
	}
	
	
	
	/**
	 * Advances time with <i>deltaT</i>
	 * @param deltaT
	 */
	public void advanceTime(double deltaT) {
		time += deltaT;
	}
	
	
	
	
	
	/**
	 * Returns the number of keyframes
	 * @return amount
	 */
	public int getNumberOfKeyframes() {
		return keyframe.size();
	}
	
	
	
	/**
	 * Returns the time of keyframe <i>index</i>
	 * @param index
	 * @return the time of keyframe
	 */
	public double getTimeOfKeyframe(int index) {
		return getKeyframeAtIndex(index).x;
	}
	
	

	/**
	 * Returns the values of keyframe <i>index</i>
	 * @param index
	 * @return the value of keyframe
	 */
	private double getValueOfKeyframe(int index) {
		return getKeyframeAtIndex(index).y; 
	}
	
	
	
	/**
	 * Deletes the keyframe at <i>time</i> if existing.
	 * @param time
	 */
	public void deleteKeyframeAt(double time) {
		int i = getIndexOfKeyframeAt(time);
		if (i>=0) keyframe.removeElementAt(i);
	}



	/**
	 * Adds a new bezier keyframe at <i>time</i> with <i>value</i>.<br>
	 * If a keyframe exists at this time, the current keyframe gets overwritten
	 * @param time
	 * @param value
	 */
	public void addKeyframe(double time, double value) {
		deleteKeyframeAt(time);
		keyframe.add(new Keyframe(time,value));
		sortKeyframes();
	}



	/**
	 * Adds a new bezier keyframe at <i>time</i> with <i>value</i>, using bezier parameters<br>
	 * If a keyframe exists at this time, the current keyframe gets overwritten
	 * @param time
	 * @param value
	 * @param bezierTime0
	 * @param bezierValue0
	 * @param bezierTime2
	 * @param bezierValue2
	 */
	public void addKeyframe(double time, double value, double bezierTime0, double bezierValue0, double bezierTime2, double bezierValue2) {
		deleteKeyframeAt(time);
		keyframe.add(new Keyframe(time,value,bezierTime0,bezierValue0,bezierTime2,bezierValue2));
		sortKeyframes();
	}



	
	/**
	 * Changes the type of keyframe at <i>time</i> to <i>type</i> {@link Keyframe#TYPE_HOLD}, {@link Keyframe#TYPE_NORMAL} or {@link Keyframe#TYPE_BEZIER}
	 * @param time
	 * @param type
	 */
	public void changeTypeOfKeyframeAt(double time, int type) {
		int i = getIndexOfKeyframeAt(time);
		if (i>=0) getKeyframeAtIndex(i).setType(type);
	}
	
	
	
	/**
	 * Changes the keyframe to a bezier keyframe with horizontal control points
	 * @param time
	 */
	public void changeKeyframeAtToHorizontalBezier(double time) {
		int i = getIndexOfKeyframeAt(time);
		if (i>=0) getKeyframeAtIndex(i).toHorizontalBezier();
	}
	
	/**
	 * Resets the keyframe at <i>time</i> to a standard bezier, with control points at keyframe point
	 * @param time
	 */
	public void resetKeyframeAtToBezier(double time) {
		int i = getIndexOfKeyframeAt(time);
		if (i>=0) getKeyframeAtIndex(i).resetToBezier();
	}
	
	
	
	
	
	/**
	 * Returns an array of times and values<br>
	 * Array looks like this: { {time1,value1} , {time2,value2} , {time3,value3} }<br>
	 * Array size is [nrOfKeyframes][2]
	 * @return array of doubles
	 */
	public double[][] getKeyframesAsDoubles() {
		double[][] out = new double[keyframe.size()][2];
		for (int i=0;i<out.length;i++) {
			out[i][0] = getTimeOfKeyframe(i);
			out[i][1] = getValueOfKeyframe(i);
		}
		return out;
	}


	
	/**
	 * Returns an array of times and values for all keyframes including bezier points<br>
	 * Array looks like this: <br>{ {time1,value1},{bezierTime0,bezierValue0},{bezierTime2,bezierValue2} , ... }<br>
	 * Array size is [nrOfKeyframes][3][2]
	 * @return
	 */
	public double[][][] getFullKeyframesAsDoubles() {
		double[][][] out = new double[keyframe.size()][3][2];
		for (int i=0;i<out.length;i++) {
			Keyframe kf = getKeyframeAtIndex(i);
			out[i][0][0] = kf.x;
			out[i][0][1] = kf.y;
			out[i][1][0] = kf.bezierX[0];
			out[i][1][1] = kf.bezierY[0];
			out[i][2][0] = kf.bezierX[1];
			out[i][2][1] = kf.bezierY[1];
		}
		return out;
	}



	


	/**
	 * Returns the value at <i>time</i>
	 * @param x
	 * @return
	 */
	public double getValueAt(double time) {
		double x = time;
		Keyframe kfFirst = getKeyframeAtIndex(0);
		Keyframe kfLast = getKeyframeAtIndex(keyframe.size()-1);
		if (x<kfFirst.x) return kfFirst.y;
		if (x>kfLast.x) return kfLast.y;

		int i=keyframe.size()-1;
		Keyframe kf0 = getKeyframeAtIndex(i);
		while (kf0.x>x&&i>0) {
			i--;
			kf0=getKeyframeAtIndex(i);
		};
		
		i = 0;
		Keyframe kf1 = getKeyframeAtIndex(i);
		while (kf1.x<=x&&i<keyframe.size()-1) {
			i++;
			kf1=getKeyframeAtIndex(i);
		};
		
		// HOLD KEYFRAME:
		if (kf0.type==Keyframe.TYPE_HOLD) return kf0.y; //returns hold keyframe
		
		// LINEAR KEYFRAME:
		double t = (x-kf0.x)/(kf1.x-kf0.x);
		if (kf0.type==Keyframe.TYPE_LINEAR) {
			double y = kf0.y + t*(kf1.y-kf0.y);
			return y;
		}
		
		// BEZIER KEYFRAME:
		double[][] cp = new double[][] { { kf0.x,kf0.y} , { kf0.bezierX[1] , kf0.bezierY[1] } , { kf1.bezierX[0] , kf1.bezierY[0] } , { kf1.x , kf1. y } };
		double[] point = PointOnCubicBezier(cp, t);
		return point[1];
	}



	/**
	 * Returns the value at the ValueAnimator's time
	 * @return value
	 */
	public double getValueAtCurrentTime() {
		return getValueAt(time);
	}



	



	

}
