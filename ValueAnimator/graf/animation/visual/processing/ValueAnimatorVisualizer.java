package graf.animation.visual.processing;

import processing.core.PApplet;
import graf.animation.ValueAnimator;

public class ValueAnimatorVisualizer {

	private static class ValueToScreenConverter {
		
		private float x0, dx, y0, dy;
		private double vx0, vdx, vy0, vdy;
		
		public ValueToScreenConverter(float x0, float x1, float y0, float y1, double vx0, double vx1, double vy0, double vy1) {
			this.x0 = x0;
			this.y0 = y0;
			this.vx0 = vx0;
			this.vy0 = vy0;
			dx = x1-x0;
			dy = y1-y0;
			vdx = vx1-vx0;
			vdy = vy1-vy0;
		}
		
		public float valueToScreenX(double vx) {
			return (float)(x0 + (vx-vx0)/vdx * dx);
		}
		
		public float valueToScreenY(double vy) {
			return (float)(y0 + (vy-vy0)/vdy * dy);
		}
		
		public double screenToValueX(float x) {
			return vx0 + vdx*(x-x0)/dx;
		}
		
		public double screenToValueY(float y) {
			return vy0 + vdy*(y-y0)/dy;
		}
	}
	
	private PApplet pa;
	private ValueAnimator anim;
	
	private float x0, y0, x1, y1;
	private double vx0, vy0, vx1, vy1;
	private ValueToScreenConverter vcon;
	
	/**
	 * Creates a new ValueAnimatorVisualizer based on <i>anim</i> and the PApplet <i>pa</i><br>
	 * Will use screen edge values <i>x0</i>/<i>y0</i> and <i>x1</i>/<i>y1</i><br>
	 * And value edge values <i>vx0</i>/<i>vy0</i> and <i>vx1</i>/<i>vy1</i>
	 * @param pa the PApplet
	 * @param anim the ValueAnimator
	 * @param x0 screen edge min
	 * @param y0 screen edge min
	 * @param x1 screen edge max
	 * @param y1 screen edge max
	 * @param vx0 value edge min
	 * @param vy0 value edge min
	 * @param vx1 value edge max
	 * @param vy1 value edge min
	 */
	public ValueAnimatorVisualizer(PApplet pa, ValueAnimator anim, float x0, float y0, float x1, float y1, double vx0, double vy0, double vx1, double vy1) {
		this.pa=pa;
		this.anim=anim;
		this.x0=x0;
		this.y0=y0;
		this.x1=x1;
		this.y1=y1;
		this.vx0=vx0;
		this.vy0=vy0;
		this.vx1=vx1;
		this.vy1=vy1;
		vcon = new ValueToScreenConverter(x0,x1,y0,y1,vx0,vx1,vy0,vy1);
	}
	
	
	public void show() {
		show(pa,anim,x0,y0,x1,y1,vx0,vy0,vx1,vy1);
	}
	
	public double[] mouseToValues() {
		return new double[] { vcon.screenToValueX(pa.mouseX) , vcon.screenToValueY(pa.mouseY)};
	}


	/**
	 * Displays the whole ValueAnimator <i>anim</i> on the PApplet <i>pa</i><br>
	 * Displays it normalized with screen edge values <i>x0</i>/<i>y0</i> and <i>x1</i>/<i>y1</i>
	 * @param pa the PApplet
	 * @param anim the ValueAnimator
	 * @param x0 screen edge min
	 * @param y0 screen edge min
	 * @param x1 screen edge max
	 * @param y1 screen edge max
	 */
	public static void show(PApplet pa, ValueAnimator anim, float x0, float y0, float x1, float y1) {
		double[][][] kf = anim.getFullKeyframesAsDoubles();
		double px0 = kf[0][0][0], px1 = kf[kf.length-1][0][0];
		double py0 = kf[0][0][1]; 
		double py1 = kf[0][0][1];//, py1 = kf[kf.length-1][0][1];
		
		for (int i=0;i<kf.length;i++) {
			if (py0>kf[i][0][1]) py0=kf[i][0][1];
			if (py1<kf[i][0][1]) py1=kf[i][0][1];
		}
		show(pa, anim, x0, y0, x1, y1, px0, py0, px1, py1);
	}
	
	
	/**
	 * Displays the whole ValueAnimator <i>anim</i> on the PApplet <i>pa</i><br>
	 * Displays it with screen edge values <i>x0</i>/<i>y0</i> and <i>x1</i>/<i>y1</i><br>
	 * And value edge values <i>vx0</i>/<i>vy0</i> and <i>vx1</i>/<i>vy1</i>
	 * @param pa the PApplet
	 * @param anim the ValueAnimator
	 * @param x0 screen edge min
	 * @param y0 screen edge min
	 * @param x1 screen edge max
	 * @param y1 screen edge max
	 * @param vx0 value edge min
	 * @param vy0 value edge min
	 * @param vx1 value edge max
	 * @param vy1 value edge min
	 */
	public static void show(PApplet pa, ValueAnimator anim, float x0, float y0, float x1, float y1, double vx0, double vy0, double vx1, double vy1) {
		
		double[][][] kf = anim.getFullKeyframesAsDoubles();
		double px0 = kf[0][0][0], px1 = kf[kf.length-1][0][0];
		ValueToScreenConverter vcon = new ValueToScreenConverter(x0,x1,y0,y1,vx0,vx1,vy0,vy1);
		
		
		// draw bezier and keyframes points:
		for (int i=0;i<kf.length;i++) {
			float px = (float)kf[i][0][0];
			float py = (float)kf[i][0][1];
			float bx0 = (float)kf[i][1][0];
			float by0 = (float)kf[i][1][1];
			float bx1 = (float)kf[i][2][0];
			float by1 = (float)kf[i][2][1];
			
			pa.stroke(0,0,0,128);
			pa.ellipse(vcon.valueToScreenX(px),vcon.valueToScreenY(py),5,5);
			pa.stroke(0,255,0,128);
			pa.ellipse(vcon.valueToScreenX(bx0),vcon.valueToScreenY(by0),3,3);
			pa.line(vcon.valueToScreenX(px),vcon.valueToScreenY(py),vcon.valueToScreenX(bx0),vcon.valueToScreenY(by0));
			pa.stroke(255,0,0,128);
			pa.ellipse(vcon.valueToScreenX(bx1),vcon.valueToScreenY(by1),3,3);
			pa.line(vcon.valueToScreenX(px),vcon.valueToScreenY(py),vcon.valueToScreenX(bx1),vcon.valueToScreenY(by1));
		}

		// draw values:
		pa.stroke(0,0,255,128);
		pa.beginShape();
		for (double x=px0;x<px1;x+=.25) {
			double y = anim.getValueAt(x);
			pa.vertex(vcon.valueToScreenX(x),vcon.valueToScreenY(y));
		}
		pa.endShape();
	}

}
